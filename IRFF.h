#ifndef IRFF_h
#define IRFF_h 1


// --------------------------------------------------------------------------
// General Includes
// --------------------------------------------------------------------------
#ifdef VB_VERSION
  #include "./StdSeqIF.h"
#endif
#include "MrServers/MrMeasSrv/MeasUtils/nlsmac.h"

// --------------------------------------------------------------------------
// Application includes
// --------------------------------------------------------------------------
#include "MrServers/MrImaging/libSBB/SBBTSat.h"
#include "MrServers/MrImaging/seq/SystemProperties.h"
#include "MrServers/MrImaging/libSBB/SEQLoop.h"
#include "MrServers/MrImaging/libSBB/SBBIRns.h"
#include "MrServers/MrMeasSrv/SeqIF/libRT/sSYNCDATA.h"
#include "MrServers/MrMeasSrv/SeqIF/libMES/SEQData.h"

#ifdef VE_VERSION
    #include "ProtBasic/Interfaces/MrWipMemBlock.h"
    #include "MrServers/MrImaging/libSeqSysProp/SysProperties.h"
#endif


#ifdef WIN32
    #include "MrServers/MrProtSrv/MrProtocol/UILink/MrStdNameTags.h"
    #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkLimited.h"
    #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkSelection.h"
    #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkArray.h"
    #include "MrServers/MrProtSrv/MrProtocol/UILink/StdProtRes/StdProtRes.h"
    #include "MrServers/MrProtSrv/MrProtocol/libUICtrl/UICtrl.h"
    #include "MrServers/MrMeasSrv/SeqIF/Sequence/Sequence.h"
    #include <vector>
#endif

#ifdef BUILD_SEQU
    #define __OWNER
#endif
#include "MrCommon/MrGlobalDefinitions/ImpExpCtrl.h"


// Library for parameters on sequence special card
#define PM_USE_SEQIF

#ifdef VB_VERSION
    #include "parameter_map_VB20.h"
#endif
#ifdef VE_VERSION
    #include "parameter_map_VD.h"
#endif

#include "IRFF_conversion.h"

#include  "MrServers/MrImaging/seq/t1rhosbb/sbbT1rhoPrepArray.h" //ASX


#define MAXRSATS 8
#define IRF_MAX_ENVELOPE_ELEMENTS 8000
#define MAXTSL 10 //ASX maximum number of tsl points

// --------------------------------------------------------------------------
// Forward declarations
// --------------------------------------------------------------------------
namespace MrProtocolData
{
    class MrProtData;
}

class MrProt;
class SeqLim;
class SeqExpo;
class Sequence;

namespace SEQ_NAMESPACE
{
    class IRFF_UI;

    class IRFF : public PM_SeqIF<StdSeqIF>
    {
    public:

        IRFF();
        virtual ~IRFF();

        #ifdef VB_VERSION
            virtual NLSStatus initialize (SeqLim* pSeqLim);
            virtual NLSStatus prepare (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo);
            virtual NLSStatus check (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo, SEQCheckMode* pSEQCheckMode);
            virtual NLSStatus run (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo);
            virtual NLS_STATUS runKernel(MrProt* pMrProt,SeqLim* pSeqLim, SeqExpo* pSeqExpo, long lKernelMode, long lSlice, long lPartition, long lLine);
        #endif

        virtual NLSStatus initialize (SeqLim &rSeqLim);
        virtual NLSStatus prepare (MrProt &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo);
        virtual NLSStatus check (MrProt  &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo, SEQCheckMode*  pSEQCheckMode);
        virtual NLSStatus run (MrProt &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo);
        virtual NLS_STATUS runKernel(MrProt &rMrProt,SeqLim &rSeqLim, SeqExpo &rSeqExpo, long lKernelMode, long lSlice, long lPartition, long lLine);

        const IRFF_UI* getUI (void) const;

        long   getKernelRequestsPerMeasurement(void);
        long   getKernelCallsPerRelevantSignal(void);
        long   getDurationMainEventBlock(void);
        long   getScanTimeAllSats(void);
        double getdMinRiseTime(void);
        double getdGradMaxAmpl(void);

    protected:

        int32_t    m_lLinesToMeasure;
        long       m_lRepetitionsToMeasure;
        long       m_lPhasesToMeasure;
        long       m_lSlicesToMeasure;
        int32_t    m_lPartitionsToMeasure;
        long       m_lDurationMainEventBlock;
        long       m_lKernelRequestsPerMeasurement;
        long       m_lKernelCallsPerRelevantSignal;

        long       m_lMySliSelRampTime;

        double     m_dMinRiseTime;
        double     m_dGradMaxAmpl;
        double     m_dVersedReadGradAmpl;
        double     m_dVersedDepGradAmpl;
        double     m_dVersedRepGradAmpl;


        // Variables for trigger mode
        SEQ::PhysioSignal m_FirstSignal;
        SEQ::PhysioMethod m_FirstMethod;
        SEQ::PhysioSignal m_SecondSignal;
        SEQ::PhysioMethod m_SecondMethod;

        // Slice position information (rotation matrices and shifts)
        sSLICE_POS m_asSLC[K_NO_SLI_MAX];

        // These variables are needed to remember the last excited slice to use the correct rf spoil phase
        double      m_dRFSpoilIncrement;
        double      m_dRFSpoilPhase;
        double      m_dRFSpoilPhasePrevSlice;           // Remember RF spoil phase of previous slice
        double      m_dRFSpoilIncrementPrevSlice;       // Remember RF spoil phase increment
        double      m_dRFPrevSlicePosSag;               // These are used to check whether we're dealing
        double      m_dRFPrevSlicePosCor;               // with a different slice when RF spoiling is used
        double      m_dRFPrevSlicePosTra;
        double      m_dRFPrevSliceNormalSag;
        double      m_dRFPrevSliceNormalCor;
        double      m_dRFPrevSliceNormalTra;


        // ----------------------------------------------------------
        // Instantiate RF Pulse objects
        // ----------------------------------------------------------
        sRF_PULSE_SINC      m_sSRFSinc;
        sRF_PULSE_ARB       m_sRF_FOCI;
        sSample             m_sFOCIsamples[IRF_MAX_ENVELOPE_ELEMENTS];

        sFREQ_PHASE         m_sSRF01zSet;
        sFREQ_PHASE         m_sSRF01zNeg;

        double*            m_dFlipAnglesSeriesA;
        double*            m_dFlipAngleArray;
        long               m_lNumberOfFlipangles;

        // ----------------------------------------------------------
        // Instantiate Gradient Pulse Objects
        // ----------------------------------------------------------
        sGRAD_PULSE m_sGSliSel;
        sGRAD_PULSE m_sGSliSelReph;

        sGRAD_PULSE m_sGReadDeph;
        sGRAD_PULSE_RO m_sGradReadout;
        sGRAD_PULSE m_sGReadReph;

        sGRAD_PULSE m_sGReadDephR;
        sGRAD_PULSE m_sGReadDephP;
        sGRAD_PULSE_RO m_sGradReadoutR;
        sGRAD_PULSE_RO m_sGradReadoutP;
        // #ifdef ADC_VERSED
          sGRAD_PULSE_RO m_sGradReadoutR2;
          sGRAD_PULSE_RO m_sGradReadoutP2;
        // #endif
        sGRAD_PULSE m_sGReadRephR;
        sGRAD_PULSE m_sGReadRephP;

        sGRAD_PULSE m_sGSpoil;

        // ----------------------------------------------------------
        // Instantiate Readout objects
        // ----------------------------------------------------------
        sREADOUT    m_sADC01;
        sFREQ_PHASE m_sADC01zSet;
        sFREQ_PHASE m_sADC01zNeg;

		// ----------------------------------------------------------
		// Instantiate Sequence Building Blocks (SBBs)
		// ---------------------------------------------------------
		SBBList            			 m_mySBBList;
        SeqBuildBlockTokTokTok 		 m_TokTokSBB;
		SeqBuildBlockT1rhoPrepArray  m_T1rhoSBB[MAXTSL];  //ASX
		
        // ----------------------------------------------------------
        // Instantiate Sync objects: Osc bit and triggering
        // ----------------------------------------------------------
        sSYNC_OSC          m_sOscBit;

        IRFF_UI* m_pUI;
        virtual NLS_STATUS createUI (SeqLim &rSeqLim);

        // Used to avoid compiler warnings
        template< class TYPE > void UnusedArg (TYPE Argument) const { if( false ) { TYPE Dummy; Dummy = Argument; } };

        bool    m_bIsFISP_Segment;

        long    m_lTRMin;
        long    m_lSetIndex;
        long    m_lShotIndex;
        long    m_lPulseDuration;
        long    m_lShotsInScan;
        long    m_lInversionAngle;
        long    m_lCurrKernelCall;

        selection m_sSliceMode; //  <- for HIP r01
        selection m_sIceMode;

        long      m_lKshift;
        double    m_dFFTFactor;
		
	//T1rho Variables
		selection m_sT1rTrainMode; //ASX
		selection m_sPrepType;  // T1rho or T2
		
		bool	  m_bUseT1rT2Prep;			//ASX
		bool      m_bIsT1r_Segment;
		
		long	  m_lT1Delay_ms;				//ASX
		long	  m_lTslToMeasure;				//ASX		
		long      m_lT1rNumberOfFlipangles;
		long	  m_lT1rRequestPerMeasurement;	//ASX
		
		double	  m_dT1rFlipAngle;
		double    m_dSpinLockFreq_Hz;			//ASX

		double	  m_adTsl_us[MAXTSL];			//ASX
		double	  m_cadMin  [MAXTSL];			//ASX
		double	  m_cadMax  [MAXTSL];			//ASX
		double	  m_cadInc  [MAXTSL];			//ASX
		double	  m_cadDef  [MAXTSL];			//ASX
		long      m_lTsl;

        // ------------------------------------------------------------------
        // Helper Functions
        // ------------------------------------------------------------------
        void updateRFpointer();
        void getRotationAngle(double &angle);
        void getSlicePos(MrProt *pMrProt, SeqLim *pSeqLim, long lIndex, double dAngle);

        NLSStatus runInversion(MrProt &rMrProt,  long lSlice);
        NLSStatus runSegment(long lSlice, long lSegment, long lSegmentSize, MrProt &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo);
        NLS_STATUS noiKernel(MrProt &rMrProt,SeqLim &rSeqLim, SeqExpo &rSeqExpo, long lChronologicSlice, long lPartition, long lLine);

        #ifdef VE_VERSION
        MrProtocolData::SeqExpoRFInfo getLocalSAR();
        #endif

        #ifdef VB_VERSION
        NLS_STATUS clockShiftCorrection (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo);
        #endif

    private:

        long run_kernel_counter;

        // ------------------------------------------------------------------
        // Copy constructor not implemented
        // ------------------------------------------------------------------
        IRFF (const IRFF &right);

        // ------------------------------------------------------------------
        // Assignment operator not implemented
        // ------------------------------------------------------------------
        IRFF & operator=(const IRFF &right);

    };



    inline long IRFF::getKernelRequestsPerMeasurement(void)
    {
        return (m_lKernelRequestsPerMeasurement);
    }

    inline long IRFF::getKernelCallsPerRelevantSignal(void)
    {
        return (m_lKernelCallsPerRelevantSignal);
    }

    inline long IRFF::getDurationMainEventBlock(void)
    {
        return (m_lDurationMainEventBlock);
    }

    inline double IRFF::getdMinRiseTime(void)
    {
        return (m_dMinRiseTime);
    }

    inline double IRFF::getdGradMaxAmpl(void)
    {
        return (m_dGradMaxAmpl);
    }


}


#endif
