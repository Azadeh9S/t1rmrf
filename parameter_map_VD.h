// FILE        : parameter_map3.h
// AUTHOR      : Maxim Zaitsev <zaitsev@ukl.uni-freiburg.de>
// DESCRIPTION : This file provides a framework to facilitate creation and
//               maintanence of the "Sequence/Special" user-interface card
//               of the NUMARIS/4 platform

// Copyright (C) 2003-2007 Maxim Zaitsev
//
// Permission is  hereby granted,  free of charge,  to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction,  including without limitation
// the rights to use,  copy, modify, merge, publish,  distribute, sublicense,
// and/or  sell  copies of  the Software,  and to permit persons  to whom the
// Software is furnished to do so, subject to the following two conditions:
//
// 1. Due to the fact that the Software is based on the Numaris IDEA software
//    package the distribution  of the Software is only  alowed as long as it
//    does  not  violate the IDEA licence  and the associated  non-disclosure
//    agreement.
//
// 2. The above copyright notice and this permission notice shall be included
//    in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED,  INCLUDING BUT NOT  LIMITED TO THE WARRANTIES  OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR  PURPOSE AND  NONINFRINGEMENT. IN NO  EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN  AN ACTION OF CONTRACT,  TORT OR OTHERWISE,  ARISING
// FROM, OUT  OF OR  IN CONNECTION  WITH  THE SOFTWARE  OR  THE USE  OR OTHER
// DEALINGS IN THE SOFTWARE.

// NEW FEATURES
// ============
// C++ sequence interface (SeqIF) support has been added to version 3, which
// includes new PM_USE_SEQIF and IMPLEMENT_PM_SEQIF() macros and template
// class PM_SeqIF. See the C++ Sequence Interface section below for details.

// Parameter grouping has been introduced in version 2 as a means to overcome 
// the 14-parameter limitation. The use of standard Siemens tags is an 
// alternative way of increasing the number of parameters. A possibility to 
// update tooltips is another usefull addition. Optional compaction boolean
// parameters into one or several long protocol params further increases the
// maximum number of usable parameters.

// Protocol Conversion Support: A new macro has been added to aid the protocol
// conversion process, if new parameters have been added or need to be updated
// at the time of the change of the scanner software version. Lookup an example
// on CONVERT_PARAMETER_MAP below for more information.

// INCOMPATIBILITIES WITH PREVIOUS VERSIONS
// ========================================
// V2 to V1:
// SKIP_PARAM(); is now a function and thus requires () and ;
// Handling of selections in PM2 has changed:
// *  Selection parameters now must be declated as having a type 'selection' 
//    instead of 'long'
// *  PARAM_SELECT(...); is now a function and requires a semicolon a the end
// *  PARAM_SELECT(...) now has tooltip as a third optional parameter 
// *  TOOLTIP() macro is eleminated
// *  PARAM_SELECT_END(); is now a function and requires opening and closing
//    brackets '()' and a semicolon at the end
// V3 toV2: none known so far

// GENERAL INTRODUCTION
// ====================
// This is just a short introduction to the PARAMETER_MAP2 (PM2) framework. In time, as
// the library evolves, me or somebody else may write a more complete and comprehensive
// manual. However, I strongly believe that the text provided here along with the
// modified FLASH example must be sufficient to start using the framework.
//
// The interface of the library consists of a dozen of "tokens", in part they are macros
// and in the other part they are implemented as overloaded functions. This makes up a
// rule number one: to make life easier always put a semicolon after the PM "tokens".
//
// Using the framework involves three basic steps:
// *  At the global level define your variables, which have to be linked to the
//    "Sequence/Special" card.
// *  In the "fSeqInit" function, using PM macros/functions, describe your
//    "Sequence/Special" card layout and associations between the various UI elements
//    and the global variables defined in the previous step. It is important to note that
//    only global variables may be used with PM.
// *  In the beginning of the "fSeqPrep" function place the
//    PREPARE_PARAMETER_MAP(pMrProt, pSeqLim) statement. In the end of the function, for
//    the case of successful preparation place the UPDATE_PROTOCOL(pMrProt, pSeqLim)
//    statement.
// *  Note for VD11 users: PREPARE_PARAMETER_MAP() and other relevant macros should be 
//    used with &rMrProt instead of pMrProt and &rSeqLim instead of pSeqLim, respectively
// After having done that you can use the global variables, which are involved in PM,
// without any additional precautions. In some cases, for example in custom 'solve' or 'try'
// handlers additional prepare and update statements might be required. Given that the
// PREPARE_PARAMETER_MAP and UPDATE_PROTOCOL macros are properly
// placed in your sequence code the framework ensures synchronization between the global
// variables and the MR protocol.
//
// SEQUENCE/SPECIAL CARD LAYOUT
// ============================
// To describe the layout and behaviour of the Sequence/Special card it is required to
// declare a parameter map in your fSeqInit. It is important that the declaration IS NOT
// placed within any conditional statements like "#ifndef VXWORKS" or BUILD_PLATFORM_LINUX.
// The PM library takes care about compilation conditions.
//
// A rather advanced parameter map looks as follows:
//
// BEGIN_PARAMETER_MAP(pSeqLim, initial_alFreeParam, initial_adFreeParam);
//     COMPACT_BOOLS(); // optional setting to save parameter space in the protocol
//     COMPACT_SELECTIONS(); // optional setting to save parameter space in the protocol
//     PARAM("Bool Parameter Name", &bMyBoolVariable, true /* default value */, "Optional tool tip text");
//     PARAM("Long Parameter Name", "unit", &lMyLongVariable, 1 /* min */, 100 /* max */, 1 /* inc */, 10 /* default */, "Optional tool tip text");
//     PARAM("Double Parameter Name", "unit", &dMyDoubleVariable, 1.0 /* min */, 100.0 /* max */, 1.0 /* inc */, 10.0 /* default */, "Optional tool tip text");
//     SKIP_PARAM(); /* leave one position empty */
//     PARAM_SELECT("Selection Parameter Name", &selMySelectionVariable, 1 /* default */, "Optional tool tip text"); /* in PM2 selMySelectionVariable needs to be defined as 'selection' */
//         OPTION("Option Text 1", 1 /* corresponding value */);
//         OPTION("Option Text 2", 2 /* corresponding value */);
//         OPTION("Option Text 3", 3 /* corresponding value */);
//     PARAM_SELECT_END();  /* for selections it is recommended to use enums instead of constants,*/
//                          /* which improves the code readability and eases maintenance */
//     SKIP_PARAMS(2); /* leave two positions empty */
//     PARAM("Long Array Name", "unit", 4 /* array length */, alMyLongArray, calMin /* constant array holding minimum values */, calMax /* max */, calInc /* inc */, calDef /* default */, "Optional tool tip");
//     USE_STANDARD_TAG(MR_TAG_TD); /* this will create the next parameter in place of "TD" on the "Contrasts" card */
//     PARAM("Double Array Name", "unit", 3 /* array length */, adMyDoubleArray, cadMin /* constant array holding minimum values */, cadMax /* max */, cadInc /* max */, cadDef /* default */, "Optional tool tip");
//
//     PARAM_GROUP(); /* create a group of parameters to save space on the card */
//         PARAM("Long Groupped Para A", "unitA", &lMyLongVariable1, 1 /* min */, 100 /* max */, 10 /* default */, "Optional tool tip text");
//         PARAM("Long Groupped Para B", "unitB", &lMyLongVariable2, 1 /* min */, 100 /* max */, 10 /* default */, "Optional tool tip text");
//     PARAM_GROUP_END();
//
//     LINK_ENABLE_DISABLE(&lMyLongVariable /* PM variable */, &bMyBoolVariable /* any global boolean variable */);
//             /* enables or disables the "Long Parameter" parameter depending on the value of bMyBoolVariable */
//     LINK_SHOW_HIDE(adMyDoubleArray /* PM variable */, &bMyBoolVariable /* any global boolean variable */);
//             /* shows or hides the "Double Array" parameter depending on the value of bMyBoolVariable */
//
//     TOOLTIP_CUSTOM(&dMyDoubleVariable, GetTooltipIdHandler /* Siemens-style GetTooltipID handler*/);
//             /* enables to produce tool tips which are dependent on the current protocol state */
//
//     SOLVE_TE_TR_TI(&dMyDoubleVariable); /* use standard Siemens solve handler with "Double  Parameter" */
//     TRY_CUSTOM(&lMyLongVariable, TryHandler /* Siemens-style GetTooltipID handler*/);
//             /* replace the standard try-handler with the custom one for "Long Parameter" */
//     SOLVE_CUSTOM(&lMyLongVariable, SolveHandler /* Siemens-style Solve handler*/);
//             /* replace the standard solve-handler with the custom one for "Long Parameter" */
//
//     VERIFY_OFF(&selMySelectionVariable);
//             /* switch off parameter range verification */
//     VERIFY_ALL(&adMyDoubleArray);
//             /* switch to the extremely slow full range verification */
// END_PARAMETER_MAP;
//
// For more examples on parameter map usage consult the modified example FLASH
// sequence (mz_FLASH).
//
// Note: all numeric parameters support base2 increments. To create such a parameter 
// the PM_INC_BASE2 macro needs to be used in place of the increment.
//
// PARAMETER GROUPING
// ==================
// Parameter grouping for all types of parameters has been introduced in PM2 as an option to
// overcome the 14 paraneter limitation of the Sequence/Special card. When user creates a group
// of one to several parameters, an array parameter is created in the UI with the elements of
// the array represented by the respective parameters. From the programmers point of view the
// grouped parameters may be treated completely in the same way as the normal ones. There may
// be certain limitatations in making some parameters of the group to appear read-only (gray).
// It is to be noted, that only parameters of the same type may be grouped together. If both
// long and double peremeters are placed between the PARAM_GROUP(); and PARAM_GROUP_END();
// commands two groups will be created. Mixing of other parameters types might lead to
// instabilities and is strongly discouraged.
//
// USING STANDARD TAGS
// ===================
// In contrast to its presecessor, PM2 is no longer limited to Sequence/Special card and can
// now handle parameters on the other UI cards. The function USE_STANDARD_TAG(tag_name)
// instructs the framework to use the specified tag for the next parameter. It is, however,
// is not possible to change the type of the UI elements associated with the standard tags.
// PM2 has also no means of checking the type of UI elements, that is why crashes are very
// probable when parameter and tag types do not match. The type limitations also apply between
// the single parameters and arrays. That is an array parameter may not be associated with the
// tag of the MrProt's single element parameter. Fot the vice-versa situation there is a work
// around. The single parameter may be declared as a group, which is then handled as an array
// of a single element.
//
// OTHER ADVANCED TOPICS
// =====================
// COMPACT_BOOLS() directive at the top of the parameter map instructs the PM2 framework to
// pack boolean parameters together onto bits of a single or multiple long parameters.
// Up to 31 bool parameters can be packed into a single long. Note that these long parameters
// appear as large negative numbers in the protocol text file. IMPORTANT to note that
// protocols generaged with and without COMPACT_BOOLS are incompatible.
//
// COMPACT_SELECTIONS() has a similar to COMPACT_BOOLS() effect on selection parameters. 
// This directive takes an optional compacting factor as parameter, which defines how many
// bits will be used to store a single option. The default of 4 results in 8 bits per option
// and thus limits the option values to 0..255. The maximum compacting factor is 16.
//
// Tooltip of any parameter can be changed at any time using function 
//   PRINTF_TOOLTIP(SeqLim* pSeqLim, const T* pValue, const char * szFormat, ...)
//
// An alternative mechanism to enable, disable, show or hide parameters at any time has
// been introduced in PM2 using functions
//   ENABLE_PARAM(SeqLim* pSeqLim, const T* pVal, bool bEnable = true)
//   DISABLE_PARAM(SeqLim* pSeqLim, const T* pVal)
//   SHOW_PARAM(SeqLim* pSeqLim, const T* pVal, bool bEnable = true)
//   HIDE_PARAM(SeqLim* pSeqLim, const T* pVal)
// To control state of multiple parameters at the same time based on the same condition use
//   SHOW_MULTIPLE_PARAMS(SeqLim* pSeqLim, bool bCondition, const void* pVal_01, const void* pVal_02,...)
//   ENABLE_MULTIPLE_PARAMS(SeqLim* pSeqLim, bool bCondition, const void* pVal_01, const void* pVal_02,...)
// NOTE: if parameter state is changed multiple times within one fSeqPrep() call the 
// last change will have the effect.
//
// It is possible to change dynamically the limits and the increment for "long" or "double"
// parameters using following functions:
//   CHANGE_LIMITS(const long* pnValue, long nMin, long nMax, long nInc)
//   CHANGE_LIMITS(const double* pdValue, double dMin, double dMax, double dInc)
// These functions can be placed anywhere in the code, but typically in the end of
// "fSeqPrep" function.
//
// It is also possible to change dynamically the "visible" size of the array parameters
// using following functions:
//   VISIBLE_ARRAY_SIZE(SeqLim* pSeqLim, const long* pnValue, long nSize)
//   VISIBLE_ARRAY_SIZE(SeqLim* pSeqLim, const double* pdValue, long nSize)
// These functions can be placed anywhere in the code, but typically in the end of
// "fSeqPrep" function.
//
// It is possible to find out the place where the respective parameter is stored in the
// protocol WIP space using the corresponding PM_INDEX() function:
//   long PM_INDEX(const bool* pbValue);
//   long PM_INDEX(const long* pnValue);
//   long PM_INDEX(const selection* pnValue); 
//   long PM_INDEX(const double* pdValue);
// For array variables the index of the first array element is returned.
//
// PROTOCOL UPDATE SUPPORT
// =======================
// If protocol strure has experienced a change e.g. through addition of new parameters 
// during the update of the sequence code from a previous software base to the new one
// it is very likely that the protocol conversion step on the scanner will fail resulting
// in old protocols to be marked as "unlicensed". PM2 provides means to smooth this 
// transition using the CONVERT_PARAMETER_MAP macro in fSEQConvProt() a following way:
// NLS_STATUS fSEQConvProt (const MrProt &rMrProtSrc, MrProt &rMrProtDst )
// {
//   NLS_STATUS                lRet                 = SEQU_NORMAL;
//   CONVERT_PARAMETER_MAP(&rMrProtDst); // this will initialise new parameters with defaults
//   // now you can optionally update the PM variables
//   // e.g. u_lManualNoOfDummyScans=-1;
//   UPDATE_PROTOCOL(&rMrProtDst, ((SeqLim*)NULL));
//   return lRet;
//  }
//
// 
// C++ SEQUENCE INTERFACE
// ======================
// Version 3 of the PM framework provides a support of the C++ sequence
// interface. There are several conceptual points, which need to be taken
// into account when switching to the C++ interface:
// * all PM variables are supposed to be members of your sequence class,
//   otherwise the sequence might misbehave
// * PM3 file in the C++ mode can now be included in the header file of your
//   sequence or within several .cpp files.
//
// C++ operation of PM3 includes 3 ingredients: (1) define PM_USE_SEQIF; 
// (2) derive your sequence from the PM_SeqIF<> template; (3) add a macro
// IMPLEMENT_PM_SEQIF() to the main .cpp file of your sequence class.
//
// 1. In order to switch to the SeqIF mode a macro PM_USE_SEQIF needs to be
// defined prior to *every* include of PM3, which might look like:
//   #define PM_USE_SEQIF
//   #include "../mz_common/parameter_map3.h"
// Alternatively a statement -DPM_USE_SEQIF can be added to your sequence
// makefile, which makes it unnecessary to use an explicit define as above.
// If you choose this way add a following line to your <seq_name>.mk at an
// appropriate place:
//   CPPFLAGS_LOCAL += -DPM_USE_SEQIF
//
// 2. Your sequence class needs now to be derived from the PM_SeqIF<> template
// class. In order to give the user flexibility in choosing parent classes for
// their sequences the template argument of PM_SeqIF needs to be specified by
// the user. That is, if your sequence based on its logic hat to be derived
// from SeqIF you have to specify PM_SeqIF<SeqIF> instead. If it is to be
// derived from some other SeqIF derivative, e.g. SomeAdvanced_SeqIF, then
// PM_SeqIF<SomeAdvanced_SeqIF> should be used.
//
// VERY IMPORTANT: in case of multiple inheritence PM_SeqIF<> is only to be
// used once and only with SeqIF or its derivative.
//
// 3. Add IMPLEMENT_PM_SEQIF(Your_Sequence_Class_Name) somewhere to your
// sequence .cpp file.
//
//
// LIMITATIONS OF THE FAMEWORK
// ===========================
// - The overall number of parameters is limited by the number of the MrProt's free parameters
//   That is, 16 doubles and 64 of others all together. The later can be effectively reduced 
//   using COMPACT_XXX macros.
// - Changes in the card layout **may** require creation of new protocols
// - Selection parameter can contain maximum of 16 options
// - All elements of an array have same name and unit (this is not the case for groups)
// - USE_STANDARD_TAG() needs to be used with the **great care**, to verify the compatibility of 
//   parameter types is the users responsibility. Using standard tags with wromg parameters may
//   cause parameter map to crash
// - Arrays may not be grouped
// - Custom handlers are not multiplexed, custom handler for each parameter has to be
//   implemented as a separate function
// - Adding new parameters to a sequence derived from another sequence, which
//   already uses PM3 is not supported yet
//
// FEEDBACK is very welcome and is highly appreciated
// ==================================================

#ifndef __PARAMETER_MAP_H__INCLUDED__
#define __PARAMETER_MAP_H__INCLUDED__

#include "ProtBasic/Interfaces/MrWipMemBlock.h"

#ifndef BUILD_PLATFORM_LINUX
  //#include "ExamUI/ExamDb/Protocol/UILink/MrStdNameTags.h"
  //#include "ExamUI/ExamDb/Protocol/libUILink/UILinkLimited.h"
  //#include "ExamUI/ExamDb/Protocol/libUILink/UILinkSelection.h"
  //#include "ExamUI/ExamDb/Protocol/libUILink/UILinkArray.h"
  //#include "ExamUI/ExamDb/Protocol/UILink/StdProtRes/StdProtRes.h"
  //#include "Application/libUICtrl/UICtrl.h"
  //#include "Common/STL/Vector.h"
  //#include "Measurement/Sequence/Sequence.h"
  #include "MrServers/MrProtSrv/MrProtocol/UILink/MrStdNameTags.h"
  #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkLimited.h"
  #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkSelection.h"
  #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkArray.h"
  #include "MrServers/MrProtSrv/MrProtocol/UILink/StdProtRes/StdProtRes.h"
  //mz: VD13-specific #include "MrServers/MrImaging/libUICtrl/UICtrl.h"
  #include "MrServers/MrProtSrv/MrProtocol/libUICtrl/UICtrl.h" //mz: VD13-specific
  //#include "MrCommon/MrCFramework/STL/Vector.h"
  #include "MrServers/MrMeasSrv/SeqIF/Sequence/Sequence.h"
#endif

//#include <limits.h> // fixes a compilation bug on VB15
// For some IDEA installations, <limits.h> cannot be found. 
// The following is an alternate definition.
// Modified by Mark Brown - Siemens
#ifdef BUILD_PLATFORM_LINUX
  #ifndef INT_MAX
    //#define INT_MAX  _ARCH_INT_MAX
	#define INT_MAX  2147483647
  #endif
#endif

#ifndef BUILD_PLATFORM_LINUX
#pragma warning( disable : 4505 )
#pragma warning( disable : 4284 )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4706 )
#endif

// VA25/VB11/VB12: option lists use mrstd::vector<> VB13: std::vector<>

//////////////////////////////////////////////////////
// a very simple stl-like template list implementation
//////////////////////////////////////////////////////

// it has to be here because it's not easy (but still possible) to use STL in sequences
// at least in the VA tree

template <class T>
struct TListItem {
	T* pData;
	TListItem* pNextItem;
	TListItem(const T& d, TListItem<T>* pn) : pNextItem(pn) {
		pData = new T(d);
	}
private:
	TListItem() {}
};

template <class T>
class TSimpleIterator {
public:
	typedef TListItem<T> ListItem;

	TSimpleIterator() : m_pItem(NULL) {}
	TSimpleIterator(const TSimpleIterator& orig) : m_pItem(orig.m_pItem) {}
	TSimpleIterator(ListItem* pItem): m_pItem(pItem) {}
	
	const TSimpleIterator& operator=(const TSimpleIterator& orig) { m_pItem = orig.m_pItem; return *this; }
	
	const TSimpleIterator& operator++() { if (m_pItem) m_pItem = m_pItem->pNextItem; return *this; } // prefix
	TSimpleIterator operator++(int) { register ListItem* pItem = m_pItem; if (m_pItem) m_pItem = m_pItem->pNextItem; return pItem; } // postfix
	
	T& operator*() { return *m_pItem->pData; }
	operator T*() { return m_pItem->pData; }
	T* operator->() { return m_pItem->pData; }

	bool operator==(const TSimpleIterator<T> & it2) { return m_pItem == it2.m_pItem; }
	bool operator!=(const TSimpleIterator<T> & it2) { return m_pItem != it2.m_pItem; }

protected:
	ListItem* m_pItem;
};

template <class T>
class TSimpleList {
public:
	typedef TListItem<T> ListItem;
	typedef TSimpleIterator<T> iterator;

	TSimpleList() : m_pHead(NULL) {}
	~TSimpleList() { for ( iterator it = begin(); it != end(); it++) delete (T*) it; }

	void push_front(const T& elm) { m_pHead = new ListItem(elm, m_pHead); }

	const iterator begin() { return m_pHead; }
	const iterator& end() { static iterator sitEnd(NULL); return sitEnd; }

	T* frontPtr() { return m_pHead->pData; }
protected:
	ListItem* m_pHead;
};


template <class T>
class TSimpleIndex {
public:
	TSimpleIndex() : m_pData(NULL), m_nSize(0), m_nOffset(0) {}
	~TSimpleIndex() { if (m_pData) delete m_pData; }
	T* find(int nPos) { if (nPos >= m_nSize || nPos < m_nOffset) return NULL; return m_pData[nPos - m_nOffset]; }
	void build(TSimpleList<T>& list) {
		if (m_pData) delete m_pData;
		m_nSize = 0;
		m_nOffset = INT_MAX;
		// first scan the list to define the size of the index
		typename TSimpleList<T>::iterator it;
		for (it = list.begin(); it != list.end(); ++it)
		{
			if (it->nID < 0)
				continue;
			if (it->nID >= m_nSize)
				m_nSize = it->nID + 1;
			if (it->nID < m_nOffset)
				m_nOffset = it->nID;
		}

		if (!m_nSize)
			return;

		if (m_nOffset == INT_MAX)
			m_nOffset = 0; // this should never happen

		m_pData = new T*[m_nSize-m_nOffset];
		memset(m_pData, 0, (m_nSize-m_nOffset)*sizeof(T*));
		for (it = list.begin(); it != list.end(); ++it)
		{
			if (it->nID < 0)
				continue;
			m_pData[it->nID-m_nOffset] = it;
		}
	}
protected:
	T** m_pData;
	int m_nSize;
	int m_nOffset;
};

///////////////////////////////////////////////////////////////////////

typedef unsigned int selection;

enum ParamBoolOptions {
	PARAM_BOOL_INVALID = 0,
	PARAM_BOOL_FALSE,
	PARAM_BOOL_TRUE
};

#ifndef BUILD_PLATFORM_LINUX

#define MAX_PARAM_NAME	64
#define MAX_UNIT		16
#define MAX_TOOLTIP		1024
#define MAX_OPTIONS		16
#define MAX_OPTIONS_STR 1024
#define MAX_CUSTOM_TAG  128

#define TEMPLATE_SPEC template<>

typedef const char* LPCSTR;

////////////////////////////////////
// template UI interface type holder
////////////////////////////////////

template <class T_data>
class TN4I { 
public:
	typedef void   tLink;
	typedef tLink* tLinkPtr;
	typedef void   tMrLimit;
	typedef T_data tData;
	typedef T_data tOption;

	typedef bool     (*tTryPtr)          (tLink* const, void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tSolvePtr)        (tLink* const, char**, const void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tGetToolTipIdPtr) (tLink* const, char* arg_list[], long lIndex);

    typedef T_data   (*tGetValuePtr)     (tLinkPtr const pThis, long lPos);
	typedef T_data   (*tSetValuePtr)     (tLinkPtr const pThis, T_data newVal, long lPos);

};

template<>
class TN4I<bool> {
public:
	typedef LINK_BOOL_TYPE tLink;
	typedef tLink*         tLinkPtr;
	typedef void           tMrLimit;
	typedef bool           tData;
	typedef unsigned       tOption; // VA25/VB11/VB12:bool VB13:unsigned

	typedef bool     (*tTryPtr)          (tLink* const, void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tSolvePtr)        (tLink* const, char**, const void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tGetToolTipIdPtr) (tLink* const, char* arg_list[], long lIndex);

    typedef tData   (*tGetValuePtr)     (tLinkPtr const pThis, long lPos);
	typedef tData   (*tSetValuePtr)     (tLinkPtr const pThis, tData newVal, long lPos);

	enum {
		VERIFY_ON            = LINK_BOOL_TYPE::VERIFY_ON,
		VERIFY_OFF           = LINK_BOOL_TYPE::VERIFY_OFF,
		VERIFY_BINARY_SEARCH = LINK_BOOL_TYPE::VERIFY_ON, // dummy to get the template formalism to work
		VERIFY_SCAN_ALL      = LINK_BOOL_TYPE::VERIFY_ON  // the same
	};
	enum {
		DEFAULT_PRECISION = 0
	};
#ifndef BUILD_PLATFORM_LINUX
	static unsigned SolveTE_TR_TI(tLinkPtr const _this, char* arg_list[], const void* pAddMem, const MrProtocolData::MrProtData* pOrigProtData, long lIndex ){
		return ( fUICSolveBoolParamConflict ( _this, arg_list, pAddMem, pOrigProtData, lIndex, NULL, NULL, NULL ) );  
	}
#endif // BUILD_PLATFORM_LINUX
};

template<>
class TN4I<long> {
public:
	typedef LINK_LONG_TYPE tLink;
	typedef tLink*         tLinkPtr;
	typedef MrLimitLong    tMrLimit;
	typedef long           tData;
	typedef long           tOption;

	typedef bool     (*tTryPtr)          (tLink* const, void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tSolvePtr)        (tLink* const, char**, const void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tGetToolTipIdPtr) (tLink* const, char* arg_list[], long lIndex);

    typedef tData   (*tGetValuePtr)     (tLinkPtr const pThis, long lPos);
	typedef tData   (*tSetValuePtr)     (tLinkPtr const pThis, tData newVal, long lPos);

	enum {
		VERIFY_BINARY_SEARCH = LINK_LONG_TYPE::VERIFY_BINARY_SEARCH,
		VERIFY_SCAN_ALL      = LINK_LONG_TYPE::VERIFY_SCAN_ALL,
		VERIFY_OFF           = LINK_LONG_TYPE::VERIFY_OFF
	};
	enum {
		DEFAULT_PRECISION = 0
	};
#ifndef BUILD_PLATFORM_LINUX
	static unsigned SolveTE_TR_TI(tLinkPtr const _this, char* arg_list[], const void* pAddMem, const MrProtocolData::MrProtData* pOrigProtData, long lIndex ){
		return ( fUICSolveLongParamConflict ( _this, arg_list, pAddMem, pOrigProtData, lIndex, NULL, NULL, NULL ) );  
	}
#endif // BUILD_PLATFORM_LINUX
};

template<>
class TN4I<double> {
public:
	typedef LINK_DOUBLE_TYPE tLink;
	typedef tLink*           tLinkPtr;
	typedef MrLimitDouble    tMrLimit;
	typedef double           tData;
	typedef double           tOption;

	typedef bool     (*tTryPtr)          (tLink* const, void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tSolvePtr)        (tLink* const, char**, const void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tGetToolTipIdPtr) (tLink* const, char* arg_list[], long lIndex);

    typedef tData   (*tGetValuePtr)     (tLinkPtr const pThis, long lPos);
	typedef tData   (*tSetValuePtr)     (tLinkPtr const pThis, tData newVal, long lPos);

	enum {
		VERIFY_BINARY_SEARCH = LINK_DOUBLE_TYPE::VERIFY_BINARY_SEARCH,
		VERIFY_SCAN_ALL      = LINK_DOUBLE_TYPE::VERIFY_SCAN_ALL,
		VERIFY_OFF           = LINK_DOUBLE_TYPE::VERIFY_OFF
	};
	enum {
		DEFAULT_PRECISION = 6
	};
#ifndef BUILD_PLATFORM_LINUX
	static unsigned SolveTE_TR_TI(tLinkPtr const _this, char* arg_list[], const void* pAddMem, const MrProtocolData::MrProtData* pOrigProtData, long lIndex ){
		return ( fUICSolveDoubleParamConflict ( _this, arg_list, pAddMem, pOrigProtData, lIndex, NULL, NULL, NULL ) );  
	}
#endif // BUILD_PLATFORM_LINUX
};

template<>
class TN4I<selection> {
public:
	typedef LINK_SELECTION_TYPE tLink;
	typedef tLink*              tLinkPtr;
	typedef void                tMrLimit;
	typedef selection           tData;
	typedef unsigned            tOption;

	typedef bool     (*tTryPtr)          (tLink* const, void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tSolvePtr)        (tLink* const, char**, const void*, const MrProtocolData::MrProtData*, long);
	typedef unsigned (*tGetToolTipIdPtr) (tLink* const, char* arg_list[], long lIndex);

    typedef tData   (*tGetValuePtr)     (tLinkPtr const pThis, long lPos);
	typedef tData   (*tSetValuePtr)     (tLinkPtr const pThis, tData newVal, long lPos);

	enum {
		VERIFY_ON            = LINK_SELECTION_TYPE::VERIFY_ON,
		VERIFY_OFF           = LINK_SELECTION_TYPE::VERIFY_OFF,
		VERIFY_BINARY_SEARCH = LINK_SELECTION_TYPE::VERIFY_ON, // dummy to get the template formalism to work
		VERIFY_SCAN_ALL      = LINK_SELECTION_TYPE::VERIFY_ON  // the same
	};
	enum {
		DEFAULT_PRECISION = 0
	};
#ifndef BUILD_PLATFORM_LINUX
	static unsigned SolveTE_TR_TI(tLinkPtr const _this, char* arg_list[], const void* pAddMem, const MrProtocolData::MrProtData* pOrigProtData, long lIndex ){
		return ( fUICSolveSelectionConflict ( _this, arg_list, pAddMem, pOrigProtData, lIndex, NULL, NULL, NULL ) );  
	}
#endif // BUILD_PLATFORM_LINUX
};

#else //BUILD_PLATFORM_LINUX

#define MAX_PARAM_NAME	4
#define MAX_UNIT		4
#define MAX_TOOLTIP		4
#define MAX_OPTIONS		16
#define MAX_OPTIONS_STR 4
#define MAX_CUSTOM_TAG  4

#define TEMPLATE_SPEC template<>

typedef const char* LPCSTR;

#endif //BUILD_PLATFORM_LINUX

static const bool bALWAYS_TRUE = true;
static const bool bALWAYS_FALSE = false;
#define PM_INC_BASE2 -1
struct ParameterMapContext;
template<class T_Type>
struct TSingleParamStorage;

////////////////////////////////////////////////////////////////
// structures holding the parameter info for each parameter type

struct SParamCommon {
	long  nWIP;
	long  nID;
	char  szName[MAX_PARAM_NAME];
	char  szToolTip[MAX_TOOLTIP];
	char  szTag[MAX_CUSTOM_TAG];
	bool  bNeedInit;

// virtual member functions decalrations
	virtual ~SParamCommon() {};
#ifndef BUILD_PLATFORM_LINUX
	virtual bool Register(SeqLim* pSeqLim, const char* lpstrID) = 0;
	virtual void UpdateUI() = 0;

	const bool* pbShow;
	const bool* pbEnable;
#endif //BUILD_PLATFORM_LINUX
	virtual bool CheckProt(MrProt* pMrProt) = 0;
	virtual void InitProt(MrProt* pMrProt) = 0;
	virtual void UpdateProt(MrProt* pMrProt) = 0;
	virtual void UpdateItself(MrProt* pMrProt) = 0;
	virtual bool ProtDiffers(MrProt* pMrProt) = 0;
	virtual void dump() = 0;

	SParamCommon(long nSetWIP, long nSetID, LPCSTR szSetName, LPCSTR szSetToolTip, LPCSTR szSetTag) :
		nWIP(nSetWIP), nID(nSetID) 
#ifndef BUILD_PLATFORM_LINUX
		, pbShow(NULL), pbEnable(NULL)
#endif //BUILD_PLATFORM_LINUX
		{
			strncpy(szName, szSetName, MAX_PARAM_NAME);
			if (szSetToolTip)
				strncpy(szToolTip, szSetToolTip, MAX_TOOLTIP);
			else
				szToolTip[0] = '\0';
			if (szSetTag)
				strncpy(szTag, szSetTag, MAX_CUSTOM_TAG);
			else
				szTag[0] = '\0';
		}
private:
	SParamCommon();
};

struct SParamGroup{
	long  nWIP;
	char  szTag[MAX_CUSTOM_TAG];
#ifndef BUILD_PLATFORM_LINUX
	virtual bool Register(SeqLim* pSeqLim, const char* lpstrID) = 0;
	virtual void UpdateUI() = 0;
#endif //BUILD_PLATFORM_LINUX
	virtual void dump() = 0;
};

////////////////////////////////////////////////////////////////////////////
// protocol access template

template<class T_data>
struct TProtAccess {
	inline T_data _GetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID) {
		return pMrProtData->getsWipMemBlock().getalFree()[nID];
	}
	inline T_data _SetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID, const T_data& newVal){
		return (pMrProtData->getsWipMemBlock().getalFree()[nID] = newVal);
	}
	// compatibility
	inline T_data _GetProtVal(MrProt* pMrProt, long nID) {
		return pMrProt->getsWipMemBlock().getalFree()[nID];
	}
	inline T_data _SetProtVal(MrProt* pMrProt, long nID, const T_data& newVal){
		return (pMrProt->getsWipMemBlock().getalFree()[nID] = newVal);
	}
	TProtAccess(bool, int) {};
private:
	TProtAccess();
};

TEMPLATE_SPEC
struct TProtAccess<double> {
#ifndef BUILD_PLATFORM_LINUX
	double _GetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID) {
		return pMrProtData->getsWipMemBlock().getadFree()[nID];
	}
	inline double _SetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID, const double& newVal){
		return (pMrProtData->getsWipMemBlock().getadFree()[nID] = newVal);
	}
#endif //BUILD_PLATFORM_LINUX
	// compatibility
	double _GetProtVal(MrProt* pMrProt, long nID) {
		return pMrProt->getsWipMemBlock().getadFree()[nID];
	}
	inline double _SetProtVal(MrProt* pMrProt, long nID, const double& newVal){
		return (pMrProt->getsWipMemBlock().getadFree()[nID] = newVal);
	}
	TProtAccess(bool, int) {};
private:
	TProtAccess();
};

// now we only declare the specialisation here and will implement in after the PMCT 
TEMPLATE_SPEC
struct TProtAccess<bool> {
#ifndef BUILD_PLATFORM_LINUX
	inline bool _GetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID){
		if (!m_bCompactBoolsLocal)
			return (pMrProtData->getsWipMemBlock().getalFree()[nID] == PARAM_BOOL_TRUE);
		return 0!=((pMrProtData->getsWipMemBlock().getalFree()[nID>>5]>>(nID&31))&1);
	}

	inline bool _SetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID, const bool& newVal){
		if (!m_bCompactBoolsLocal) 
			pMrProtData->getsWipMemBlock().getalFree()[nID] = (newVal ? PARAM_BOOL_TRUE : PARAM_BOOL_FALSE);
		else
		{
			if (newVal)
				pMrProtData->getsWipMemBlock().getalFree()[nID>>5] |= (1<<(nID&31));
			else
				pMrProtData->getsWipMemBlock().getalFree()[nID>>5] &= (~(1<<(nID&31)));
			pMrProtData->getsWipMemBlock().getalFree()[nID>>5] |= (1<<31); // mark it as valid
		}
		return newVal;
	}
#endif //BUILD_PLATFORM_LINUX
	// compatibility
	inline bool _GetProtVal(MrProt* pMrProt, long nID){
		if (!m_bCompactBoolsLocal)
			return (pMrProt->getsWipMemBlock().getalFree()[nID] == PARAM_BOOL_TRUE);
		return 0!=((pMrProt->getsWipMemBlock().getalFree()[nID>>5]>>(nID&31))&1);
	}

	inline bool _SetProtVal(MrProt* pMrProt, long nID, const bool& newVal){
		if (!m_bCompactBoolsLocal) 
			pMrProt->getsWipMemBlock().getalFree()[nID] = (newVal ? PARAM_BOOL_TRUE : PARAM_BOOL_FALSE);
		else
		{
			if (newVal)
				pMrProt->getsWipMemBlock().getalFree()[nID>>5] |= (1<<(nID&31));
			else
				pMrProt->getsWipMemBlock().getalFree()[nID>>5] &= (~(1<<(nID&31)));
			pMrProt->getsWipMemBlock().getalFree()[nID>>5] |= (1<<31); // mark it as valid
		}
		return newVal;
	}

	inline bool _CheckProt(MrProt* pMrProt, long nID){
		if (!m_bCompactBoolsLocal) 
			return pMrProt->getsWipMemBlock().getalFree()[nID] == PARAM_BOOL_TRUE || pMrProt->getsWipMemBlock().getalFree()[nID] == PARAM_BOOL_FALSE;
		return 0!=(pMrProt->getsWipMemBlock().getalFree()[nID>>5]>>31);
	}

	bool m_bCompactBoolsLocal;
	TProtAccess(bool bCompactBools, int) : m_bCompactBoolsLocal(bCompactBools) {}
private:
	TProtAccess();
};

TEMPLATE_SPEC
struct TProtAccess<selection> {
#ifndef BUILD_PLATFORM_LINUX
	inline selection _GetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID){
		if (m_nCompactSelFactLocal<=1)
			return (pMrProtData->getsWipMemBlock().getalFree()[nID]);
		int nBits = 32/m_nCompactSelFactLocal;
		long nMask = (1<<nBits)-1;
		return (pMrProtData->getsWipMemBlock().getalFree()[nID>>5]>>(nBits*(nID&31)))&nMask;
	}

	inline selection _SetProtVal(MrProtocolData::MrProtData* pMrProtData, long nID, const selection& newVal){
		if (m_nCompactSelFactLocal<=1) 
			pMrProtData->getsWipMemBlock().getalFree()[nID] = newVal;
		else
		{
			int nBits = 32/m_nCompactSelFactLocal;
			long nMask = (1<<nBits)-1;
			int nPID = nID>>5;
			int nSID = nID&31;

			pMrProtData->getsWipMemBlock().getalFree()[nPID] &= ~(nMask<<(nBits*nSID));
			pMrProtData->getsWipMemBlock().getalFree()[nPID] |= (nMask&newVal)<<(nBits*nSID);
		}
		return newVal;
	}
#endif //BUILD_PLATFORM_LINUX
	// compatibility
	inline selection _GetProtVal(MrProt* pMrProt, long nID){
		if (m_nCompactSelFactLocal<=1)
			return (pMrProt->getsWipMemBlock().getalFree()[nID]);
		int nBits = 32/m_nCompactSelFactLocal;
		long nMask = (1<<nBits)-1;
		return (pMrProt->getsWipMemBlock().getalFree()[nID>>5]>>(nBits*(nID&31)))&nMask;
	}

	inline selection _SetProtVal(MrProt* pMrProt, long nID, const selection& newVal){
		if (m_nCompactSelFactLocal<=1) 
			pMrProt->getsWipMemBlock().getalFree()[nID] = newVal;
		else
		{
			int nBits = 32/m_nCompactSelFactLocal;
			long nMask = (1<<nBits)-1;
			int nPID = nID>>5;
			int nSID = nID&31;

			pMrProt->getsWipMemBlock().getalFree()[nPID] &= ~(nMask<<(nBits*nSID));
			pMrProt->getsWipMemBlock().getalFree()[nPID] |= (nMask&newVal)<<(nBits*nSID);
		}
		return newVal;
	}

	int m_nCompactSelFactLocal;
	TProtAccess(bool, int nCompactSelFact) : m_nCompactSelFactLocal(nCompactSelFact) {}
private:
	TProtAccess();
};

/////////////////////////////////////////////////////////////////////////////////////
// ParameterMapContextBase: an anbstact class needed to untangle dependencies
// but it also practical to let it to contain some data members

struct ParameterMapContextBase {
	///////////////////////////////////////////////
	// creation and protocol storage relevant flags
	bool m_bCompactBools;
	int  m_nCompactSelFact;
};

//////////////////////////////////////////////////////////////////////////////////////
// TParamCommon

template<class T_data>
struct TParamCommon : public SParamCommon, public TProtAccess<T_data> {
	typedef T_data               tData;
	typedef TParamCommon<T_data> tSelf;
	typedef tSelf*               tSelfPtr;

// general use data members
	T_data* pValue;
	T_data  valDef;

#ifndef BUILD_PLATFORM_LINUX
	typedef typename TN4I<T_data>::tLink    tLink;
	typedef typename TN4I<T_data>::tLinkPtr tLinkPtr;
	
	typename TN4I<T_data>::tTryPtr          pTry;
	typename TN4I<T_data>::tSolvePtr        pSolve;
	typename TN4I<T_data>::tGetToolTipIdPtr pToolTip;
	typename TN4I<T_data>::tGetValuePtr     pOurGetValue;
	typename TN4I<T_data>::tSetValuePtr     pOurSetValue;

	typename TN4I<T_data>::tLinkPtr pLink;
	MrUILinkArray* pDummyArray; // workaround the UI bug on the scanner

// member functions
	static TParamCommon<T_data>* FindByIndexOrLink(long lIndex, tLinkPtr const pL); // implementation follows at the very end
	static TParamCommon<T_data>* FindByArrayLink(MrUILinkArray* const pArray); // implementation follows at the very end
	static bool IsAvailable(tLinkPtr const pL, long lIndex){
		TParamCommon<T_data>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis)
			if (!pThis->pbShow || *pThis->pbShow)
				return true;
		return false;
	}

	static unsigned GetLabelId(tLinkPtr const pL, char* arg_list[], long lIndex){
		TParamCommon<T_data>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis)
		{
			arg_list[0] = pThis->szName;
			return MRI_STD_STRING;
		}
		return 0;
	}

	static unsigned GetToolTipId(tLinkPtr const pL, char* arg_list[], long lIndex){
		TParamCommon<T_data>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis && pThis->szToolTip[0] != '\0')
		{
			arg_list[0] = pThis->szToolTip;
			return MRI_STD_STRING;
		}
		return 0;
	}

	static long GetDummySize (MrUILinkArray* const _this, long /*nReserved*/){
		// workaround for the UI bug
		TParamCommon<T_data>* pThis = FindByArrayLink(_this);
		if (pThis)
			if (!pThis->pbShow || *pThis->pbShow)
				return 1;
		return 0;
	}

	void RegisterCommon(SeqLim* pSeqLim, const char* lpstrID){
		// workaround for the UI bug with the forgotten array arrows...
		pLink = NULL;
		pDummyArray = NULL;
		if (pDummyArray = _createArray<tLink>(pSeqLim, lpstrID, 1, pLink))
			pDummyArray->registerSizeHandler(GetDummySize);
		if (pLink || (pLink = _search< tLink >(pSeqLim, lpstrID)) )
		{
			pLink->registerGetLabelIdHandler  (GetLabelId);
			pLink->registerIsAvailableHandler (IsAvailable);
			if (pToolTip)
				pLink->registerGetToolTipIdHandler(pToolTip);
			else
				pLink->registerGetToolTipIdHandler(GetToolTipId);
			if (pTry)
				pLink->registerTryHandler(pTry);
			if (pSolve)
				pLink->registerSolveHandler(pSolve);
		}
	}

	virtual void UpdateUI() {
		if (!pLink)
			return;
		if (pbEnable)
			pLink->registerSetValueHandler( (*pbEnable) ? pOurSetValue : NULL);
		if (pbShow)
			pLink->registerGetValueHandler( (*pbShow) ? pOurGetValue : NULL);
	}

	static unsigned SolveTE_TR_TI(tLinkPtr const _this, char* arg_list[], const void* pAddMem, const MrProtocolData::MrProtData* pOrigProtData, long lIndex ){
		return TN4I<T_data>::SolveTE_TR_TI(_this, arg_list, pAddMem, pOrigProtData, lIndex);
	}

#endif //BUILD_PLATFORM_LINUX

#ifndef BUILD_PLATFORM_LINUX
	inline T_data GetProtVal(MrProtocolData::MrProtData* pMrProtData) { return _GetProtVal(pMrProtData, nID); }
	inline T_data SetProtVal(MrProtocolData::MrProtData* pMrProtData, const T_data& newVal) { return _SetProtVal(pMrProtData, nID, newVal); }
#endif //BUILD_PLATFORM_LINUX
	// compatibility
	inline T_data GetProtVal(MrProt* pMrProt) { return TProtAccess<T_data>::_GetProtVal(pMrProt, nID); }
	inline T_data SetProtVal(MrProt* pMrProt, const T_data& newVal) { return _SetProtVal(pMrProt, nID, newVal); }

	virtual void InitProt(MrProt* pMrProt){ SetProtVal(pMrProt, valDef); }
	virtual void UpdateProt(MrProt* pMrProt){ SetProtVal(pMrProt, *pValue); }
	virtual void UpdateItself(MrProt* pMrProt){ *pValue = GetProtVal(pMrProt); }
	virtual bool ProtDiffers(MrProt* pMrProt){ return *pValue != GetProtVal(pMrProt); }
	virtual void dump(){ std::cout << szName << "(" << nWIP << ", "  << nID << ") = " << *pValue << std::endl; }

	TParamCommon(long nSetWIP, long nSetID, LPCSTR szSetName, T_data& rValue, T_data valSetDef, LPCSTR szSetToolTip, LPCSTR szSetTag, ParameterMapContextBase* pPMC) : 
		SParamCommon(nSetWIP, nSetID, szSetName, szSetToolTip, szSetTag), TProtAccess<T_data>(pPMC->m_bCompactBools, pPMC->m_nCompactSelFact), pValue(&rValue), valDef(valSetDef)
#ifndef BUILD_PLATFORM_LINUX
		, pTry(NULL), pSolve(NULL), pToolTip(NULL), pOurGetValue(NULL), pOurSetValue(NULL),	pLink(NULL), pDummyArray(NULL) 
#endif //BUILD_PLATFORM_LINUX
		{}
private:
	TParamCommon();
};

// relatively general template for long and double

#ifndef BUILD_PLATFORM_LINUX
static unsigned PM_calcPrecisioin(double dVal)
{
	dVal=fabs(dVal);
	//cout << "PM: val=" << dVal;
	/*INT64*/ //typedef __int64 LONGLONG;
	__int64 llVal = dVal*1e7 + 0.5;
	int p=7;
	for (p=7; p>0 && ((llVal % 10)==0); --p)
	{
		//cout << " v:" << (int)llVal;
		llVal /= 10;
	}
	//cout << " p=" << p << std::endl;
	return p;
}
#endif //!BUILD_PLATFORM_LINUX

template<class T_data>
struct TParamTemplate : public TParamCommon<T_data> {
	T_data  valMin, valMax, valInc;
	char  szUnit[MAX_UNIT];

#ifndef BUILD_PLATFORM_LINUX
	// member function declaration
	typename typedef TN4I<T_data>::tMrLimit tMrLimit;
	unsigned long ulVerify;

	static unsigned GetPrecision(tLinkPtr const pL, long lIndex){
		if (!TN4I<T_data>::DEFAULT_PRECISION)
			return 0;
		TParamTemplate<T_data>* pThis = (TParamTemplate<T_data>*) FindByIndexOrLink(lIndex, pL);
		if (!pThis)
			return TN4I<T_data>::DEFAULT_PRECISION;
		unsigned nPMin = PM_calcPrecisioin(pThis->valMin);
		unsigned nPMax = PM_calcPrecisioin(pThis->valMax);
		unsigned nLims = (nPMin > nPMax) ? nPMin : nPMax;
		if (PM_INC_BASE2 == pThis->valInc)
			return nLims;
		unsigned nPInc = PM_calcPrecisioin(pThis->valInc);
		return (nLims > nPInc) ? nLims : nPInc;
	}

	static unsigned GetUnitId(tLinkPtr const pL, char* arg_list[], long lIndex){
		TParamTemplate<T_data>* pThis = (TParamTemplate<T_data>*) FindByIndexOrLink(lIndex, pL);
		if (pThis)
		{
			arg_list[0] = pThis->szUnit;
			return MRI_STD_STRING;
		}
		return 0;
	}

	static T_data GetValue(tLinkPtr const pL, long lIndex){
		TParamCommon<T_data>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis)
			return pThis->GetProtVal(&pL->prot());
		return -1;
	}

	static T_data SetValue(tLinkPtr const pL, T_data value, long lIndex){
		TParamCommon<T_data>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis)
			return (pThis->SetProtVal(&pL->prot(), value));
		return -1;
	}

	static bool GetLimits(tLinkPtr const pL, std::vector<tMrLimit>& rLimitVector, unsigned long& rulVerify, long lIndex){
		TParamTemplate<T_data>* pThis = (TParamTemplate<T_data>*) FindByIndexOrLink(lIndex, pL);
		if (!pThis)
			return false;
		
		rulVerify = pThis->ulVerify; 
		rLimitVector.resize(1);
		if (PM_INC_BASE2 == pThis->valInc)
			rLimitVector[0].setBase2(pThis->valMin, pThis->valMax);
		else
			rLimitVector[0].setEqualSpaced(pThis->valMin, pThis->valMax, pThis->valInc);
		return true;
	}

	virtual bool Register(SeqLim* pSeqLim, const char* lpstrID){
		RegisterCommon(pSeqLim, lpstrID);
		if (pLink)
		{
			pLink->registerGetValueHandler    (GetValue);
			pLink->registerSetValueHandler    (SetValue);
			pLink->registerGetLimitsHandler   (GetLimits);
			pLink->registerGetPrecisionHandler(GetPrecision);
			pLink->registerGetUnitIdHandler   (GetUnitId);
			pOurGetValue = GetValue;
			pOurSetValue = SetValue;
		}
		return pLink != NULL;
	}
#endif //BUILD_PLATFORM_LINUX
	virtual bool CheckProt(MrProt* pMrProt){ return TParamTemplate<T_data>::GetProtVal(pMrProt)!=0 || valMin <= 0; }

	TParamTemplate(long nSetWIP, long nSetID, LPCSTR szSetName, LPCSTR szSetUnit, T_data& rValue, T_data valSetMin, T_data valSetMax, T_data valSetInc, T_data valSetDef, LPCSTR szSetToolTip, LPCSTR szSetTag, ParameterMapContext* pPMC) : 
		TParamCommon<T_data>(nSetWIP, nSetID, szSetName, rValue, valSetDef, szSetToolTip, szSetTag, pPMC),  
		valMin(valSetMin), valMax(valSetMax), valInc(valSetInc) 
#ifndef BUILD_PLATFORM_LINUX
		,ulVerify(TN4I<T_data>::VERIFY_BINARY_SEARCH)
#endif //BUILD_PLATFORM_LINUX
		{
			//cout << "*** NewParam:" << szSetName << " wip:" << nSetWIP << " protID:" << nSetID << std::endl;
			strncpy(szUnit, szSetUnit, MAX_UNIT);
		}
private:
	TParamTemplate();
};

// now come specialisations
///////////////////////////

TEMPLATE_SPEC
struct TParamTemplate<bool> : public TParamCommon<bool> {
#ifndef BUILD_PLATFORM_LINUX
	unsigned long ulVerify;

	static bool GetValue(tLinkPtr const pL, long lIndex){
		TParamCommon<tData>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis)
			return pThis->GetProtVal(&pL->prot());
		return false;
	}

	static bool SetValue(tLinkPtr const pL, tData value, long lIndex){
		TParamCommon<tData>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis)
		{
			pThis->SetProtVal(&pL->prot(), value);
			return true;
		}
		return false;
	}

	static bool GetOptions(tLinkPtr const pL, std::vector<TN4I<tData>::tOption>& rOptionVector, unsigned long& rulVerify, long lIndex){
		TParamTemplate<tData>* pThis = (TParamTemplate<tData>*) FindByIndexOrLink(lIndex, pL);
		if (!pThis)
			return false;
		rulVerify = pThis->ulVerify;
		rOptionVector.resize(2);
		rOptionVector[0] = false;
		rOptionVector[1] = true;
		return true;
	}

	virtual bool Register(SeqLim* pSeqLim, const char* lpstrID){
		RegisterCommon(pSeqLim, lpstrID);
		if (pLink)
		{
			pLink->registerGetOptionsHandler  (GetOptions);
			pLink->registerGetValueHandler    (GetValue);
			pLink->registerSetValueHandler    (SetValue);
			pOurGetValue = GetValue;
			pOurSetValue = SetValue;
		}
		return pLink != NULL;
	}
#endif //BUILD_PLATFORM_LINUX
	virtual bool CheckProt(MrProt* pMrProt){ return _CheckProt(pMrProt, nID); }

	TParamTemplate(long nSetWIP, long nSetID, LPCSTR szSetName, bool& rValue, bool valSetDef, LPCSTR szSetToolTip, LPCSTR szSetTag, ParameterMapContextBase* pPMC) : 
		TParamCommon<bool>(nSetWIP, nSetID, szSetName, rValue, valSetDef, szSetToolTip, szSetTag, pPMC) 
#ifndef BUILD_PLATFORM_LINUX
		,ulVerify(TN4I<bool>::VERIFY_ON)
#endif //BUILD_PLATFORM_LINUX
	{}
private:
	TParamTemplate();
};

TEMPLATE_SPEC
struct TParamTemplate<selection> : public TParamCommon<selection> {
	long       nOptions;
	char       szzOptions[MAX_OPTIONS_STR]; // zero-separated list of options, ending with a double-zero
	int        nOptionIndex[MAX_OPTIONS];
	selection  aOptions[MAX_OPTIONS];

#ifndef BUILD_PLATFORM_LINUX
	unsigned long ulVerify;

	static int Format(tLinkPtr const pL, tData nID, char* arg_list[], long lIndex){
		TParamTemplate<tData>* pThis = (TParamTemplate<tData>*) FindByIndexOrLink(lIndex, pL);
		if (!pThis){
			std::cout << "!!!!!!! PARAM_SELECT-Format() : VERY STRANGE!\n";
			return 0;
		}
		unsigned uVal = GET_MODIFIER(nID);
		int i = 0;
		for (i = 0; i < pThis->nOptions; ++i){
			if (pThis->aOptions[i] == uVal){
				arg_list[0] = pThis->szzOptions + pThis->nOptionIndex[i];
				return 1;
			}
		}
		std::cout << "PARAM_SELECT-Format() : SHOULD NOT HAPPEN!\n";
		return i;
	}
 
	static bool GetOptions(tLinkPtr const pL, std::vector<TN4I<tData>::tOption>& rOptionVector, unsigned long& rulVerify, long lIndex){
		TParamTemplate<tData>* pThis = (TParamTemplate<tData>*) FindByIndexOrLink(lIndex, pL);
		if (!pThis)
			return false;
		rulVerify = pThis->ulVerify;
		rOptionVector.resize(pThis->nOptions);
		for (long i=0; i<pThis->nOptions; ++i){
			rOptionVector[i] = MRI_STD_STRING;
			SET_MODIFIER(rOptionVector[i], pThis->aOptions[i]);
		}
		return true;
	}

	static selection GetValue(tLinkPtr const pL, long lIndex){
		TParamCommon<tData>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis){
			unsigned nRet = MRI_STD_STRING;
			SET_MODIFIER(nRet, pThis->GetProtVal(&pL->prot()));
			return nRet;
		}
		return 0;
	}
	
	static selection SetValue(tLinkPtr const pL, tData nNewVal, long lIndex){
		TParamCommon<tData>* pThis = FindByIndexOrLink(lIndex, pL);
		if (pThis)
		{
			pThis->SetProtVal(&pL->prot(), GET_MODIFIER(nNewVal));
			return pL->value(lIndex); // ?????
		}
		return 0;
	}

	virtual bool Register(SeqLim* pSeqLim, const char* lpstrID){
		RegisterCommon(pSeqLim, lpstrID);
		if (pLink){
			pLink->registerGetOptionsHandler  (GetOptions);
			pLink->registerGetValueHandler    (GetValue);
			pLink->registerSetValueHandler    (SetValue);
			pLink->registerFormatHandler      (Format);
			pOurGetValue = GetValue;
			pOurSetValue = SetValue;
		}
		return pLink != NULL;
	}
#endif //BUILD_PLATFORM_LINUX 
//TEMPLATE_SPEC
	virtual bool CheckProt(MrProt* pMrProt){
		if (GetProtVal(pMrProt)) // it is a somewhat lazy check...
			return true;
		for (int i = 0; i < nOptions; ++i)
			if (!aOptions[i])
					return true;
		return false;
	}

	virtual void dump() {
		char* szOpt = "<error>";
		for (int i = 0; i < nOptions; ++i){
			if (aOptions[i] == *pValue){
				szOpt = szzOptions + nOptionIndex[i];
				break;
			}
		}
		std::cout << szName << "(" << nWIP << ", "  << nID << ") = " << szOpt << "(" << *pValue << ")" << std::endl;
	}

	void AddOption(LPCSTR szOption, selection nValue){
		if (nOptions+1>=MAX_OPTIONS) {
			std::cout << "PM ERROR: trying to create too many options for a selection\n";
			return;
		}
		aOptions[nOptions++] = nValue;
		for(int i = 1; i<MAX_OPTIONS_STR-1; ++i){
			if (szzOptions[i-1] == '\0' && szzOptions[i] == '\0'){
				if (i == 1) i = 0;
				strncpy(szzOptions+i, szOption, MAX_OPTIONS_STR-1-i);
				nOptionIndex[nOptions-1] = i;
				i+=strlen(szOption);
				if (i >= MAX_OPTIONS_STR-1)
					szzOptions[MAX_OPTIONS_STR-1] = szzOptions[MAX_OPTIONS_STR-2] = '\0';
				else
					szzOptions[i] = szzOptions[i+1] = '\0';
				//cout << "AddOption() : " << nOptionIndex[nOptions-1] <<std::endl;
				return;
			}
		}
	}

	TParamTemplate(long nSetWIP, long nSetID, LPCSTR szSetName, selection& rValue, selection valSetDef, LPCSTR szSetToolTip, LPCSTR szSetTag, ParameterMapContextBase* pPMC) : 
		TParamCommon<selection>(nSetWIP, nSetID, szSetName, rValue, valSetDef, szSetToolTip, szSetTag, pPMC), nOptions(0) 
#ifndef BUILD_PLATFORM_LINUX
		,ulVerify(TN4I<selection>::VERIFY_ON)
#endif //BUILD_PLATFORM_LINUX
		{
			szzOptions[0] = szzOptions[1] = '\0';
			nOptionIndex[0] = 0;
		}
private:
	TParamTemplate();
};

template<class T_data>
struct TParamArrayTemplate : public TParamCommon<T_data> {
	long    nSize;
	long    nVisibleSize;
	const T_data*  pMin;
	const T_data*  pMax;
	const T_data*  pInc;
	const T_data*  pDef;
	char  szUnit[MAX_UNIT];
#ifndef BUILD_PLATFORM_LINUX
	typedef typename TN4I<T_data>::tLink    tLink;
	typedef typename TN4I<T_data>::tLinkPtr tLinkPtr;
	typedef typename TN4I<T_data>::tMrLimit tMrLimit;
	typedef typename TParamCommon<T_data>*  tParamPtr;

	MrUILinkArray* puiArray;
	unsigned long ulVerify;

	static TParamArrayTemplate < T_data > * FindByArrayLink(MrUILinkArray* puiArray); // implementation follows at the very end
	static TParamArrayTemplate < T_data > * FindByElementLink(tLinkPtr const pLink); // implementation follows at the very end

	// handler function implementation
	static long GetSize(MrUILinkArray* const _this, long){
		TParamArrayTemplate < T_data > * pThis = FindByArrayLink(_this);
		if (pThis && (!pThis->pbShow || *pThis->pbShow))
			return (pThis->nSize < pThis->nVisibleSize) ? pThis->nSize : pThis->nVisibleSize;
		return 0;
	}

	static unsigned GetLabelId(tLinkPtr const _this, char* arg_list[], long lIndex){
		TParamArrayTemplate < T_data > * pThis = FindByElementLink(_this);
		if (pThis && (!pThis->pbShow || *pThis->pbShow)){
			static char szTmp[MAX_PARAM_NAME + 16];
			sprintf(szTmp, "%s[%d]", pThis->szName, lIndex + 1);
			arg_list[0] = szTmp;
			return MRI_STD_STRING;
		}
		return 0;
	}

	static unsigned GetUnitId(tLinkPtr const _this, char* arg_list[], long){
		TParamArrayTemplate < T_data > * pThis = FindByElementLink(_this);
		if (pThis && (!pThis->pbShow || *pThis->pbShow)){
			arg_list[0] = pThis->szUnit;
			return MRI_STD_STRING;
		}
		return 0;
	}

	static unsigned GetToolTipId(tLinkPtr const _this, char* arg_list[], long){
		TParamArrayTemplate < T_data > * pThis = FindByElementLink(_this);
		if (pThis && (!pThis->pbShow || *pThis->pbShow)){
			arg_list[0] = pThis->szToolTip;
			return MRI_STD_STRING;
		}
		return 0;
	}

	static bool IsAvailable(tLinkPtr const, long){ return true; }
	
	static bool GetLimits(tLinkPtr const _this, std::vector<tMrLimit>& rLimitVector, unsigned long& rulVerify, long lIndex){
		TParamArrayTemplate < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			if (lIndex >= pThis->nSize || lIndex < 0)
				return false;

			rulVerify = pThis->ulVerify;
			rLimitVector.resize(1);
			if (PM_INC_BASE2 == pThis->pInc[lIndex]){
				rLimitVector[0].setBase2(pThis->pMin[lIndex], pThis->pMax[lIndex]);
			}
			else{
				rLimitVector[0].setEqualSpaced(pThis->pMin[lIndex], pThis->pMax[lIndex], pThis->pInc[lIndex]);
			}
			return true;
		}
		return false;
	}

	static unsigned GetPrecision(tLinkPtr const _this, long lIndex){
		if (!TN4I<T_data>::DEFAULT_PRECISION)
			return 0;
		TParamArrayTemplate <T_data> * pThis = FindByElementLink(_this);
		if (!pThis)
			return TN4I<T_data>::DEFAULT_PRECISION;
		unsigned nPMin = PM_calcPrecisioin(pThis->pMin[lIndex]);
		unsigned nPMax = PM_calcPrecisioin(pThis->pMax[lIndex]);
		unsigned nLims = (nPMin > nPMax) ? nPMin : nPMax;
		if (PM_INC_BASE2 == pThis->pInc[lIndex])
			return nLims;
		unsigned nPInc = PM_calcPrecisioin(pThis->pInc[lIndex]);
		return (nLims > nPInc) ? nLims : nPInc;
	}

	static T_data GetValue(tLinkPtr const _this, long lIndex){
		TParamArrayTemplate < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			if (lIndex >= pThis->nSize || lIndex < 0)
				return -1.0;
			return pThis->_GetProtVal(&_this->prot(), pThis->nID + lIndex);
		}
		return -1.0;
	}

	static T_data SetValue(tLinkPtr const _this, T_data value, long lIndex){
		TParamArrayTemplate < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			if (lIndex >= pThis->nSize || lIndex < 0)
				return -1.0;
			return pThis->_SetProtVal(&_this->prot(), pThis->nID + lIndex, value);
		}
		return -1.0;
	}

	bool RegisterArray(SeqLim* pSeqLim, const char* lpstrID, long nSize, tParamPtr pParam){
		tLinkPtr pElm = NULL;
		if( (puiArray = _createArray<tLink>(pSeqLim, lpstrID, nSize, pElm)) ||
			((puiArray = _search<MrUILinkArray>(pSeqLim, lpstrID)) && (pElm=_searchElm<tLink>(pSeqLim, lpstrID)) ) )
		{
			puiArray->registerSizeHandler(GetSize);
			pElm->registerGetLabelIdHandler  (GetLabelId  );
			pElm->registerGetUnitIdHandler   (GetUnitId   );
			pElm->registerIsAvailableHandler (IsAvailable );
			pElm->registerGetLimitsHandler   (GetLimits   );
			pElm->registerGetPrecisionHandler(GetPrecision);
			pElm->registerGetValueHandler    (GetValue    );
			pElm->registerSetValueHandler    (SetValue    );
			if (pParam->pToolTip)
				pElm->registerGetToolTipIdHandler(pParam->pToolTip);
			else
				pElm->registerGetToolTipIdHandler(GetToolTipId);

			if (pParam->pTry)
				pElm->registerTryHandler(pParam->pTry);
			if (pParam->pSolve)
				pElm->registerSolveHandler(pParam->pSolve);
			
			pParam->pLink = pElm;
		}
		return pLink != NULL && puiArray != NULL;
	}

	virtual bool Register(SeqLim* pSeqLim, const char* lpstrID){
		RegisterArray(pSeqLim, lpstrID, nSize, this);
		pOurGetValue = GetValue;
		pOurSetValue = SetValue;
		return pLink != NULL && puiArray != NULL;
	}
#endif //BUILD_PLATFORM_LINUX

	TParamArrayTemplate(long nSetWIP, long nSetID, LPCSTR szSetName, LPCSTR szSetUnit, long nSetSize, T_data* pSetValue, const T_data* pSetMin, const T_data* pSetMax, const T_data* pSetInc, const T_data* pSetDef, LPCSTR szSetToolTip, LPCSTR szSetTag, ParameterMapContext* pPMC) : 
		TParamCommon<T_data>(nSetWIP, nSetID, szSetName, *pSetValue, (T_data)/*dummy*/0, szSetToolTip, szSetTag, pPMC), 
		nSize(nSetSize), nVisibleSize(nSetSize), 
		pMin(pSetMin), pMax(pSetMax), pInc(pSetInc), pDef(pSetDef)
#ifndef BUILD_PLATFORM_LINUX
		, puiArray(NULL) 
		, ulVerify(TN4I<T_data>::VERIFY_BINARY_SEARCH)
#endif //BUILD_PLATFORM_LINUX
		{
			strncpy(szUnit, szSetUnit, MAX_UNIT);
		}
private:
	TParamArrayTemplate();

public:

	virtual bool CheckProt(MrProt* pMrProt){
		for (int i = 0; i < this->nSize; ++i)
			if (TProtAccess<T_data>::_GetProtVal(pMrProt, this->nID+i) == 0 && this->pMin[i] > 0)
				return false;
		return true;
	}

	virtual void InitProt(MrProt* pMrProt){
		for (int i = 0; i < this->nSize; ++i)
			TProtAccess<T_data>::_SetProtVal(pMrProt, this->nID+i, this->pDef[i]);
	}

	virtual void UpdateProt(MrProt* pMrProt){
		for (int i = 0; i < this->nSize; ++i)
			TProtAccess<T_data>::_SetProtVal(pMrProt, this->nID+i, this->pValue[i]);
	}

	virtual void UpdateItself(MrProt* pMrProt){
		for (int i = 0; i < this->nSize; ++i)
			this->pValue[i] = TProtAccess<T_data>::_GetProtVal(pMrProt, this->nID+i);
	}

	virtual bool ProtDiffers(MrProt* pMrProt){
		for (int i = 0; i < this->nSize; ++i)
			if (this->pValue[i] != TProtAccess<T_data>::_GetProtVal(pMrProt, this->nID+i))
				return true;
		return false;
	}

	virtual void dump(){
		std::cout << this->szName << "(" << this->nWIP << ", "  << this->nID << ") [" << this->nSize << ", " << this->nVisibleSize << "] = [ ";
		for (int i = 0; i < this->nSize; ++i)
			std::cout << this->pValue[i] << " ";
		std::cout << "]\n";
	}

/*	TParamTemplate(long nSetWIP, long nSetID, LPCSTR szSetName, LPCSTR szSetUnit, long nSetSize, T_data pSetValue, const T_data pSetMin, const T_data pSetMax, const T_data pSetInc, const T_data pSetDef, LPCSTR szSetToolTip = NULL) : 
		TParamCommon<long>(nSetWIP, nSetID, szSetName, szSetToolTip), 
		TParamArrayTemplate<long>(szSetUnit, nSetSize, pSetValue, pSetMin, pSetMax, pSetInc, pSetDef) {}
private:
	TParamTemplate();*/

};

// two dummy specialisations, which are never used but needed for compilation
TEMPLATE_SPEC
struct TParamArrayTemplate<bool> : public TParamCommon<bool> {
#ifndef BUILD_PLATFORM_LINUX
	unsigned long ulVerify;
#endif //BUILD_PLATFORM_LINUX
private:
	TParamArrayTemplate();
};
TEMPLATE_SPEC
struct TParamArrayTemplate<selection> : public TParamCommon<selection> {
#ifndef BUILD_PLATFORM_LINUX
	unsigned long ulVerify;
#endif //BUILD_PLATFORM_LINUX
private:
	TParamArrayTemplate();
};

// group template, something very special...

template<class T_data>
struct TParamGroupTemplateBase : public SParamGroup{
	long  nID;
	long  nSize;
#ifndef BUILD_PLATFORM_LINUX
	typedef typename TN4I<T_data>::tLink     tLink;
	typedef typename TN4I<T_data>::tLinkPtr  tLinkPtr;
	typedef typename TN4I<T_data>::tMrLimit  tMrLimit;
	typedef typename TParamTemplate<T_data>* tParamPtr;
	typename TN4I<T_data>::tGetValuePtr      pOurGetValue;
	typename TN4I<T_data>::tSetValuePtr      pOurSetValue;

	MrUILinkArray* puiArray;
	tLink*         pLink;

	//
	static TParamGroupTemplateBase < T_data >* FindByElementLink(tLinkPtr const pLink); // implementation follows at the very end
	static TParamTemplate<T_data>* FindByIndex(int nIndex, tLinkPtr const pLink); // implementation follows at the very end
	static TParamGroupTemplateBase < T_data >* FindByArrayLink(MrUILinkArray* const pArray); // implementation follows at the very end

	// group template implemention
	long GetVisibleSize(){
		if(!pLink) {
			std::cout << "*** PM WARNING: findParam() GetVisibleSize with pLink=NULL" << std::endl;
			return 0;
		}
		long nS = 0;
		for (long i = 0; i< nSize; ++i){
			TParamTemplate<T_data>* pThis = FindByIndex(nID+i, pLink);
			if (pThis && (pThis->pbShow==NULL || *pThis->pbShow))
				++nS;
		}
		return nS;
	}

	virtual void UpdateUI(){ 
		if (!pLink)
			return;
		pLink->registerGetValueHandler( GetVisibleSize() ? pOurGetValue : NULL);
	}

	TParamTemplate<T_data>* findParam(long lIndex0){
		if(!pLink) {
			std::cout << "*** PM WARNING: findParam() called with pLink=NULL" << std::endl;
			return NULL;
		}
		long lIndex = lIndex0;
		//cout << "**** findParam(), lIndex = " << lIndex << " nWIP=" << nWIP << " nID=" << nID << " size=" << nSize << std::endl;
		for (long i = 0; i< nSize; ++i){
			TParamTemplate<T_data>* pThis = FindByIndex(nID+i, pLink);
			if (pThis && (pThis->pbShow==NULL || *pThis->pbShow)){
				if (lIndex == 0)
					return pThis;
				--lIndex;
			}
		}
		std::cout << "*** PM WARNING: findParam() failed, lIndex = " << lIndex0 << " nWIP=" << nWIP << " nID=" << nID << " size=" << nSize << std::endl;
		return NULL;
	}

	static long GetSize(MrUILinkArray* const _this, long /*nReserved*/){
		TParamGroupTemplateBase < T_data > * pThis = FindByArrayLink(_this);
		if (pThis){
			//cout << "*** TParamGroupTemplateBase<T_data>::GetSize(): nWIP="<< pThis->nWIP <<" szTag='"<< pThis->szTag << "'\n";
			return pThis->GetVisibleSize();
		}
		return 0;
	}

	static unsigned GetLabelId(tLinkPtr const _this, char* arg_list[], long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			//cout << "*** TParamGroupTemplateBase<T_data>::GetLabelId(): nWIP="<< pThis->nWIP <<" szTag='"<< pThis->szTag << "'\n";
			//cout << "*** calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam){
				arg_list[0] = pParam->szName;
				return MRI_STD_STRING;
			}
		}
		std::cout << "!!!!! GetLabelId(): should not happen!" << std::endl;
		return 0;
	}

	static unsigned GetUnitId(tLinkPtr const _this, char* arg_list[], long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			//cout << "GetUnitId: calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam){
				if (pParam->pbShow==NULL || *pParam->pbShow){
					arg_list[0] = pParam->szUnit;
					return MRI_STD_STRING;
				}
				else
					return 0;
			}
		}
		std::cout << "!!!!! GetUnitId(): should not happen!" << std::endl;
		return 0;
	}

	static unsigned GetToolTipId(tLinkPtr const _this, char* arg_list[], long){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			static char szCommonToolTip[8096];
			long nLength = 0;
			// todo: align the colons: we will need to know the maximum name length
			for (long i = 0; i<pThis->nSize; ++i){
				TParamTemplate < T_data > * pParam = FindByIndex(pThis->nID+i, _this);  // PROBLEM: here is the problem with nID for custom params
				if (pParam && strlen(pParam->szToolTip) && (!pParam->pbShow || *pParam->pbShow) ){
					_snprintf(szCommonToolTip+nLength, 8096 - nLength, "%s%s : %s", nLength ? "\n\n" : "", pParam->szName, pParam->szToolTip);
					nLength = strlen(szCommonToolTip);
				}
			}
			if (nLength){
				arg_list[0] = szCommonToolTip;
				return MRI_STD_STRING;
			}
		}
		return 0;
	}

	static bool IsAvailable(tLinkPtr const /*_this*/, long){ return true; }

	static bool GetLimits(tLinkPtr const _this, std::vector<tMrLimit>& rLimitVector, unsigned long& rulVerify, long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis) 
		{
			//cout << "GetLimits: calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam)
			{
				rulVerify = pParam->ulVerify; 
				rLimitVector.resize(1);
				if (PM_INC_BASE2 == pParam->valInc)
					rLimitVector[0].setBase2(pParam->valMin, pParam->valMax);
				else
					rLimitVector[0].setEqualSpaced(pParam->valMin, pParam->valMax, pParam->valInc);
				return true;
			}
		}
		std::cout << "!!!!! GetLimits(): should not happen!" << std::endl;
		return false;
	}

	static unsigned GetPrecision(tLinkPtr const _this, long lIndex){
		if (!TN4I<T_data>::DEFAULT_PRECISION)
			return 0;
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (!pThis)
			return TN4I<T_data>::DEFAULT_PRECISION;
		TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
		if (!pParam)
			return TN4I<T_data>::DEFAULT_PRECISION;
		
		unsigned nPMin = PM_calcPrecisioin(pParam->valMin);
		unsigned nPMax = PM_calcPrecisioin(pParam->valMax);
		unsigned nLims = (nPMin > nPMax) ? nPMin : nPMax;
		if (PM_INC_BASE2 == pParam->valInc)
			return nLims;
		unsigned nPInc = PM_calcPrecisioin(pParam->valInc);
		return (nLims > nPInc) ? nLims : nPInc;
	}


	static bool GetOptions(tLinkPtr const _this, std::vector<typename TN4I<T_data>::tOption>& rOptionVector, unsigned long& rulVerify, long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			//cout << "GetOptions: calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam)
				return pParam->GetOptions(_this, rOptionVector, rulVerify, pParam->nID);
		}
		std::cout << "!!!!! GetOptions(): should not happen!" << std::endl;
		return false;
	}

	static int Format(tLinkPtr const _this, T_data value, char* arg_list[], long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis) {
			//cout << "GetFormat: calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam)
				return pParam->Format(_this, value, arg_list, pParam->nID);
		}
		std::cout << "!!!!! Format(): should not happen!" << std::endl;
		return 0;
	}

	static T_data GetValue(tLinkPtr const _this, long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			//cout << "GetValue: calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam)
				return TParamTemplate<T_data>::GetValue(_this, pParam->nID);
			/*else // quick fix, see the comment below
				if (pThis->nID<=lIndex && pThis->nID+pThis->nSize>lIndex)
				{
					//cout << "GetValue: trying agan calling findParam(), lIndex = " << 0 << std::endl;
					pParam = pThis->findParam(lIndex-pThis->nID);
					if (pParam)
						return TParamTemplate<T_data>::GetValue(_this, pParam->nID);
				}*/
		}
		// mz: bloody hell, it happens... time and again : lIndex == nID...
		// have no idea where it's coming from. I've only observed it with selection grous
		//cout << "PM_ERROR: GetValue(): for the parameter group failed, << lIndex=" << lIndex << " pThis->nID=" << pThis->nID << std::endl;
		return (T_data) -1;
	}

	static T_data SetValue(tLinkPtr const _this, T_data value, long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			//cout << "SetValue: calling findParam(), lIndex = " << lIndex << endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam)
				return TParamTemplate<T_data>::SetValue(_this, value, pParam->nID);
		}
		std::cout << "!!!!! SetValue(): should not happen!" << std::endl;
		return (T_data) -1;
	}

	static bool TryHandler (tLinkPtr const _this, void* pAddMem, const MrProtocolData::MrProtData* pMrProtData, long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			//cout << "TryHandler: calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam && pParam->pTry)
				return (*pParam->pTry)(_this, pAddMem, pMrProtData, pParam->nID);
		}
		return MrUILinkBase::stdTryHandler(_this, pAddMem, pMrProtData, lIndex);
	}

	static unsigned SolveHandler (tLinkPtr const _this, char** arg_list, const void* pAddMem, const MrProtocolData::MrProtData* pMrProtData, long lIndex){
		TParamGroupTemplateBase < T_data > * pThis = FindByElementLink(_this);
		if (pThis){
			//cout << "SolveHandler: calling findParam(), lIndex = " << lIndex << std::endl;
			TParamTemplate < T_data > * pParam = pThis->findParam(lIndex);
			if (pParam && NULL != pParam->pSolve)
				return (*pParam->pSolve)(_this, arg_list, pAddMem, pMrProtData, pParam->nID);
		}
		return 0;//MrUILinkBase::stdSolveHandler(_this, arg_list, pAddMem, pMrProtData, lIndex);
	}

	virtual void RegisterHelper(tLinkPtr pElm) = 0;
	virtual bool Register(SeqLim* pSeqLim, const char* lpstrID){
		tLinkPtr pElm = NULL;
		if( (puiArray = _createArray<tLink>(pSeqLim, lpstrID, nSize, pElm)) ||
			((puiArray = _search<MrUILinkArray>(pSeqLim, lpstrID)) && (pElm=_searchElm<tLink>(pSeqLim, lpstrID))) ){
			puiArray->registerSizeHandler(GetSize);

			pElm->registerGetLabelIdHandler  (GetLabelId  );
			pElm->registerIsAvailableHandler (IsAvailable );
			//pElm->registerGetUnitIdHandler   (GetUnitId   ); // belongs to RegisterHelper() of long and double
			//pElm->registerGetLimitsHandler   (GetLimits   ); // belongs to RegisterHelper() of long and double
			//pElm->registerGetPrecisionHandler(GetPrecision); // belongs to RegisterHelper() of long and double
			//pElm->registerGetOptionsHandler  (GetOptions  ); // belongs to RegisterHelper() of bool and selection
			//pElm->registerFormatHandler      (Format); // belongs to RegisterHelper() of selection
			pElm->registerGetValueHandler    (GetValue    );
			pElm->registerSetValueHandler    (SetValue    );
			pElm->registerGetToolTipIdHandler(GetToolTipId);
			RegisterHelper(pElm);

			pElm->registerTryHandler         (TryHandler  );
			pElm->registerSolveHandler       (SolveHandler);

			pOurGetValue = GetValue;
			pOurSetValue = SetValue;
			
			pLink = pElm;
		}
		return pLink != NULL && puiArray != NULL;
	}

#endif //BUILD_PLATFORM_LINUX

	TParamGroupTemplateBase(long nSetWIP, long nSetID, LPCSTR szSetTag) : 
		nID(nSetID), nSize(0)
#ifndef BUILD_PLATFORM_LINUX
		, puiArray(NULL), pLink(NULL), pOurGetValue(NULL), pOurSetValue(NULL)
#endif //BUILD_PLATFORM_LINUX
		{
			//cout << "*** NewGroup, wip:" << nSetWIP << " protID:" << nSetID << std::endl;
			nWIP = nSetWIP;
			if (szSetTag)
				strncpy(szTag, szSetTag, MAX_CUSTOM_TAG);
			else
				szTag[0] = '\0';

		}

	void Add(SParamCommon* pP) { 
		if (nID<0 && nSize==0)
			nID=pP->nID;
		++nSize; 
	}
	void setWIP(long n) { nWIP = n; }
	virtual void dump()	{ std::cout << "group: " << "(" << nWIP << ", "  << nID << ") : " << nSize << std::endl; }
private:
	TParamGroupTemplateBase();
};

// now we need those specialisations, the only reason is the register function

template<class T_data>
struct TParamGroupTemplate : public TParamGroupTemplateBase<T_data>
{
#ifndef BUILD_PLATFORM_LINUX
	virtual void RegisterHelper(tLinkPtr pElm){
		pElm->registerGetUnitIdHandler   (GetUnitId   );
		pElm->registerGetLimitsHandler   (GetLimits   );
		pElm->registerGetPrecisionHandler(GetPrecision);
	}
#endif
	TParamGroupTemplate(long nSetWIP, long nSetID, LPCSTR szSetTag) : TParamGroupTemplateBase<T_data>(nSetWIP, nSetID, szSetTag) {}
private:
	TParamGroupTemplate();
};

#ifndef BUILD_PLATFORM_LINUX

template<>
struct TParamGroupTemplate<bool> : public TParamGroupTemplateBase<bool>
{
	virtual void RegisterHelper(tLinkPtr pElm){
		pElm->registerGetOptionsHandler  (GetOptions  );
	}
	TParamGroupTemplate(long nSetWIP, long nSetID, LPCSTR szSetTag) : TParamGroupTemplateBase<bool>(nSetWIP, nSetID, szSetTag) {}
private:
	TParamGroupTemplate();
};

template<>
struct TParamGroupTemplate<selection> : public TParamGroupTemplateBase<selection>
{
	virtual void RegisterHelper(tLinkPtr pElm){
		pElm->registerGetOptionsHandler  (GetOptions  );
		pElm->registerFormatHandler      (Format);
	}
	TParamGroupTemplate(long nSetWIP, long nSetID, LPCSTR szSetTag) : TParamGroupTemplateBase<selection>(nSetWIP, nSetID, szSetTag) {}
private:
	TParamGroupTemplate();
};

#endif //!BUILD_PLATFORM_LINUX

// storage classes

template<class T_Type>
struct TSingleParamStorage {
	typedef TParamTemplate < T_Type > tParam;
	typedef TSimpleList  < tParam >   tList;
	typedef TSimpleIndex < tParam >   tIndex;

	tParam * FindByIndex(long nIndex) {
		//return ms_index.find(nIndex);
		tParam * pP = m_index.find(nIndex);
		//if (pP)
			//cout << "**** FindByIndex(): nIndex="<<nIndex<<" sName="<<pP->szName<<std::endl;
		//else
			//cout << "**** FindByIndex(): failed to find an entry, nIndex="<<nIndex<<std::endl;
		return pP;
		
	}

#ifndef BUILD_PLATFORM_LINUX
	typedef typename TN4I<T_Type>::tLinkPtr tLinkPtr;

	tParam * FindByLink(tLinkPtr pElm) {
		for (tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->pLink == pElm)
				return it;
		//cout << "**** FindByLink(): failed to find an entry, pL=0x"<<pElm<<std::endl;
		return NULL;
	}

	tParam * FindByArrayLink(MrUILinkArray* puiArray) {
		// this will be kinda slow but we don't have a better alternative... unless we start using maps or indexes...
		for (tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->pDummyArray == puiArray)
				return it;
		return NULL;
	}

	/*static int FindAllByTag(LPCSTR szGrpTag, tParam** apParams, int nArraySize) {
		if (nArraySize <= 0)
			return -1;
		int n=0;
		for (tList::iterator it = ms_list.begin(); it != ms_list.end(); ++it)
			if (0==strcmp(szGrpTag, it->szTag))
			{
				apParams[n++]=it;
				if (n == nArraySize)
					return n; // this will save looping
			}
		return n;
	}*/

	/*static tParam * FindByIndexOrLink(long nIndex, tLinkPtr pL) {
		cout << "**** FindByIndexOrLink(): nIndex="<<nIndex<<" pL=0x"<<pL<<std::endl;
		tParam * pThis = NULL;
		if (nIndex)
			pThis = FindByIndex(nIndex);
		if (pThis && (pThis->pLink == NULL || pThis->pLink == pL))
			return pThis;
		return FindByLink(pL);
	}*/
	tParam * FindByIndexOrLink(long nIndex, tLinkPtr pL) {
		//cout << "**** FindByIndexOrLink(): nIndex="<<nIndex<<" pL=0x"<<pL<<std::endl;
		tParam * pThis = NULL;
		if (pL)
			pThis = FindByLink(pL);
		if (pThis)
			return pThis;
		return FindByIndex(nIndex);
	}

#endif //BUILD_PLATFORM_LINUX

	tParam * FindByValuePtr(const T_Type* pVal) {
		for (typename tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->pValue == pVal)
				return it;
		return NULL;
	}

	void Add(tParam& param) {
		m_list.push_front(param);
	}

	void BuildIndex() {
		m_index.build(m_list);
	}

	void Dump() {
		for (typename tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
		{
			it->dump();
			//cout << it->szName << " [ pLink=0x" << it->pLink << " pArrayLink=0x" << it->pDummyArray << "]\n";
		}
	}

	tList  m_list;
	tIndex m_index;
};


template<class T_Type>
struct TArrayParamStorage {
	typedef TParamArrayTemplate < T_Type > tParam;
	typedef TSimpleList< tParam >          tList;

#ifndef BUILD_PLATFORM_LINUX
	typedef typename TN4I<T_Type>::tLinkPtr tLinkPtr;
	tParam * FindByArrayLink(MrUILinkArray* puiArray) {
		// this will be kinda slow if we have many arrays, but normally we do not
		for (tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->puiArray == puiArray)
				return it;
		return NULL;
	}

	tParam * FindByElementLink(tLinkPtr pElm) {
		// this will be kinda slow if we have many arrays, but normally we do not
		for (tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->pLink == pElm)
				return it;
		return NULL;
	}

#endif //BUILD_PLATFORM_LINUX

	tParam * FindByValuePtr(const T_Type* pVal) {
		// this will be kinda slow if we have many arrays, but normally we do not
		for (typename tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->pValue == pVal)
				return it;
		return NULL;
	}

	void Add(tParam& param) {
		m_list.push_front(param);
	}

	tList m_list;
};

template<class T_Type>
struct TGroupParamStorage {
	typedef TParamGroupTemplate < T_Type > tParam;
	typedef TSimpleList< tParam >          tList;

#ifndef BUILD_PLATFORM_LINUX
	typedef typename TN4I<T_Type>::tLinkPtr tLinkPtr;
	tParam * FindByArrayLink(MrUILinkArray* puiArray) {
		// this will be kinda slow if we have many groups, but normally we do not
		for (tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->puiArray == puiArray)
				return it;
		return NULL;
	}

	tParam * FindByElementLink(tLinkPtr pElm) {
		// this will be kinda slow if we have many groups, but normally we do not
		for (tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->pLink == pElm)
				return it;
		return NULL;
	}

#endif //BUILD_PLATFORM_LINUX

	tParam * FindByValuePtr(const T_Type* pVal) {
		// this will be kinda slow if we have many groups, but normally we do not
		for (typename tList::iterator it = m_list.begin(); it != m_list.end(); ++it)
			if (it->pValue == pVal)
				return it;
		return NULL;
	}

	void Add(tParam& param) {
		m_list.push_front(param);
	}

	tList m_list;
};


///////////////////////////////////////////////////////////////////////////////
// this structure contains all important "global" PM parameters
// this is also the place where all the layout information is stored
struct ParameterMapContext : public ParameterMapContextBase{
	////////////////////////////////////////////////////
	// data members related to the card creation process
	int m_nLongProtocolID;
	int m_nBoolProtocolID;
	int m_nSelectionProtocolID;
	int m_nDoubleProtocolID;
	int m_nWipCounter;
	
	char m_szCustomTag[MAX_CUSTOM_TAG];
	
	// "current" objects to handle BEGIN_ macros
	TParamTemplate<selection>* m_pCurrentSelect;
	// same for groups
	TParamGroupTemplate<long>* m_pCurrentLongGroup;
	TParamGroupTemplate<double>* m_pCurrentDoubleGroup;
	TParamGroupTemplate<bool>* m_pCurrentBoolGroup;
	TParamGroupTemplate<selection>* m_pCurrentSelectionGroup;

	bool m_bIsNew;

	// a constructor: we need it
	ParameterMapContext() {
		m_bIsNew = true;
		m_nLongProtocolID =0;
		m_nDoubleProtocolID = 0;
		m_nBoolProtocolID= 31;
		m_nSelectionProtocolID= 31;
		m_bCompactBools = false;
		m_nCompactSelFact = 0;
	}

	// functions
	void setCustomTag(LPCSTR pszTag) { 
		if (m_pCurrentLongGroup || m_pCurrentDoubleGroup || m_pCurrentBoolGroup || m_pCurrentSelectionGroup)
		{
			std::cout << "PM ERROR: it is not allowed to use standard tags inside groups\n";
			return;
		}
		strncpy(m_szCustomTag, pszTag, MAX_CUSTOM_TAG); 
		m_szCustomTag[MAX_CUSTOM_TAG-1]='\0'; 
	}
	bool isCustomTagSet() { return m_szCustomTag[0]!='\0'; }
	LPCSTR getCustomTag() { return isCustomTagSet() ? m_szCustomTag : NULL; }
	void resetCustomTag() { m_szCustomTag[0]='\0'; }

	int getIncWipCounter() { return (isCustomTagSet() || m_pCurrentLongGroup || m_pCurrentDoubleGroup || m_pCurrentBoolGroup || m_pCurrentSelectionGroup) ? -1 : m_nWipCounter++; }

#ifndef BUILD_PLATFORM_LINUX
	// parameter functioning support
	virtual void registerAll(SeqLim* pSeqLim) = 0;
	virtual void updateControls() = 0;
	virtual bool checkAndSoftInitParameters(MrProt* pMrProt, const SeqLim* pSeqLim) = 0;
	virtual bool protocolBasedEnable(MrProt* pMrProt) = 0;
	virtual void recoveryForProtocolBasedEnable(MrProt* pMrProt) = 0;
#endif //!BUILD_PLATFORM_LINUX
	virtual bool checkAndInitParameters(MrProt* pMrProt, const SeqLim* pSeqLim) = 0;
	virtual void updateLocalParameters(MrProt* pMrProt) = 0;
	virtual void updateProtocol(MrProt* pMrProt) = 0;
	virtual bool protocolDiffers(MrProt* pMrProt) = 0;
	virtual void dump_parameters() = 0;

	// storage support
	typedef TSimpleList<SParamCommon*> tCommonParamPtrList;
	typedef TSimpleList<SParamGroup*>  tGroupPtrList;	
	
	tCommonParamPtrList m_listAllParams;
	tGroupPtrList       m_listAllGroups;

	TSingleParamStorage<bool>      m_storageBool;
	TSingleParamStorage<selection> m_storageSelection;
	TSingleParamStorage<long>      m_storageLong;
	TSingleParamStorage<double>    m_storageDouble;
	
	TSingleParamStorage<bool>      m_storageEmptyBool; // trick to simulate non-existent arrays
	TSingleParamStorage<selection> m_storageEmptySelection; // trick to simulate non-existent arrays

	TArrayParamStorage<long>   m_storageLongArray;
	TArrayParamStorage<double> m_storageDoubleArray;

	TGroupParamStorage<bool>      m_storageBoolGroup;
	TGroupParamStorage<selection> m_storageSelectionGroup;
	TGroupParamStorage<long>      m_storageLongGroup;
	TGroupParamStorage<double>    m_storageDoubleGroup;

	void BuildIndexes() {
		m_storageBool.BuildIndex();
		m_storageSelection.BuildIndex();
		m_storageLong.BuildIndex();
		m_storageDouble.BuildIndex();
	}

#ifndef PM_USE_SEQIF
	// singelton support
	static inline ParameterMapContext& instance(); //{ return ms_self; }
	//static ParameterMapContext ms_self;
#endif
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// helper templates to access PMC

template<class T> 
struct PMCT_Base {
	static TParamCommon<T> * SmartFindByValuePtr(ParameterMapContext& pmc, const T* pVal);
	static void AddParam(ParameterMapContext& pmc, TParamTemplate<T>& param);
	static void AddArray(ParameterMapContext& pmc, TParamArrayTemplate<T>& param);
};

template<class T> 
struct PMCT : public PMCT_Base<T> {
};

TEMPLATE_SPEC
struct PMCT<long> : public PMCT_Base<long> {
	static inline int  GetProtID(ParameterMapContext& pmc)    { return pmc.m_nLongProtocolID; };
	static inline int  GetIncProtID(ParameterMapContext& pmc) { return pmc.m_nLongProtocolID++; }
	static inline void IncProtID(ParameterMapContext& pmc)    { ++pmc.m_nLongProtocolID; };
	static inline void IncProtID(ParameterMapContext& pmc, int nInc) { pmc.m_nLongProtocolID += nInc; }
	static inline TSingleParamStorage<long>& GetParStorage(ParameterMapContext& pmc) { return pmc.m_storageLong; }
	static inline TArrayParamStorage<long>& GetArrStorage(ParameterMapContext& pmc) { return pmc.m_storageLongArray; }
	static inline TGroupParamStorage<long>& GetGrpStorage(ParameterMapContext& pmc) { return pmc.m_storageLongGroup; }	
	static inline TParamGroupTemplate<long>* GetCurrGrpPtr(ParameterMapContext& pmc) { return pmc.m_pCurrentLongGroup; }
};

TEMPLATE_SPEC
struct PMCT<double> : public PMCT_Base<double> {
	static inline int  GetProtID(ParameterMapContext& pmc)    { return pmc.m_nDoubleProtocolID; };
	static inline int  GetIncProtID(ParameterMapContext& pmc) { return pmc.m_nDoubleProtocolID++; };
	static inline void IncProtID(ParameterMapContext& pmc)    { ++pmc.m_nDoubleProtocolID; };
	static inline void IncProtID(ParameterMapContext& pmc, int nInc) { pmc.m_nDoubleProtocolID += nInc; };
	static inline TSingleParamStorage<double>& GetParStorage(ParameterMapContext& pmc) { return pmc.m_storageDouble; }
	static inline TArrayParamStorage<double>& GetArrStorage(ParameterMapContext& pmc) { return pmc.m_storageDoubleArray; }
	static inline TGroupParamStorage<double>& GetGrpStorage(ParameterMapContext& pmc) { return pmc.m_storageDoubleGroup; }
	static inline TParamGroupTemplate<double>* GetCurrGrpPtr(ParameterMapContext& pmc) { return pmc.m_pCurrentDoubleGroup; }
};

// PMCT specialisation for bool type to enable compacting
TEMPLATE_SPEC
struct PMCT<bool> : public PMCT_Base<bool> {
	static int GetProtID(ParameterMapContext& pmc) {
		if (!pmc.m_bCompactBools)
			return PMCT<long>::GetProtID(pmc); 
		if ((pmc.m_nBoolProtocolID&31)==31)
			pmc.m_nBoolProtocolID = PMCT<long>::GetIncProtID(pmc)<<5;
		return pmc.m_nBoolProtocolID;
	};
	static void IncProtID(ParameterMapContext& pmc) { 
		if (!pmc.m_bCompactBools) {
			PMCT<long>::IncProtID(pmc);
			return;
		}
		++pmc.m_nBoolProtocolID; // if it points to bit 31 the next "get" will fix this
	};
	static inline int GetIncProtID(ParameterMapContext& pmc) {
		register int nCurr=GetProtID(pmc);
		IncProtID(pmc);
		return nCurr;
	}
	static inline TSingleParamStorage<bool>& GetParStorage(ParameterMapContext& pmc) { return pmc.m_storageBool; }
	static inline TSingleParamStorage<bool>& GetArrStorage(ParameterMapContext& pmc) { return pmc.m_storageEmptyBool; } // a trick to make some template functions happy
	static inline TGroupParamStorage<bool>& GetGrpStorage(ParameterMapContext& pmc) { return pmc.m_storageBoolGroup; }	
	static inline TParamGroupTemplate<bool>* GetCurrGrpPtr(ParameterMapContext& pmc) { return pmc.m_pCurrentBoolGroup; }
};


// PMCT specialisation for selection type to enable compacting
TEMPLATE_SPEC
struct PMCT<selection> : public PMCT_Base<selection> {
	static int GetProtID(ParameterMapContext& pmc) {
		if (pmc.m_nCompactSelFact<=1)
			return PMCT<long>::GetProtID(pmc); 
		if ((pmc.m_nSelectionProtocolID&31)>=pmc.m_nCompactSelFact)
			pmc.m_nSelectionProtocolID = PMCT<long>::GetIncProtID(pmc)<<5;
		return pmc.m_nSelectionProtocolID;
	};
	static void IncProtID(ParameterMapContext& pmc) { 
		if (pmc.m_nCompactSelFact<=1) {
			PMCT<long>::IncProtID(pmc);
			return;
		}
		++pmc.m_nSelectionProtocolID; // if it gets too big the next "get" will fix it
	};
	static inline int GetIncProtID(ParameterMapContext& pmc) {
		register int nCurr=GetProtID(pmc);
		IncProtID(pmc);
		return nCurr;
	}
	static inline TSingleParamStorage<selection>& GetParStorage(ParameterMapContext& pmc) { return pmc.m_storageSelection; }
	static inline TSingleParamStorage<selection>& GetArrStorage(ParameterMapContext& pmc) { return pmc.m_storageEmptySelection; } // a trick to make some template functions happy
	static inline TGroupParamStorage<selection>& GetGrpStorage(ParameterMapContext& pmc) { return pmc.m_storageSelectionGroup; }
	static inline TParamGroupTemplate<selection>* GetCurrGrpPtr(ParameterMapContext& pmc) { return pmc.m_pCurrentSelectionGroup; }
};

//
template<class T>
TParamCommon<T> * PMCT_Base<T>::SmartFindByValuePtr(ParameterMapContext& pmc, const T* pVal) {
	TParamCommon<T>* pP = PMCT<T>::GetParStorage(pmc).FindByValuePtr(pVal);
	if (pP) 
		return pP;
	return PMCT<T>::GetArrStorage(pmc).FindByValuePtr(pVal);
}

template<class T>
void PMCT_Base<T>::AddParam(ParameterMapContext& pmc, TParamTemplate<T>& param) {
	PMCT<T>::GetParStorage(pmc).m_list.push_front(param);
	pmc.m_listAllParams.push_front(PMCT<T>::GetParStorage(pmc).m_list.frontPtr());
	if (PMCT<T>::GetCurrGrpPtr(pmc))
	{
		if (PMCT<T>::GetCurrGrpPtr(pmc)->nSize == 0 && PMCT<T>::GetCurrGrpPtr(pmc)->nWIP == -1) 
			PMCT<T>::GetCurrGrpPtr(pmc)->setWIP(pmc.m_nWipCounter++);
		PMCT<T>::GetCurrGrpPtr(pmc)->Add(&param);
	}
}

template<class T>
void PMCT_Base<T>::AddArray(ParameterMapContext& pmc, TParamArrayTemplate<T>& param) {
	PMCT<T>::GetArrStorage(pmc).m_list.push_front(param);
	pmc.m_listAllParams.push_front(PMCT<T>::GetArrStorage(pmc).m_list.frontPtr());
}

// template functions implementation
////////////////////////////////////


//////////////////////////////////////////////////////////
// UI-support related stuff -- Siemens callbacks and so on

#ifndef BUILD_PLATFORM_LINUX

extern const char* s_szaWIPs[];

extern bool WIP_LIMIT_CONDITION(int nwip);

class PM_Recovery {
public:
	PM_Recovery(ParameterMapContext* pPMC, MrProt* pMrProt) : m_pPMC(pPMC), m_pMrProt(pMrProt) {};
	~PM_Recovery() {
		m_pPMC->recoveryForProtocolBasedEnable(m_pMrProt);
	}
protected:
	ParameterMapContext* m_pPMC;
	MrProt* m_pMrProt;
private:
	PM_Recovery();
};

//////////////////////////
// data exchange and alike

struct ParameterMapContextImpl : public ParameterMapContext {

	void registerAll(SeqLim* pSeqLim) {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			if ((*it)->nWIP>=0)
				(*it)->Register(pSeqLim, s_szaWIPs[(*it)->nWIP]);
			else if ((*it)->szTag[0])
				(*it)->Register(pSeqLim, (*it)->szTag);
		// register groups
		for (tGroupPtrList::iterator itg = m_listAllGroups.begin(); itg != m_listAllGroups.end(); ++itg)
			if ((*itg)->nWIP>=0)
				(*itg)->Register(pSeqLim, s_szaWIPs[(*itg)->nWIP]);
			else if ((*itg)->szTag[0])
				(*itg)->Register(pSeqLim, (*itg)->szTag);
	}

	void updateControls() {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			if ((*it)->nWIP>=0 || (*it)->szTag[0])
				(*it)->UpdateUI();
		for (tGroupPtrList::iterator itg = m_listAllGroups.begin(); itg != m_listAllGroups.end(); ++itg)
			if ((*itg)->nWIP>=0 || (*itg)->szTag[0])
				(*itg)->UpdateUI();
	}

	bool checkAndSoftInitParameters(MrProt* pMrProt, const SeqLim* pSeqLim) {
		bool bNeedInit = false;
		tCommonParamPtrList::iterator it;
		for (it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
		{
			if (!(*it)->CheckProt(pMrProt))
				(*it)->bNeedInit = bNeedInit = true;
			else
				(*it)->bNeedInit = false;
		}

		if (!bNeedInit)
			return true;

		if (pSeqLim && !pSeqLim->isContextPrepForMrProtUpdate())
			return false;

		for (it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			if ((*it)->bNeedInit)
				(*it)->InitProt(pMrProt);
		return true;
	}

	bool protocolBasedEnable(MrProt* pMrProt) {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			if ((*it)->nWIP<0 && (*it)->pbEnable && !*(*it)->pbEnable)
				if ((*it)->ProtDiffers(pMrProt))
				{
					//cout << "protocolBasedEnable() : " << (*it)->szName << " false\n";
					return false;
				}
		//cout << "protocolBasedEnable() : true\n";
		return true;
	}

	void recoveryForProtocolBasedEnable(MrProt* pMrProt) {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
		if ((*it)->nWIP<0)
		{
			//sanity check to make sure that protocolBasedEnable does not destroy everything
			if ((*it)->ProtDiffers(pMrProt) && (*it)->pbEnable && !*(*it)->pbEnable)
			{
				std::cout << "PM_WARNING: read-only grouped parameter (" << (*it)->szName << ") modified, restoring its value from the protocol\n";
				(*it)->UpdateItself(pMrProt);
			}
		}
	}

#else //!BUILD_PLATFORM_LINUX

#define WIP_LIMIT_CONDITION(_nwip) 0

struct ParameterMapContextImpl : public ParameterMapContext {

#endif //!BUILD_PLATFORM_LINUX

	bool checkAndInitParameters(MrProt* pMrProt, const SeqLim* pSeqLim) {
		bool bNeedInit = false;
		tCommonParamPtrList::iterator it;
		for (it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			if (!(*it)->CheckProt(pMrProt)) {
				bNeedInit = true;
				break;
			}

		if (!bNeedInit)
			return true;

		if (!pSeqLim->isContextPrepForMrProtUpdate())
			return false;

		for (it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			(*it)->InitProt(pMrProt);

		return true;
	}

	void updateLocalParameters(MrProt* pMrProt) {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			(*it)->UpdateItself(pMrProt);
	}

	void updateProtocol(MrProt* pMrProt) {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			(*it)->UpdateProt(pMrProt);
	}

	bool protocolDiffers(MrProt* pMrProt) {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			if ((*it)->ProtDiffers(pMrProt))
				return true;
		return false;
	}

	void dump_parameters() {
		for (tCommonParamPtrList::iterator it = m_listAllParams.begin(); it != m_listAllParams.end(); ++it)
			(*it)->dump();
		for (tGroupPtrList::iterator itg = m_listAllGroups.begin(); itg != m_listAllGroups.end(); ++itg)
			(*itg)->dump();
	}

#ifndef PM_USE_SEQIF
	// singelton support
	static ParameterMapContextImpl ms_self;
#endif

};

//////////////////////////////////////
// "Interface" PM macros and functions

#ifdef PM_USE_SEQIF

template<class T_Parent>
class PM_SeqIF : public T_Parent {

private:
	ParameterMapContextImpl pm_internal_pmc;

public:
	inline ParameterMapContext& get_pmc_instance() { return pm_internal_pmc; }

#else //PM_USE_SEQIF

	inline ParameterMapContext& get_pmc_instance() { return ParameterMapContext::instance(); }

#endif //PM_USE_SEQIF

void COMPACT_BOOLS() { get_pmc_instance().m_bCompactBools=true; }
void COMPACT_SELECTIONS(int nFactor=4) { 
	if (nFactor>16)
		nFactor=16;
	get_pmc_instance().m_nCompactSelFact=nFactor; 
}

void SKIP_PARAM() { get_pmc_instance().m_nWipCounter++; }
void SKIP_PARAMS(int n) { get_pmc_instance().m_nWipCounter += n; }

void PARAM(LPCSTR szName, bool* pbValue, bool bDef, LPCSTR szToolTip = NULL) {
	ParameterMapContext& PMC = get_pmc_instance();
	TParamTemplate<bool> param(PMC.getIncWipCounter(), 
		PMCT<bool>::GetIncProtID(PMC), szName, *pbValue, bDef, szToolTip, PMC.getCustomTag(), &PMC);
	PMC.resetCustomTag();
	if (WIP_LIMIT_CONDITION(param.nWIP))
	{
		std::cout << "PM ERROR: parameter '" << szName << "' does not fit into the card. Consider using groups or standard tags\n";
		return;
	}
	PMCT<bool>::AddParam(PMC, param);
}

void PARAM(LPCSTR szName, LPCSTR szUnit, long* pValue, long Min, long Max, long Inc, long Def, LPCSTR szToolTip = NULL) {
	ParameterMapContext& PMC = get_pmc_instance();
	TParamTemplate<long> param(PMC.getIncWipCounter(), 
		PMCT<long>::GetIncProtID(PMC), szName, szUnit, *pValue, Min, Max, Inc, Def, szToolTip, PMC.getCustomTag(), &PMC);
	PMC.resetCustomTag();
	if (WIP_LIMIT_CONDITION(param.nWIP))
	{
		std::cout << "PM ERROR: parameter '" << szName << "' does not fit into the card. Consider using groups or standard tags\n";
		return;
	}
	PMCT<long>::AddParam(PMC, param);
}

void PARAM(LPCSTR szName, LPCSTR szUnit, double* pValue, double Min, double Max, double Inc, double Def, LPCSTR szToolTip = NULL) {
	ParameterMapContext& PMC = get_pmc_instance();
	TParamTemplate<double> param(PMC.getIncWipCounter(),
		PMCT<double>::GetIncProtID(PMC), szName, szUnit, *pValue, Min, Max, Inc, Def, szToolTip, PMC.getCustomTag(), &PMC);
	PMC.resetCustomTag();
	if (WIP_LIMIT_CONDITION(param.nWIP))
	{
		std::cout << "PM ERROR: parameter '" << szName << "' does not fit into the card. Consider using groups or standard tags\n";
		return;
	}
	PMCT<double>::AddParam(PMC, param);
}

void PARAM_SELECT(LPCSTR szName, selection* pnValue, selection nDef, LPCSTR szToolTip = NULL) {
	ParameterMapContext& PMC = get_pmc_instance();
	if (PMC.m_pCurrentSelect)
		std::cout << "PM ERROR: starting new selection without closing the previous one\n";
	
	PMC.m_pCurrentSelect = new TParamTemplate<selection>(PMC.getIncWipCounter(),
		PMCT<selection>::GetIncProtID(PMC), szName, *pnValue, nDef, szToolTip, PMC.getCustomTag(), &PMC);
	PMC.resetCustomTag();
}

void PARAM_SELECT_END() {
	ParameterMapContext& PMC = get_pmc_instance();
	if (!PMC.m_pCurrentSelect)
	{
		std::cout << "PM ERROR: closing a selection without openning it\n";
		return;
	}

	if (WIP_LIMIT_CONDITION(PMC.m_pCurrentSelect->nWIP))
		std::cout << "PM ERROR: parameter '" << PMC.m_pCurrentSelect->szName << "' does not fit into the card. Consider using groups or standard tags\n";
	else
		PMCT<selection>::AddParam(PMC, *PMC.m_pCurrentSelect);

	delete PMC.m_pCurrentSelect;
	PMC.m_pCurrentSelect = NULL;
}

void OPTION(LPCSTR szOption, selection nValue) {
	ParameterMapContext& PMC = get_pmc_instance();
	if (!PMC.m_pCurrentSelect)
	{
		std::cout << "PM ERROR: addind an option without openning a selection\n";
		return;
	}

	if (PMC.m_nCompactSelFact>1 && nValue >= (1u<<(32/PMC.m_nCompactSelFact))) 
	{
		std::cout << "PM ERROR: trying to create an option with a value too high for the used compacting factor\n";
		std::cout << "PM ERROR: max value: " << (1<<(32/PMC.m_nCompactSelFact))-1 << "; creating: " << nValue << std::endl;
		return;
	}

	PMC.m_pCurrentSelect->AddOption(szOption, nValue);
}

void PARAM_GROUP() {
	ParameterMapContext& PMC = get_pmc_instance();
	if (PMC.m_pCurrentLongGroup)
		std::cout << "PM ERROR: starting a new long group without closing the previous one\n";
	if (PMC.m_pCurrentDoubleGroup)
		std::cout << "PM ERROR: starting a new double group without closing the previous one\n";
	if (PMC.m_pCurrentDoubleGroup)
		std::cout << "PM ERROR: starting a new bool group without closing the previous one\n";
	
	PMC.m_pCurrentLongGroup = new TParamGroupTemplate<long>(PMC.isCustomTagSet() ? -2 : -1, -1/*PMCT<long>::GetProtID()*/, PMC.getCustomTag());
	PMC.m_pCurrentDoubleGroup = new TParamGroupTemplate<double>(PMC.isCustomTagSet() ? -2 : -1, -1/*PMCT<double>::GetProtID()*/, PMC.getCustomTag());
	// WARNING: GetProtID below is a posible source of bugs if the mixed groups are used...
	PMC.m_pCurrentBoolGroup = new TParamGroupTemplate<bool>(PMC.isCustomTagSet() ? -2 : -1, -1/*PMCT<bool>::GetProtID()*/, PMC.getCustomTag());
	PMC.m_pCurrentSelectionGroup = new TParamGroupTemplate<selection>(PMC.isCustomTagSet() ? -2 : -1, -1/*PMCT<selection>::GetProtID()*/, PMC.getCustomTag());
	PMC.resetCustomTag();
}

void PARAM_GROUP_END() {
	ParameterMapContext& PMC = get_pmc_instance();
	if (!PMC.m_pCurrentLongGroup)
	{
		std::cout << "PM ERROR: closing a long group without openning it\n";
	}
	else
	{
		if (PMC.m_pCurrentLongGroup->nSize)
			if (WIP_LIMIT_CONDITION(PMC.m_pCurrentLongGroup->nWIP))
				std::cout << "PM ERROR: one of the 'long' groups does not fit into the card. Consider using standard tags\n";
			else
			{
				PMC.m_storageLongGroup.Add(*PMC.m_pCurrentLongGroup);
				PMC.m_listAllGroups.push_front(PMC.m_storageLongGroup.m_list.frontPtr());
			}
		delete PMC.m_pCurrentLongGroup;
		PMC.m_pCurrentLongGroup = NULL;
	}

	if (!PMC.m_pCurrentDoubleGroup)
	{
		std::cout << "PM ERROR: closing a double group without openning it\n";
	}
	else
	{
		if (PMC.m_pCurrentDoubleGroup->nSize)
			if (WIP_LIMIT_CONDITION(PMC.m_pCurrentDoubleGroup->nWIP))
				std::cout << "PM ERROR: one of the 'double' groups does not fit into the card. Consider using standard tags\n";
			else
			{
				PMC.m_storageDoubleGroup.Add(*PMC.m_pCurrentDoubleGroup);
				PMC.m_listAllGroups.push_front(PMC.m_storageDoubleGroup.m_list.frontPtr());
			}
		delete PMC.m_pCurrentDoubleGroup;
		PMC.m_pCurrentDoubleGroup = NULL;
	}

	if (!PMC.m_pCurrentBoolGroup)
	{
		std::cout << "PM ERROR: closing a bool group without openning it\n";
	}
	else
	{
		if (PMC.m_pCurrentBoolGroup->nSize)
			if (WIP_LIMIT_CONDITION(PMC.m_pCurrentBoolGroup->nWIP))
				std::cout << "PM ERROR: one of the 'bool' groups does not fit into the card. Consider using standard tags\n";
			else
			{
				PMC.m_storageBoolGroup.Add(*PMC.m_pCurrentBoolGroup);
				PMC.m_listAllGroups.push_front(PMC.m_storageBoolGroup.m_list.frontPtr());
			}
		delete PMC.m_pCurrentBoolGroup;
		PMC.m_pCurrentBoolGroup = NULL;
	}

	if (!PMC.m_pCurrentSelectionGroup)
	{
		std::cout << "PM ERROR: closing a selection group without openning it\n";
	}
	else
	{
		if (PMC.m_pCurrentSelectionGroup->nSize)
			if (WIP_LIMIT_CONDITION(PMC.m_pCurrentSelectionGroup->nWIP))
				std::cout << "PM ERROR: one of the 'selection' groups does not fit into the card. Consider using standard tags\n";
			else
			{
				PMC.m_storageSelectionGroup.Add(*PMC.m_pCurrentSelectionGroup);
				PMC.m_listAllGroups.push_front(PMC.m_storageSelectionGroup.m_list.frontPtr());
			}
		delete PMC.m_pCurrentSelectionGroup;
		PMC.m_pCurrentSelectionGroup = NULL;
	}
}

template<class T>
void PARAM(LPCSTR szName, LPCSTR szUnit, long nSize, T* pValue, const T* pMin, const T* pMax, const T* pInc, const T* pDef, LPCSTR szToolTip = NULL) {
	if (nSize <= 0 || !pValue || !pMin || !pMax || !pInc || !pDef)
		return;
	ParameterMapContext& PMC = get_pmc_instance();
	TParamArrayTemplate<T> param(PMC.getIncWipCounter(),
		PMCT<T>::GetProtID(PMC), szName, szUnit, nSize, pValue, pMin, pMax, pInc, pDef, szToolTip, PMC.getCustomTag(), &PMC);
	PMCT<T>::IncProtID(PMC, nSize);
	PMC.resetCustomTag();
	PMCT<T>::AddArray(PMC, param);
}

void USE_STANDARD_TAG(LPCSTR szTag) {
	ParameterMapContext& PMC = get_pmc_instance();
	PMC.setCustomTag(szTag);
}

template<class T>
void CHANGE_LIMITS(const T* pValue, T Min, T Max, T Inc) {
	TParamTemplate<T>* pP = PMCT<T>::GetParStorage(get_pmc_instance()).FindByValuePtr(pValue);
	if (pP) {
		pP->valMin = Min;
		pP->valMax = Max;
		pP->valInc = Inc;
	}
}

template<class T>
long PM_INDEX(const T* pValue) {
	TParamCommon<T> * pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pValue);
	if (pP) 
		return pP->nID;
	return -1;
}

#ifndef BUILD_PLATFORM_LINUX

template<class T>
void VERIFY_ALL(const T* pVal) {
	TParamTemplate<T>* pP = PMCT<T>::GetParStorage(get_pmc_instance()).FindByValuePtr(pVal);
	if (pP) 
	{
		pP->ulVerify = TN4I<T>::VERIFY_SCAN_ALL;
		return;
	}
	TParamArrayTemplate<T>* pPA = PMCT<T>::GetArrStorage(get_pmc_instance()).FindByValuePtr(pVal);
		pPA->ulVerify = TN4I<T>::VERIFY_SCAN_ALL;
}

template<class T>
void VERIFY_OFF(const T* pVal) {
	TParamTemplate<T>* pP = PMCT<T>::GetParStorage(get_pmc_instance()).FindByValuePtr(pVal);
	if (pP) 
	{
		pP->ulVerify = TN4I<T>::VERIFY_OFF;
		return;
	}
	TParamArrayTemplate<T>* pPA = PMCT<T>::GetArrStorage(get_pmc_instance()).FindByValuePtr(pVal);
		pPA->ulVerify = TN4I<T>::VERIFY_OFF;
}

template<class T>
void LINK_SHOW_HIDE(const T* pVal, const bool* pbShow) {
	std::cout << "PM_WARNING: LINK_SHOW_HIDE does not worpk properly in VBxx versions and is depricated. Use SHOW_PARAM() or HIDE_PARAM() instead\n";
	TParamCommon<T> * pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
	if (pP) 
		pP->pbShow = pbShow;
}

template<class T>
void LINK_ENABLE_DISABLE(const T* pVal, const bool* pbEnable) {
	std::cout << "PM_WARNING: LINK_ENABLE_DISABLE does not worpk properly in VBxx versions and is depricated. Use ENABLE_PARAM() or DISABLE_PARAM() instead\n";
	TParamCommon<T> * pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
		pP->pbEnable = pbEnable;
}

template<class T>
void TOOLTIP_CUSTOM(const T* pVal, typename TN4I<T>::tGetToolTipIdPtr pToolTip)
{
	TParamCommon<T> * pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
	if (pP) 
		pP->pToolTip = pToolTip;
}

template<class T>
void TRY_CUSTOM(const T* pVal, typename TN4I<T>::tTryPtr pTry)
{
	TParamCommon<T> * pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
	if (pP) 
		pP->pTry = pTry;
}

template<class T>
void SOLVE_CUSTOM(const T* pVal, typename TN4I<T>::tSolvePtr pSolve)
{
	TParamCommon<T> * pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
	if (pP) 
		pP->pSolve = pSolve;
}

template<class T>
void SOLVE_TE_TR_TI(const T* pVal)
{
	TParamCommon<T> * pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
	if (pP) 
		pP->pSolve = TParamCommon<T>::SolveTE_TR_TI;
}

// functions that may be called to update the state of controls from fSeqPrep()
template<class T>
void VISIBLE_ARRAY_SIZE(SeqLim* pSeqLim, const T* pValue, long nSize) {
	if (pSeqLim->isContextPrepForMrProtUpdate() || pSeqLim->isContextPrepForSeqLimUpdate() || pSeqLim->isContextPrepForScanTimeCalculation())
	{
		if (nSize < 1)
			return;

		TParamArrayTemplate<T>* pP = PMCT<T>::GetArrStorage(get_pmc_instance()).FindByValuePtr(pValue);
		if (pP) 
			pP->nVisibleSize = nSize;
	}
}

template<class T>
int PRINTF_TOOLTIP(SeqLim* pSeqLim, const T* pValue, const char * szFormat, ...)
{
	if (pSeqLim->isContextPrepForMrProtUpdate() || pSeqLim->isContextPrepForSeqLimUpdate() || pSeqLim->isContextPrepForScanTimeCalculation())
	{
		SParamCommon* pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pValue);
		if (!pP) 
			return -1;

		va_list ap;
		va_start(ap, szFormat);
		int nRes = _vsnprintf(pP->szToolTip, MAX_TOOLTIP, szFormat, ap);
		va_end(ap);
		return nRes;
	}
	return 0;
}

template<class T>
void ENABLE_PARAM(SeqLim* pSeqLim, const T* pVal, bool bEnable = true) {
	if (pSeqLim->isContextPrepForMrProtUpdate() || pSeqLim->isContextPrepForSeqLimUpdate() || pSeqLim->isContextPrepForScanTimeCalculation())
	{
		TParamCommon<T>* pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
		if (pP) 
			pP->pbEnable = bEnable ? &bALWAYS_TRUE : &bALWAYS_FALSE;
	}
}

template<class T>
void DISABLE_PARAM(SeqLim* pSeqLim, const T* pVal) {
	ENABLE_PARAM(pSeqLim, pVal, false);
}

template<class T>
void SHOW_PARAM(SeqLim* pSeqLim, const T* pVal, bool bShow = true) {
	if (pSeqLim->isContextPrepForMrProtUpdate() || pSeqLim->isContextPrepForSeqLimUpdate() || pSeqLim->isContextPrepForScanTimeCalculation())
	{
		TParamCommon<T>* pP = PMCT<T>::SmartFindByValuePtr(get_pmc_instance(), pVal);
		if (pP) 
			pP->pbShow = bShow ? &bALWAYS_TRUE : &bALWAYS_FALSE;
	}
}

template<class T>
void HIDE_PARAM(SeqLim* pSeqLim, const T* pVal) {
	SHOW_PARAM(pSeqLim, pVal, false);
}

// small helper macros
#define _BRUTE_FORCE_FIND(_val_ptr) \
	pP=PMCT<bool>::SmartFindByValuePtr(get_pmc_instance(), (bool*)(_val_ptr)); \
	if (pP==NULL) { pP=PMCT<selection>::SmartFindByValuePtr(get_pmc_instance(), (selection*)(_val_ptr)); \
		if (pP==NULL) { pP=PMCT<long>::SmartFindByValuePtr(get_pmc_instance(), (long*)(_val_ptr)); \
			if (pP==NULL) { pP=PMCT<double>::SmartFindByValuePtr(get_pmc_instance(), (double*)(_val_ptr)); \
	} } }
#define _SHOW_INTERNAL(_nn) \
	if (pVal_##_nn==NULL) return; \
	_BRUTE_FORCE_FIND(pVal_##_nn) \
	if (pP) pP->pbShow = bShow ? &bALWAYS_TRUE : &bALWAYS_FALSE;

#define _ENABLE_INTERNAL(_nn) \
	if (pVal_##_nn==NULL) return; \
	_BRUTE_FORCE_FIND(pVal_##_nn) \
	if (pP) pP->pbEnable = bEnable ? &bALWAYS_TRUE : &bALWAYS_FALSE;

// it was no fun writing these functions, but as it might be usefull...
void SHOW_MULTIPLE_PARAMS(SeqLim* pSeqLim, bool bShow, const void* pVal_01, 
															  const void* pVal_02 = NULL, 
															  const void* pVal_03 = NULL, 
															  const void* pVal_04 = NULL, 
															  const void* pVal_05 = NULL, 
															  const void* pVal_06 = NULL, 
															  const void* pVal_07 = NULL, 
															  const void* pVal_08 = NULL, 
															  const void* pVal_09 = NULL, 
															  const void* pVal_10 = NULL, 
															  const void* pVal_11 = NULL, 
															  const void* pVal_12 = NULL, 
															  const void* pVal_13 = NULL, 
															  const void* pVal_14 = NULL, 
															  const void* pVal_15 = NULL, 
															  const void* pVal_16 = NULL, 
															  const void* pVal_17 = NULL, 
															  const void* pVal_18 = NULL, 
															  const void* pVal_19 = NULL, 
															  const void* pVal_20 = NULL) {
	if (pSeqLim->isContextPrepForMrProtUpdate() || pSeqLim->isContextPrepForSeqLimUpdate() || pSeqLim->isContextPrepForScanTimeCalculation())
	{
		SParamCommon* pP;
		_SHOW_INTERNAL(01)
		_SHOW_INTERNAL(02)
		_SHOW_INTERNAL(03)
		_SHOW_INTERNAL(04)
		_SHOW_INTERNAL(05)
		_SHOW_INTERNAL(06)
		_SHOW_INTERNAL(07)
		_SHOW_INTERNAL(08)
		_SHOW_INTERNAL(09)
		_SHOW_INTERNAL(10)
		_SHOW_INTERNAL(11)
		_SHOW_INTERNAL(12)
		_SHOW_INTERNAL(13)
		_SHOW_INTERNAL(14)
		_SHOW_INTERNAL(15)
		_SHOW_INTERNAL(16)
		_SHOW_INTERNAL(17)
		_SHOW_INTERNAL(18)
		_SHOW_INTERNAL(19)
		_SHOW_INTERNAL(20)
	}
}

void ENABLE_MULTIPLE_PARAMS(SeqLim* pSeqLim, bool bEnable, const void* pVal_01, 
														   const void* pVal_02 = NULL, 
														   const void* pVal_03 = NULL, 
														   const void* pVal_04 = NULL, 
														   const void* pVal_05 = NULL, 
														   const void* pVal_06 = NULL, 
														   const void* pVal_07 = NULL, 
														   const void* pVal_08 = NULL, 
														   const void* pVal_09 = NULL, 
														   const void* pVal_10 = NULL, 
														   const void* pVal_11 = NULL, 
														   const void* pVal_12 = NULL, 
														   const void* pVal_13 = NULL, 
														   const void* pVal_14 = NULL, 
														   const void* pVal_15 = NULL, 
														   const void* pVal_16 = NULL, 
														   const void* pVal_17 = NULL, 
														   const void* pVal_18 = NULL, 
														   const void* pVal_19 = NULL, 
														   const void* pVal_20 = NULL) {
	if (pSeqLim->isContextPrepForMrProtUpdate() || pSeqLim->isContextPrepForSeqLimUpdate() || pSeqLim->isContextPrepForScanTimeCalculation())
	{
		SParamCommon* pP;
		_ENABLE_INTERNAL(01)
		_ENABLE_INTERNAL(02)
		_ENABLE_INTERNAL(03)
		_ENABLE_INTERNAL(04)
		_ENABLE_INTERNAL(05)
		_ENABLE_INTERNAL(06)
		_ENABLE_INTERNAL(07)
		_ENABLE_INTERNAL(08)
		_ENABLE_INTERNAL(09)
		_ENABLE_INTERNAL(10)
		_ENABLE_INTERNAL(11)
		_ENABLE_INTERNAL(12)
		_ENABLE_INTERNAL(13)
		_ENABLE_INTERNAL(14)
		_ENABLE_INTERNAL(15)
		_ENABLE_INTERNAL(16)
		_ENABLE_INTERNAL(17)
		_ENABLE_INTERNAL(18)
		_ENABLE_INTERNAL(19)
		_ENABLE_INTERNAL(20)
	}
}

#else //!BUILD_PLATFORM_LINUX

void VERIFY_ALL(void* pVal) {}
void VERIFY_OFF(void* pVal) {}

void LINK_SHOW_HIDE(void* szName, const void* pbShow) {}
void LINK_ENABLE_DISABLE(void* szName, const void* pbEnable) {}

void VISIBLE_ARRAY_SIZE(SeqLim* /*pSeqLim*/, const void* /*pValue*/, long nSize) {}
int PRINTF_TOOLTIP(SeqLim* /*pSeqLim*/, const void* /*pValue*/, const char * /*szFormat*/, ...) {
	return 0;
}

void ENABLE_PARAM(SeqLim* /*pSeqLim*/, const void* /*pVal*/, bool /*bEnable*/ = true) {}
void DISABLE_PARAM(SeqLim* /*pSeqLim*/, const void* /*pVal*/) {}
void SHOW_PARAM(SeqLim* /*pSeqLim*/, const void* /*pVal*/, bool /*bShow*/ = true) {}
void HIDE_PARAM(SeqLim* /*pSeqLim*/, const void* /*pVal*/) {}
void SHOW_MULTIPLE_PARAMS(SeqLim*, bool, const void*, const void* pVal_02 = NULL, const void* pVal_03 = NULL, const void* pVal_04 = NULL, const void* pVal_05 = NULL, const void* pVal_06 = NULL, const void* pVal_07 = NULL, const void* pVal_08 = NULL, const void* pVal_09 = NULL, const void* pVal_10 = NULL, const void* pVal_11 = NULL, const void* pVal_12 = NULL, const void* pVal_13 = NULL, const void* pVal_14 = NULL, const void* pVal_15 = NULL, const void* pVal_16 = NULL, const void* pVal_17 = NULL, const void* pVal_18 = NULL, const void* pVal_19 = NULL, const void* pVal_20 = NULL) {}
void ENABLE_MULTIPLE_PARAMS(SeqLim*, bool, const void*, const void* pVal_02 = NULL, const void* pVal_03 = NULL, const void* pVal_04 = NULL, const void* pVal_05 = NULL, const void* pVal_06 = NULL, const void* pVal_07 = NULL, const void* pVal_08 = NULL, const void* pVal_09 = NULL, const void* pVal_10 = NULL, const void* pVal_11 = NULL, const void* pVal_12 = NULL, const void* pVal_13 = NULL, const void* pVal_14 = NULL, const void* pVal_15 = NULL, const void* pVal_16 = NULL, const void* pVal_17 = NULL, const void* pVal_18 = NULL, const void* pVal_19 = NULL, const void* pVal_20 = NULL) {}

#define USE_STANDARD_TAG(_szTag)

#define TOOLTIP_CUSTOM(_pVariable, _pToolTip)
#define TRY_CUSTOM(_pVariable, _pTry)
#define SOLVE_CUSTOM(_pVariable, _pSolve)
#define SOLVE_TE_TR_TI(_pVariable)

#endif //!BUILD_PLATFORM_LINUX

#ifdef PM_USE_SEQIF
}; // end of the template PM class definition
#endif //PM_USE_SEQIF

// PM interface functions end
/////////////////////////////////////////////////////////////////////////////////////////////

#define BEGIN_PARAMETER_MAP(_pSeqLim, _nIDLong, _nIDDouble) \
{\
	SeqLim* pSLim = _pSeqLim;\
	ParameterMapContext& PMC=get_pmc_instance();\
	PMC.m_nWipCounter = 0;\
	PMC.m_pCurrentSelect = NULL;\
	PMC.m_pCurrentLongGroup = NULL;\
	PMC.m_pCurrentDoubleGroup = NULL;\
	PMC.m_pCurrentBoolGroup = NULL;\
	PMC.m_pCurrentSelectionGroup = NULL;\
	PMC.m_szCustomTag[0] = '\0';\
	if (PMC.m_bIsNew) {\
		PMC.m_bIsNew = false;\
		PMC.m_nLongProtocolID= _nIDLong;\
		PMC.m_nDoubleProtocolID= _nIDDouble;

#ifndef BUILD_PLATFORM_LINUX

#define END_PARAMETER_MAP \
	}\
	if (PMCT<long>::GetProtID(PMC) > K_WIP_MEM_BLOCK_FREE_LONG_MAX_SIZE) \
		{ std::cout << "PM ERROR: trying to create too many 'long' parameters\n"; } \
	if (PMCT<double>::GetProtID(PMC) > K_WIP_MEM_BLOCK_FREE_DBL_MAX_SIZE) \
		{ std::cout << "PM ERROR: trying to create too many 'double' parameters\n"; }\
	PMC.BuildIndexes();\
	PMC.registerAll(pSLim);\
}

#define PREPARE_PARAMETER_MAP(_pMrProt, _pSeqLim) \
	ParameterMapContext& PMC=get_pmc_instance();\
	if (!PMC.checkAndInitParameters(_pMrProt, _pSeqLim))\
		return SEQU_ERROR;\
	if (!PMC.protocolBasedEnable(_pMrProt)) \
			return SEQU_ERROR;\
	PMC.updateLocalParameters(_pMrProt);\
	if ((_pSeqLim)->isContextPrepForSeqLimUpdate() || (_pSeqLim)->isContextPrepForScanTimeCalculation()) \
		PMC.updateControls();\
	PM_Recovery pm_recovery_instance(&PMC, _pMrProt);

#define PREPARE_PARAMETER_MAP_SOFTLY(_pMrProt, _pSeqLim) \
	ParameterMapContext& PMC=get_pmc_instance();\
	if (!PMC.checkAndSoftInitParameters(_pMrProt, _pSeqLim))\
		return SEQU_ERROR;\
	if (!PMC.protocolBasedEnable(_pMrProt)) \
			return SEQU_ERROR;\
	PMC.updateLocalParameters(_pMrProt);\
	if ((_pSeqLim)->isContextPrepForSeqLimUpdate() || (_pSeqLim)->isContextPrepForScanTimeCalculation()) \
		PMC.updateControls();\
	PM_Recovery pm_recovery_instance(&PMC, _pMrProt);

#define QUICK_PREPARE_PARAMETER_MAP(_pMrProt, _pSeqLim) \
	get_pmc_instance().updateLocalParameters(_pMrProt);

#define UPDATE_PROTOCOL(_pMrProt, _pSeqLim) \
	if ((_pSeqLim)!=NULL && ((_pSeqLim)->isContextPrepForMrProtUpdate() || (_pSeqLim)->isContextPrepForSeqLimUpdate() || (_pSeqLim)->isContextPrepForScanTimeCalculation())) \
		get_pmc_instance().updateControls();\
	if ((_pSeqLim)==NULL || (_pSeqLim)->isContextPrepForMrProtUpdate() || (_pSeqLim)->isContextPrepForScanTimeCalculation()) {\
		get_pmc_instance().updateProtocol(_pMrProt);\
	}

#define CONVERT_PARAMETER_MAP(_pMrProt) \
	ParameterMapContext& PMC=get_pmc_instance();\
	if (!PMC.checkAndSoftInitParameters(_pMrProt, NULL))\
		return SEQU_ERROR;\
	PMC.updateLocalParameters(_pMrProt);\
	PM_Recovery pm_recovery_instance(&PMC, _pMrProt);\
	PMC.updateProtocol(_pMrProt);
	
#else //BUILD_PLATFORM_LINUX

#define END_PARAMETER_MAP \
	}\
	PMC.BuildIndexes();\
}

#define PREPARE_PARAMETER_MAP(_pMrProt, _pSeqLim) \
	ParameterMapContext& PMC=get_pmc_instance();\
	PMC.checkAndInitParameters(_pMrProt, _pSeqLim);\
	PMC.updateLocalParameters(_pMrProt);

#define PREPARE_PARAMETER_MAP_SOFTLY(_pMrProt, _pSeqLim) \
	ParameterMapContext& PMC=get_pmc_instance();\
	PMC.checkAndInitParameters(_pMrProt, _pSeqLim);\
	PMC.updateLocalParameters(_pMrProt);

#define QUICK_PREPARE_PARAMETER_MAP(_pMrProt, _pSeqLim) \
	get_pmc_instance().updateLocalParameters(_pMrProt);

#define UPDATE_PROTOCOL(_pMrProt, _pSeqLim) \
	if ((_pSeqLim)->isContextPrepForMrProtUpdate() || (_pSeqLim)->isContextPrepForScanTimeCalculation())\
		get_pmc_instance().updateProtocol(_pMrProt);

#endif //BUILD_PLATFORM_LINUX

#ifdef PM_USE_SEQIF

#ifndef BUILD_PLATFORM_LINUX

#define IMPLEMENT_PM_SEQIF(_SEQ_CLASS) \
ParameterMapContext& get_pmc_instance(MrUILinkBase* const pLink) { return (static_cast<_SEQ_CLASS*>(pLink->sequence().getSeq()))->get_pmc_instance();} \
ParameterMapContext& get_pmc_instance(MrUILinkArray* pArrayLink) { return (static_cast<_SEQ_CLASS*>(pArrayLink->sequence().getSeq()))->get_pmc_instance();}\
const char* s_szaWIPs[] = {\
	MR_TAG_SEQ_WIP1,\
	MR_TAG_SEQ_WIP2,\
	MR_TAG_SEQ_WIP3,\
	MR_TAG_SEQ_WIP4,\
	MR_TAG_SEQ_WIP5,\
	MR_TAG_SEQ_WIP6,\
	MR_TAG_SEQ_WIP7,\
	MR_TAG_SEQ_WIP8,\
	MR_TAG_SEQ_WIP9,\
	MR_TAG_SEQ_WIP10,\
	MR_TAG_SEQ_WIP11,\
	MR_TAG_SEQ_WIP12,\
	MR_TAG_SEQ_WIP13,\
	MR_TAG_SEQ_WIP14\
};\
bool WIP_LIMIT_CONDITION(int nwip) { return (nwip>=int(sizeof(s_szaWIPs)>>2)); }

ParameterMapContext& get_pmc_instance(MrUILinkBase* const pLink);
ParameterMapContext& get_pmc_instance(MrUILinkArray* pArrayLink);

template<class T_data> TParamCommon<T_data>* TParamCommon<T_data>::FindByIndexOrLink(long lIndex, tLinkPtr const pL){ return PMCT<T_data>::GetParStorage(get_pmc_instance(pL)).FindByIndexOrLink(lIndex, pL); }
template<class T_data> TParamCommon<T_data>* TParamCommon<T_data>::FindByArrayLink(MrUILinkArray* const pArray){ return PMCT<T_data>::GetParStorage(get_pmc_instance(pArray)).FindByArrayLink(pArray); }
template<class T_data> TParamArrayTemplate < T_data > * TParamArrayTemplate<T_data>::FindByArrayLink(MrUILinkArray* pArray){ return PMCT<T_data>::GetArrStorage(get_pmc_instance(pArray)).FindByArrayLink(pArray); }
template<class T_data> TParamArrayTemplate < T_data > * TParamArrayTemplate<T_data>::FindByElementLink(tLinkPtr const pLink){ return PMCT<T_data>::GetArrStorage(get_pmc_instance(pLink)).FindByElementLink(pLink); }
template<class T_data> TParamGroupTemplateBase < T_data >* TParamGroupTemplateBase<T_data>::FindByElementLink(tLinkPtr const pLink){ return PMCT<T_data>::GetGrpStorage(get_pmc_instance(pLink)).FindByElementLink(pLink); }
template<class T_data> TParamTemplate<T_data>* TParamGroupTemplateBase<T_data>::FindByIndex(int nIndex, tLinkPtr const pLink) { return PMCT<T_data>::GetParStorage(get_pmc_instance(pLink)).FindByIndex(nIndex); }
template<class T_data> TParamGroupTemplateBase < T_data >* TParamGroupTemplateBase<T_data>::FindByArrayLink(MrUILinkArray* const pArray){ return PMCT<T_data>::GetGrpStorage(get_pmc_instance(pArray)).FindByArrayLink(pArray); }

#else //!BUILD_PLATFORM_LINUX

#define IMPLEMENT_PM_SEQIF(_SEQ_CLASS)

#endif //!BUILD_PLATFORM_LINUX

#else //PM_USE_SEQIF

ParameterMapContextImpl ParameterMapContextImpl::ms_self;
inline ParameterMapContext& ParameterMapContext::instance() { return ParameterMapContextImpl::ms_self; }

#ifndef BUILD_PLATFORM_LINUX
template<class T_data> TParamCommon<T_data>* TParamCommon<T_data>::FindByIndexOrLink(long lIndex, tLinkPtr const pL){ return PMCT<T_data>::GetParStorage(ParameterMapContext::instance()).FindByIndexOrLink(lIndex, pL); }
template<class T_data> TParamCommon<T_data>* TParamCommon<T_data>::FindByArrayLink(MrUILinkArray* const pArray){ return PMCT<T_data>::GetParStorage(ParameterMapContext::instance()).FindByArrayLink(pArray); }
template<class T_data> TParamArrayTemplate < T_data > * TParamArrayTemplate<T_data>::FindByArrayLink(MrUILinkArray* pArray){ return PMCT<T_data>::GetArrStorage(ParameterMapContext::instance()).FindByArrayLink(pArray); }
template<class T_data> TParamArrayTemplate < T_data > * TParamArrayTemplate<T_data>::FindByElementLink(tLinkPtr const pLink){ return PMCT<T_data>::GetArrStorage(ParameterMapContext::instance()).FindByElementLink(pLink); }
template<class T_data> TParamGroupTemplateBase < T_data >* TParamGroupTemplateBase<T_data>::FindByElementLink(tLinkPtr const pLink){ return PMCT<T_data>::GetGrpStorage(ParameterMapContext::instance()).FindByElementLink(pLink); }
template<class T_data> TParamTemplate<T_data>* TParamGroupTemplateBase<T_data>::FindByIndex(int nIndex, tLinkPtr const) { return PMCT<T_data>::GetParStorage(ParameterMapContext::instance()).FindByIndex(nIndex); }
template<class T_data> TParamGroupTemplateBase < T_data >* TParamGroupTemplateBase<T_data>::FindByArrayLink(MrUILinkArray* const pArray){ return PMCT<T_data>::GetGrpStorage(ParameterMapContext::instance()).FindByArrayLink(pArray); }

const char* s_szaWIPs[] = {
	MR_TAG_SEQ_WIP1,
	MR_TAG_SEQ_WIP2,
	MR_TAG_SEQ_WIP3,
	MR_TAG_SEQ_WIP4,
	MR_TAG_SEQ_WIP5,
	MR_TAG_SEQ_WIP6,
	MR_TAG_SEQ_WIP7,
	MR_TAG_SEQ_WIP8,
	MR_TAG_SEQ_WIP9,
	MR_TAG_SEQ_WIP10,
	MR_TAG_SEQ_WIP11,
	MR_TAG_SEQ_WIP12,
	MR_TAG_SEQ_WIP13,
	MR_TAG_SEQ_WIP14/*,
	MR_TAG_SEQ_RESERVED1,
	MR_TAG_SEQ_RESERVED2,
	MR_TAG_SEQ_RESERVED3,
	MR_TAG_SEQ_RESERVED4,
	MR_TAG_SEQ_RESERVED5,
	MR_TAG_SEQ_RESERVED6,
	MR_TAG_SEQ_RESERVED7,
	MR_TAG_SEQ_RESERVED8*/
};

bool WIP_LIMIT_CONDITION(int nwip) { return (nwip>=14); }	

#endif //!BUILD_PLATFORM_LINUX

#endif //!PM_USE_SEQIF

#endif //__PARAMETER_MAP_H__INCLUDED__
