//This sequence is written by Martijn Cloos at NYU


// ------------------------------------------------------------------------------
// Application includes
// ------------------------------------------------------------------------------
#include "./IRFF.h"
#include "./IRFF_UI.h"

// ------------------------------------------------------------------------------
// General includes
// ------------------------------------------------------------------------------
#include "MrServers/MrImaging/libSeqUtil/libSeqUtil.h"
#include "MrServers/MrMeasSrv/MeasNuclei/IF/MeasKnownNuclei.h"
#include "MrServers/MrProtSrv/MrProt/MeasParameter/MrSysSpec.h"
#include "MrServers/MrProtSrv/MrProt/MeasParameter/MrRXSpec.h"
#include "MrServers/MrProtSrv/MrProt/MeasParameter/MrTXSpec.h"

#ifdef VB_VERSION
    #include "MrServers/MrMeasSrv/SeqIF/sde_allincludes.h"
    //#include <vector>
#endif

#include <iostream>
#include <fstream>

//#define HIP_R01

#ifndef SEQ_NAMESPACE
    #error SEQ_NAMESPACE not defined
#endif

#ifdef SEQUENCE_CLASS_IRFF
    SEQIF_DEFINE (SEQ_NAMESPACE::IRFF)
#endif


using namespace SEQ_NAMESPACE;

#include "seq_design.h"
#include "FOCI.h"

using std::cout;
using std::endl;
using std::string;

typedef Slice OSlice;

#define GAMMA 42.5781e6
#define PI 3.14159265359
#define TWOPI 6.28318530718
#define NYU_GOLDENANGLE 111.246117975

#define RF_TIME_BANDWIDTH_PRODUCT 3.0

//#define VERSED_ADC
#ifdef VERSED_ADC
  #define READOUT_OVERSAMPLING_FACTOR 8.0
#else
  #define READOUT_OVERSAMPLING_FACTOR 2.0
#endif

#define GRAD_SPOILER_SCALE 4.0 // n x 2pi
#define GLOBAL_FFT_SCALE_FACTOR 0.05
#define SEGMENT_SIZE 250
#define SEGMENT_SIZE_T1RHO 125
#define NUMBER_OF_NOISE_LINES 50

enum { NORMAL_SLICES = 0,
  RADIAL_SLICES = 1
};

enum {ICE_OFF = 0, ICE_ON =1};
enum{VAR_MODE = 0, CONST_MODE = 1}; //ASX
enum{T1RHO_PREP = 1, T2_PREP = 0}; //ASX
// --------------------------------------------------------------------------------------
// Debug macros
// --------------------------------------------------------------------------------------
#define DEBUG_BY_REGISTRY(A, B){if (lDebugMask & A){cout  << __FILE__ << ": " << B << endl;}}

#define TRUE_IF_DETAILED_DEBUG ( lDebugMask > 63 )
long lDebugMask = getMaskFromRegistry ("DEBUG_USER_SEQUENCE") ; // read the debug value in a global

// LINK_LONG_TYPE *pSGSize = _searchElm<LINK_LONG_TYPE>(pSeqLim, MR_TAG_SLICE_GROUP_LIST, MR_TAG_SG_SIZE);

IMPLEMENT_PM_SEQIF(IRFF)



IRFF::IRFF()
  : m_lRepetitionsToMeasure                         (0)
    , m_lLinesToMeasure                             (1)
    , m_lPhasesToMeasure                            (1)
    , m_lSlicesToMeasure                            (1)
    , m_lPartitionsToMeasure                        (1)
    , m_lDurationMainEventBlock                     (0)
    , m_lKernelRequestsPerMeasurement               (0)
    , m_lKernelCallsPerRelevantSignal               (0)
    , m_dRFSpoilIncrement                           (0)
    , m_dRFSpoilPhase                               (0)
    , m_lMySliSelRampTime                           (0)
    , m_dMinRiseTime                                (100000)
    , m_dGradMaxAmpl                                (0)
    , m_FirstSignal                                 (SEQ::SIGNAL_NONE)
    , m_FirstMethod                                 (SEQ::METHOD_NONE)
    , m_SecondSignal                                (SEQ::SIGNAL_NONE)
    , m_SecondMethod                                (SEQ::METHOD_NONE)
    , m_dRFSpoilPhasePrevSlice                      (0.0)
    , m_dRFSpoilIncrementPrevSlice                  (0.0)
    , m_dRFPrevSlicePosSag                          (999999.0)
    , m_dRFPrevSlicePosCor                          (999999.0)
    , m_dRFPrevSlicePosTra                          (999999.0)
    , m_dRFPrevSliceNormalSag                       (999999.0)
    , m_dRFPrevSliceNormalCor                       (999999.0)
    , m_dRFPrevSliceNormalTra                       (999999.0)
    , m_sSRFSinc                                    ("sSRFSinc")
    , m_sRF_FOCI                                    ("FOCI")
    , m_sSRF01zSet                                  ("sSRF01zSet")
    , m_sSRF01zNeg                                  ("sSRF01zNeg")
    , m_sGSliSel                                    ("sGSliSel")
    , m_sGSliSelReph                                ("sGSliSelRep")
    , m_sGReadDeph                                  ("sGReadDeph")
    , m_sGradReadout                                ("sGradReadout")
    , m_sGReadReph                                  ("sGReadReph")
    , m_sADC01                                      ("sADC01")
    , m_sADC01zSet                                  ("sADC01zSet")
    , m_sADC01zNeg                                  ("sADC01zNeg")
    , m_sGSpoil                                     ("GSpoil")
    , m_TokTokSBB                                   (&m_mySBBList)
    , m_pUI                                         (0)
{
    m_lShotsInScan          =    1;
    m_lKshift               =    0;
    m_lNumberOfFlipangles   =    1;
    m_lInversionAngle       =  360;

    m_lPulseDuration        = 2000;
    m_lShotIndex            =    1;

    m_dFlipAnglesSeriesA    =  NULL;
    m_dFlipAngleArray       =  NULL;
	m_lTsl = 0; //ASX
	//double daMin[10]={0.0,.5,2.0,4.0,6.0,8.0,10.0,20.0,35.0,50.0};
	//double daMin[10] ={5.0, 15.0, 25.0, 40.0, 55.0, 2.0, 4.0, 6.0, 8.0, 10.0 }; 
	double daMin[10] = {2000, 3725, 6950, 12950, 24150, 45000, 1000, 1000, 1000, 1000};
	for (long lTsl=0;lTsl<MAXTSL; lTsl++) //ASX T1rho default Tsls
	{
		m_cadMin[lTsl] = 0; ;
		m_cadMax[lTsl] = 100000;
		m_cadInc[lTsl] = 25 ;
		m_cadDef[lTsl] = daMin[lTsl];
	}		  
}


IRFF::~IRFF()
{
    if (m_pUI != NULL) {
        delete m_pUI;
        m_pUI = NULL;
    }

    if (m_dFlipAnglesSeriesA != NULL) {
        delete m_dFlipAnglesSeriesA;
        m_dFlipAnglesSeriesA = NULL;
    }

    if (m_dFlipAngleArray != NULL) {
        delete m_dFlipAngleArray;
        m_dFlipAngleArray = NULL;
    }

}



#ifdef VB_VERSION

    NLSStatus IRFF::initialize (SeqLim* pSeqLim)
    {
        return initialize(*pSeqLim);
    }

    NLSStatus IRFF::prepare (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo)
    {
        return prepare(*pMrProt, *pSeqLim, *pSeqExpo);
    }

    NLSStatus IRFF::check (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo, SEQCheckMode* pSEQCheckMode)
    {
        return check(*pMrProt, *pSeqLim, *pSeqExpo, pSEQCheckMode);
    }

    NLSStatus IRFF::run (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo)
    {
        return run(*pMrProt, *pSeqLim, *pSeqExpo);
    }

    NLS_STATUS IRFF::runKernel(MrProt* pMrProt,SeqLim* pSeqLim, SeqExpo* pSeqExpo, long lKernelMode, long lSlice, long lPartition, long lLine)
    {
        return runKernel(*pMrProt,*pSeqLim, *pSeqExpo, lKernelMode, lSlice, lPartition, lLine);
    }

#endif



//  --------------------------------------------------------------------------
//  Initialization of the sequence
//  --------------------------------------------------------------------------
NLSStatus IRFF::initialize (SeqLim &rSeqLim)
{

    static const char *ptModule = {"IRFF::initialize"};

    NLS_STATUS  lStatus = SEQU__NORMAL;

    // -----------------------------------------------------------
    // copy  data into samples object
    // -----------------------------------------------------------
    for( int iSample = 0; iSample<SizeOfFOCI; iSample++ ){
     m_sFOCIsamples[iSample].flAbs = (float) fociRF[iSample];
     m_sFOCIsamples[iSample].flPha = (float) fociPH[iSample];
    }

    //. -----------------------------------------------------------------------------
    //. Load flip angles and TR
    //. -----------------------------------------------------------------------------
    /*// ASX: Moved to prepare section
	m_lNumberOfFlipangles=SizeOfSeriesFA;
    m_dFlipAnglesSeriesA =  new double[m_lNumberOfFlipangles];

    cout<<"******************************"<<endl;
    cout<<"m_lNumberOfFlipangles = "<<m_lNumberOfFlipangles<<endl;
    cout<<"******************************"<<endl<<endl;


    for(long lL=0; lL<m_lNumberOfFlipangles; lL++){
      m_dFlipAnglesSeriesA[lL] = tmpFA[lL];
    }

    m_dFlipAngleArray= new double[m_lNumberOfFlipangles];
	*///ASX

    // ----------------------------------------------------------------------
    // Definition of sequence hard limits
    // ----------------------------------------------------------------------

    rSeqLim.disableSAFEConsistencyCheck();

    double      dMin, dMax, dInc, dDef;
    long        lMin, lMax, lInc, lDef;

    dMin = dMax = dInc = dDef = 0.;
    lMin = lMax = lInc = lDef = 0;

    // --------------------------------------------------------
    // Give general information about the sequence
    // --------------------------------------------------------
    rSeqLim.setMyOrigFilename ( __FILE__ );
    #ifdef VE_VERSION
      rSeqLim.setSequenceOwner    ( "USER" ); //SEQ_OWNER_SIEMENS );
    #else
      rSeqLim.setSequenceOwner    ( "USER" );
    #endif

    //-----------------------------------------------------------------------
    //  The sequence hint text (seen with "imprint name.dll" command )
    //-----------------------------------------------------------------------
    rSeqLim.setSequenceHintText( (char *)"\nT1rho_nFISP-IRFF sequence \nBuild: "__DATE__"   "__TIME__"\n" )  ;

    // -----------------------------------------------------------------------------
    // Define the system requirements like frequency and gradient power
    // -----------------------------------------------------------------------------
    rSeqLim.setAllowedFrequency     (8000000, 500000000); // Hz
    rSeqLim.setRequiredGradAmpl     (10.0); //  mT/m
    rSeqLim.setRequiredGradSlewRate (10.0); // (mT/m)/ms
    rSeqLim.setGradients(SEQ::GRAD_FAST, SEQ::GRAD_WHISPER, SEQ::GRAD_NORMAL,
                         SEQ::GRAD_FAST_GSWD_RISETIME, SEQ::GRAD_NORMAL_GSWD_RISETIME,
                         SEQ::GRAD_WHISPER_GSWD_RISETIME);

    #ifdef VE_VERSION
        rSeqLim.setSupportedNuclei(NUCLEI_ALL.get().c_str());
    #else
        rSeqLim.setSupportedNuclei(SEQ::NUCLEI_ALL);
    #endif


    // -----------------------------
    // Specify base matrix size of the image
    // -----------------------------
    rSeqLim.setBaseResolution ( 32, 2048, SEQ::INC_16, 224);//ASX


    // -----------------------------------------------------------------------------
    // Specify phase encoding parameters
    // -----------------------------------------------------------------------------
    rSeqLim.setPhasePartialFourierFactor (SEQ::PF_OFF);
    rSeqLim.setPhaseOversampling         (0.0,   1.0,   0.01,   0.0);
    rSeqLim.setPELines                   (32, 1024, 1, rSeqLim.getBaseResolution().getDef());
    rSeqLim.getPELines().setDisplayMode  (SEQ::DM_OFF);


    // --------------------------------------------------------------------------
    // Set the hard limits for Field of View
    // --------------------------------------------------------------------------
    dMax = SysProperties::getFoVMax() ;
    dDef = 140;//ASX Def 240.0;
    dInc =   1.0;
    dMin =    20;

    dMin = fSDSdRoundUpMinimum(dMin, dMax, dInc);
    if (dDef < dMin) dDef = dMin;
    if (dDef > dMax) dDef = dMax;

    rSeqLim.setReadoutFOV                ( dMin, dMax, dInc, dDef );
    rSeqLim.setPhaseFOV                  ( dMin, dMax, dInc, dDef );


    // --------------------------------------------------------------------------
    // Set TE / TR hard limits
    // --------------------------------------------------------------------------
    rSeqLim.setTR       (0,  100, 30000, 100,7500 ); //ASX Def 10000
    rSeqLim.setContrasts(1,    1,     1,          1);
    rSeqLim.setTE       (0,  100, 30000, 100,  3500);//ASX Def 4000


    // -------------------------------
    // Set TI limits
    // -------------------------------
    rSeqLim.setInversion (SEQ::INVERSION_OFF);
    rSeqLim.setSTIRMode  (SEQ::OFF);

    lMin    = 8000 ;
    lMin    = fSDSRoundToInc (lMin,1000);
    lDef    = (lMin <= 150000) ? 150000 : lMin;
    rSeqLim.setTI             (        0,    lMin,  2000000,     1000,      lDef) ;


    // --------------------------------------------------------------------------------------
    // Define limits for slices, excitation order etc.
    // --------------------------------------------------------------------------------------
    rSeqLim.setConcatenations       (           2, K_NO_SLI_MAX,           2,           2)  ;
    rSeqLim.setSlices               (           2, K_NO_SLI_MAX,           2,           2)  ;
    rSeqLim.setSliceThickness       (       1.000,      10.000,       0.500,       5.000)  ;
    rSeqLim.setSliceDistanceFactor  (      -1.000,       8.000,       0.010,       1.000)  ;

    rSeqLim.setMultiSliceMode(SEQ::MSM_SEQUENTIAL);
    rSeqLim.setSliceSeriesMode (SEQ::DESCENDING);

    rSeqLim.enableSliceShift ();
    rSeqLim.enableMSMA ();
    rSeqLim.enableOffcenter ();
    rSeqLim.setAllowedSliceOrientation (SEQ::DOUBLE_OBLIQUE);

    rSeqLim.setAveragingMode(SEQ::OUTER_LOOP);


    // --------------------------------------------------------------------------------------
    // Define 3D-properties
    // --------------------------------------------------------------------------------------
    rSeqLim.setDimension (SEQ::DIM_2) ;
    rSeqLim.setTrajectory(SEQ::TRAJECTORY_RADIAL);

    // These are 3D-parameters:
    rSeqLim.setPartition            (           8,         256,           2,          32);
    rSeqLim.setImagesPerSlab        (           8,         512,           2,          32);
    rSeqLim.setSlabThickness        (       5.000,     160.000                          );
    rSeqLim.set3DPartThickness      (       0.100,      25.000,        0.01,
                   rSeqLim.getSlabThickness().getMax()/ rSeqLim.getPartition().getDef());
    rSeqLim.setSliceOversampling    (       0.000,       1.000,       0.010,       0.000);
    rSeqLim.setMinSliceResolution   ( 0.5 );


    // --------------------------------------------------------------------------------------
    // Set Limits for Loop control counters
    // --------------------------------------------------------------------------------------
    rSeqLim.setRepetitions                  (        0,        0,        1,        0);
    rSeqLim.setAverages                     (        1,       32,        1,        1);

    // Set Limits for delays in Loops
    lInc = 100000;
    lMax = 2000000000;
    rSeqLim.setRepetitionsDelayTime         (        0,     lMax,     lInc,        0);
    // rSeqLim.setTD                 (        0,        0, 30000000,     1000,        0);

    // --------------------------------------------------------------------------------------
    // Configure user interface: Hide some switches
    // --------------------------------------------------------------------------------------
    rSeqLim.getEllipticalScanning().setDisplayMode (SEQ::DM_OFF);
    rSeqLim.getAsymmetricEcho    ().setDisplayMode (SEQ::DM_OFF);
    rSeqLim.getEPIFactor         ().setDisplayMode (SEQ::DM_OFF);
    rSeqLim.getInversion         ().setDisplayMode (SEQ::DM_OFF);
    rSeqLim.getTI                ().setDisplayMode (SEQ::DM_OFF);
    rSeqLim.getTD                ().setDisplayMode (SEQ::DM_OFF);


    // Patient information: Gradient TokTokTok at Measurement start
    rSeqLim.setIntro                       (SEQ::OFF, SEQ::ON);


    // --------------------------------------------------------------------------------------
    // Configure preparation pulses
    // --------------------------------------------------------------------------------------
    rSeqLim.setRSats                        (         0,       0,        1,        0);
    rSeqLim.setRSatThickness                (     3.000, 150.000,    1.000,   50.000);

    // Parallel Sats
    rSeqLim.setPSatMode       ( SEQ::PSAT_NONE);
    rSeqLim.setPSatThickness  (     3.000, 150.000,    1.000,   50.000);
    rSeqLim.setPSatGapToSlice (     5.000,  50.000,    1.000,   10.000);

    // Fat/Water Sats
    rSeqLim.setFatSuppression  (SEQ::FAT_SUPPRESSION_OFF);
    rSeqLim.setWaterSuppression(SEQ::WATER_SUPPRESSION_OFF);
    rSeqLim.setMTC             (SEQ::OFF);

    // Travelling Sats
    rSeqLim.setTSats                        (         0,       0,        0,        0);
    rSeqLim.setTSatThickness                (     3.000, 150.000,    1.000,   40.000);
    rSeqLim.setTSatGapToSlice               (     0.000,  50.000,    1.000,   10.000);

    // --------------------------------------------------------------------------------------
    // Define properties of excitation
    // --------------------------------------------------------------------------------------
    rSeqLim.setFlipAngle                        (   10.000,  720.000,    1.000,   60.000)  ;
    rSeqLim.setExtSrfFilename                   ( "%MEASDAT%/extrf.dat");
    rSeqLim.setRFSpoiling                       ( SEQ::ON );


    // --------------------------------------------------------------------------------
    // Set ADC properties
    // --------------------------------------------------------------------------------
    rSeqLim.setBandWidthPerPixel        (    0,        80,     8000,       10,    400);
    rSeqLim.setReadoutOSFactor          ( READOUT_OVERSAMPLING_FACTOR );


    // -----------------------------------------------------------------------------
    // Configure Reconstruction / Interpolation
    // -----------------------------------------------------------------------------
    rSeqLim.set2DInterpolation          (SEQ::NO);
    rSeqLim.setReconstructionMode       (SEQ::RECONMODE_MAGNITUDE);


    // ------------------------------------------------------------------------------------
    // Database control:
    //     If repetitions >= 1, is it allowed to put each one in a different series?
    // ------------------------------------------------------------------------------------
    rSeqLim.setMultipleSeriesMode (SEQ::MULTIPLE_SERIES_OFF, SEQ::MULTIPLE_SERIES_EACH_MEASUREMENT,
                                   SEQ::MULTIPLE_SERIES_EACH_SLICE , SEQ::MULTIPLE_SERIES_EACH_SLICE_AND_MEASUREMENT );


    // -----------------------------------------------------------------------------
    // Adjustment parameters
    // -----------------------------------------------------------------------------
    rSeqLim.setAdjShim         (SEQ::ADJSHIM_STANDARD, SEQ::ADJSHIM_ADVANCED, SEQ::ADJSHIM_TUNEUP);
    rSeqLim.setCoilCombineMode (SEQ::COILCOMBINE_SUM_OF_SQUARES);


    // -----------------------------------------------------------------------------
    // User Interface: SolveHandlers and Sequence/Special card parameters
    // -----------------------------------------------------------------------------
#ifdef WIN32  // only needed on the host
    // This configures some UI behaviour (e.g. TD) and has to be called before my own SolveHandlers

    #ifdef VE_VERSION
        fStdImagingInitPost (rSeqLim);
    #else
        fStdImagingInitPost (&rSeqLim);
    #endif

#endif


 #ifdef WIN32
    // -------------------------------------------------------------------------------------
    // File containing the default postprocessing protocol (EVAProtocol)
    // -------------------------------------------------------------------------------------
    rSeqLim.setDefaultEVAProt (_T("%SiemensEvaDefProt%\\Inline\\Inline.evp"));
 #endif


    //  ----------------------------------------------------------------------
    //  Instantiate of UI class
    //  ----------------------------------------------------------------------
    if ( (NLS_SEV & (lStatus = createUI (rSeqLim))) == NLS_SEV )
    {
        TRACE_PUT1(TC_ALWAYS, TF_SEQ,"%s: Instantiation of UI class failed: FlashBack::createUI(SeqLim&)", ptModule);
        return ( lStatus );
    }


#ifdef WIN32
    //  ----------------------------------------------------------------------
    //  Declaration of pointer to UI parameter classes
    //  ----------------------------------------------------------------------
    lStatus = m_pUI->registerUI (rSeqLim);

    if ( NLS_SEVERITY(lStatus) != NLS_SUCCESS )
    {
        TRACE_PUT1_NLS(TC_INFO, TF_SEQ, "%s : Initialization of UI failed : "  , ptModule, lStatus);
        return ( lStatus );
    }
#endif

    // Register Sequence/Special card parameters
    BEGIN_PARAMETER_MAP(&rSeqLim, 0, 0);

    // not used:
    // PARAM("Pulse Duration","us", &m_lPulseDuration,    1000, 6000, 500, 2000,  "Duration of sinc"); //fixed to 2000


    //#1
    PARAM("Shots",       ""  , &m_lShotsInScan,  1, 20,  1,    1,  "Defines the number of shots per slice");
    VERIFY_ALL(&m_lShotsInScan);
    //#2
    #ifdef VB_VERSION
      PARAM("Inversion angle", "", &m_lInversionAngle, 360, 720, 1, 360, "Deg");
    #else
      SKIP_PARAM();
    #endif
	
    /* //ASX		
    SKIP_PARAM();//#3
    SKIP_PARAM();//#4
    SKIP_PARAM();//#5
    SKIP_PARAM();//#6
	*/ //ASX
	//#3and #4
        #ifdef VB_VERSION
          PARAM("k-space shift",   "100ns"  , &m_lKshift,  -1000, 1000,  1,    0,  "k-space center shift");
          PARAM("FFT factor",   "a.u."  , &m_dFFTFactor,  0.01, 2.0,  0.01,    1.0,  "PD scale factor"); 
        #else
          PARAM("FFT factor",   "a.u."  , &m_dFFTFactor,  0.01, 2.0,  0.01,    1.0,  "PD scale factor");
          SKIP_PARAM();
        #endif
    
    //#5
    PARAM_SELECT("Slice mode",&m_sSliceMode, NORMAL_SLICES);
      OPTION("Normal", NORMAL_SLICES);
      OPTION("Radial", RADIAL_SLICES);
    PARAM_SELECT_END();
	
	//#
	SKIP_PARAM();
	PARAM_SELECT("ICE",&m_sIceMode, ICE_OFF);
      OPTION("ON", ICE_ON);
      OPTION("OFF", ICE_OFF);
    PARAM_SELECT_END();
	
	// page 2
	//ASX T1rho PARAM
	//#9
	PARAM_SELECT("Prep. Type",&m_sPrepType,T1RHO_PREP);
		OPTION("T1rho", T1RHO_PREP);
		OPTION("T2", T2_PREP);		
	PARAM_SELECT_END();
	//PARAM("Enable T1rho-T2",			&m_bUseT1rT2Prep, true);
	//#10
	PARAM("Fsl",			      "[Hz]",	&m_dSpinLockFreq_Hz,	       0,		  2000,			   1,		 500,   "Spinlock Power");
	//#11
	PARAM("Number of Tsls",		      "",		&m_lTslToMeasure ,   	       1,		   10 ,		 	   1,		  6,   "Number of TSLs");
	//#12
	PARAM("Tsl",				  "[ms]",	MAXTSL,	m_adTsl_us,		m_cadMin,	 m_cadMax ,		m_cadInc,	m_cadDef,   "PrepTimes (Tsls/TEs) ");
	//#13
	PARAM("T1 Delay",		      "[ms]",	    &m_lT1Delay_ms ,		  100,    10000 ,   1,   1000,   "T1 Restoration Delay");
	//#14
	PARAM("T1rho Flip Angle",        "[degree]", &m_dT1rFlipAngle, 1, 180, 1, 20, "maximum FA of the train After T1rho prep");
	//#15
	/*PARAM_SELECT("FA Mode",&m_sT1rTrainMode,VAR_MODE);
		OPTION("Variable", VAR_MODE);
		OPTION("Constant", CONST_MODE);		
	PARAM_SELECT_END();
	*/
	
	/* //ASX
    SKIP_PARAM(); //#11
    SKIP_PARAM(); //#12
    SKIP_PARAM(); //#13
    */																										  

    PARAM_SELECT_END();


    END_PARAMETER_MAP;

    cout<<"Finnished INIT"<<endl;

    return (lStatus);
}


NLSStatus IRFF::prepare (MrProt &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo)
{
    MrProt* pMrProt=&rMrProt;
    SeqLim* pSeqLim=&rSeqLim;
    SeqExpo* pSeqExpo=&rSeqExpo;

    static const char *ptModule = {"IRFF::prepare"};


    long         lI                    = 0;
    double       dMeasureTimeUsec      = 0.0;
    double       dTotalMeasureTimeMsec = 0.0;


    // ---------------------------------------------------------------------------
    // for the current release, we'd like to limit some of the user options
    // ---------------------------------------------------------------------------
   
	//ASX
	VISIBLE_ARRAY_SIZE(rSeqLim, m_adTsl_us, m_lTslToMeasure); //ASX
	
	//if (m_sPrepType == T2_PREP)
		//DISABLE_PARAM(rSeqLim,&m_dSpinLockFreq_Hz);
	//else
		//ENABLE_PARAM(rSeqLim,&m_dSpinLockFreq_Hz,true);

    // ---------------------------------------------------------------------------
    // Read parameters from the special card
    // ---------------------------------------------------------------------------
    PREPARE_PARAMETER_MAP(pMrProt, pSeqLim);


    // ---------------------------------------------------------------------------
    // Set default return status
    // ---------------------------------------------------------------------------
    NLS_STATUS   lStatus               = SEQU__NORMAL;

    if(m_lShotsInScan == 5){
      return(SEQU_ERROR);
    }

    if(m_lShotsInScan == 7){
      return(SEQU_ERROR);
    }



    // ---------------------------------------------------------------------------
    // Initialize the desired pulse sequence file
    // ---------------------------------------------------------------------------
	
	m_lNumberOfFlipangles=SizeOfSeriesFA+(m_lTslToMeasure*SEGMENT_SIZE_T1RHO); //ASX		   
    m_dFlipAnglesSeriesA =  new double[m_lNumberOfFlipangles];
    m_dFlipAngleArray= new double[m_lNumberOfFlipangles];
	pSeqExpo->setNSet(m_lNumberOfFlipangles);
    

	long lL=0;
	/* //ASX
	using namespace std;
	ofstream myfile ("test.txt");
	if (myfile.is_open())
		cout<<"file OPEN"<<endl;
	else
		cout<<"can't open file"<<endl;
	*/
	
	for( lI=0; lI<m_lNumberOfFlipangles; lI++)
	{
		//ASX
		if (lI < SizeOfSeriesFA)
		{
			m_dFlipAnglesSeriesA[lI] = tmpFA[lI];	
			m_dFlipAngleArray[lI] = m_dFlipAnglesSeriesA[lI]*pMrProt->flipAngle();		  
		}
		else
 	    {
			lL = (lI - SizeOfSeriesFA) % SEGMENT_SIZE_T1RHO;
			m_dFlipAnglesSeriesA[lI] = tmpT1rFA[lL];   
			m_dFlipAngleArray[lI] = m_dFlipAnglesSeriesA[lI] * m_dT1rFlipAngle;		   
		}
		//myfile<< m_dFlipAngleArray[lI]<<"	" ;  //ASX
    }  
	//myfile.close(); //ASX
	
	
	// ----------------------------------------------------------------------------
    // Prepare T1rho/T2 preparation module		//ASX
    // ----------------------------------------------------------------------------
	m_lT1rRequestPerMeasurement = m_lSlicesToMeasure  * m_lShotsInScan * rMrProt.averages();
	for (long lTsl=0;lTsl<m_lTslToMeasure;lTsl++)
	{
		m_T1rhoSBB[lTsl].addToSBBList (&m_mySBBList);
		m_T1rhoSBB[lTsl].setRequestsPerMeasurement(m_lSlicesToMeasure);
		if (m_sPrepType == T2_PREP)
			m_T1rhoSBB[lTsl].setdT1rhoPrepFreq_Hz(0);
		else
			m_T1rhoSBB[lTsl].setdT1rhoPrepFreq_Hz(m_dSpinLockFreq_Hz);			
		
		
		m_T1rhoSBB[lTsl].setlT1rhoPrepTime_us(long(m_adTsl_us[lTsl]));

		if (!m_T1rhoSBB[lTsl].prep(__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo)))
		{
			TRACE_PUT1(TC_ALWAYS, TF_SEQ,"%s:Prep of T1rhoSBB[lTsl] failed!", ptModule);
			return (m_T1rhoSBB[lTsl].getNLSStatus());
		}
	}
																							  
    
    // ---------------------------------------------------------------------------
    // Initialize variables for rf spoiler
    // ---------------------------------------------------------------------------
    m_dRFSpoilPhasePrevSlice     = 0.0;
    m_dRFSpoilIncrementPrevSlice = 0.0;
    m_dRFPrevSlicePosSag         = 999999.0;
    m_dRFPrevSlicePosCor         = 999999.0;
    m_dRFPrevSlicePosTra         = 999999.0;
    m_dRFPrevSliceNormalSag      = 999999.0;
    m_dRFPrevSliceNormalCor      = 999999.0;
    m_dRFPrevSliceNormalTra      = 999999.0;

    // ---------------------------------------------------------------------------
    // Define gradient rise times and strengths
    // ---------------------------------------------------------------------------
    m_dMinRiseTime = SysProperties::getGradMinRiseTime(rMrProt.gradSpec().mode());
    m_dGradMaxAmpl = SysProperties::getGradMaxAmpl(rMrProt.gradSpec().mode());

    long lMaxGradMinRampTime = fSDSRoundUpGRT( (long) (m_dGradMaxAmpl * m_dMinRiseTime));

    // ---------------------------------------------------------------------------
    // Set and Get the approriate matrix dimensions
    // ---------------------------------------------------------------------------

    rMrProt.kSpace().phaseEncodingLines( m_lShotsInScan );
    rMrProt.kSpace().echoLine(0); // <- without we get lots of ice warnings... //MC

    lStatus = rMrProt.kSpace().linesToMeasure( m_lLinesToMeasure );
    OnErrorPrintAndReturn(lStatus,"linesToMeasure");

    lStatus = rMrProt.kSpace().partitionsToMeasure( m_lPartitionsToMeasure );
    OnErrorPrintAndReturn(lStatus,"PartitionsToMeasure");


    // ---------------------------------------------------------------------------
    // Prepare the Genereic RF pulse objects  (SINC)
    // ---------------------------------------------------------------------------
    m_sSRFSinc.setFlipAngle           (rMrProt.flipAngle());
    m_sSRFSinc.setInitialPhase        (0);
#ifdef VB_VERSION
    m_sSRFSinc.setThickness           (rMrProt.sliceGroupList()[0].thickness());
#else
    m_sSRFSinc.setThickness           (rMrProt.sliceSeries().aFront().thickness());
#endif
    m_sSRFSinc.setDuration            (m_lPulseDuration);
    m_sSRFSinc.setSamples             (m_lPulseDuration); //dense sampling needed for construction of MB pulses
    m_sSRFSinc.setTypeExcitation      ();
    m_sSRFSinc.setBandwidthTimeProduct(RF_TIME_BANDWIDTH_PRODUCT);
    m_sSRFSinc.setAsymmetry           (0.5);

    m_sSRFSinc.setRunIndex(1);
    if (!m_sSRFSinc.setFlipAngleArray ( m_dFlipAngleArray, m_lNumberOfFlipangles ) ){
        TRACE_PUT1(TC_INFO, TF_SEQ, "%s Failed to prepare flip-angle array.", ptModule);
        return SEQU_ERROR;
    }

    if (! m_sSRFSinc.prepSinc(__REF(rMrProt), __REF(rSeqExpo))){
      TRACE_PUT1(TC_INFO, TF_SEQ, "%s: m_sSRFSinc.prepSinc failed ", ptModule);
      return (m_sSRFSinc.getNLSStatus());
    }



    // ---------------------------------------------------------------------------
    // Prepare the Genereic RF pulse objects (INVERSION)
    // ---------------------------------------------------------------------------
#ifdef VE_VERSION
    MrProtocolData::MrTXSpecData& rTXSpec = rMrProt.getsTXSPEC();       //VD13C PTX
    // if(m_sTXMode==PTX_MODE){
    //   rTXSpec.getasNucleusInfo()[0].setbHasPTXPulses(true);             //VD13C PTX
    //         rSeqExpo.setbTxScaleFactorsFromSeq(true);
    // }else{
      rTXSpec.getasNucleusInfo()[0].setbHasPTXPulses(false);            //VD13C PTX
      rSeqExpo.setbTxScaleFactorsFromSeq(false);
    // }
#endif


    m_sRF_FOCI.setFlipAngle          (m_lInversionAngle);
    m_sRF_FOCI.setInitialPhase       (0);
    m_sRF_FOCI.setThickness          (10000);
    m_sRF_FOCI.setDuration           (10240);
    m_sRF_FOCI.setSamples            (1024 );
    m_sRF_FOCI.setTypeUndefined      ();


    // double dNormalizedAmplitudeIntegral = 138.0;//m_sRF_HYP.m_Ctrl.getNormalizedAmplitudeIntegral();
    // set 138 to match amplitude of HypSec
    if (!m_sRF_FOCI.prepArbitrary(__REF(rMrProt),__REF(rSeqExpo), m_sFOCIsamples, 138.0)){
        if ( rSeqLim.isContextNormal() ){
            TRACE_PUT1(TC_INFO, TF_SEQ, "%s Failed to prepare the arb RF pulse.", ptModule);
        }
        return SEQU_ERROR;
    }




#ifdef VE_VERSION
    rMrProt.getsWipMemBlock().getalFree()[51] = m_lKshift ;
    rMrProt.getsWipMemBlock().getalFree()[52] = 100*m_dFFTFactor;  //NOISE scan enabled
#else
    pMrProt->wipMemBlock().alFree[51] = m_lKshift ;
    pMrProt->wipMemBlock().alFree[52] = 100*m_dFFTFactor;
#endif


    m_lSlicesToMeasure      = rMrProt.sliceSeries().getlSize();
    m_lPhasesToMeasure      = rMrProt.physiology().phases();
    m_lRepetitionsToMeasure = rMrProt.repetitions();

    // ---------------------------------------------------------------------------
    // Get information about used trigger (off)
    // ---------------------------------------------------------------------------
    rMrProt.physiology().getPhysioMode (m_FirstSignal, m_FirstMethod, m_SecondSignal, m_SecondMethod);



    // -------------------------------------------------------------------------------
    // Prepare readout (ADC) objects
    // -------------------------------------------------------------------------------
    m_sADC01.prep(rMrProt.kSpace().getlBaseResolution(), static_cast<long>(rMrProt.rxSpec().effDwellTime( rSeqLim.getReadoutOSFactor() )[0] + 0.5));
    m_sADC01.getMDH().setKSpaceCentreColumn((unsigned short) (0.5 * rMrProt.kSpace().getlBaseResolution()) );


    // -------------------------------------------------------------------------------
    // Prepare readout gradient
    // -------------------------------------------------------------------------------
    m_sGradReadout.setRampTimes(lMaxGradMinRampTime);
    m_sGradReadout.setDuration(fSDSRoundUpGRT(m_sADC01.getDuration()+m_sGradReadout.getRampUpTime()));
    if ( !m_sGradReadout.prepRO( __REF(rMrProt), m_sADC01.getDwellTime()) ){
        return m_sGradReadout.getNLSStatus();
    }


    // ----------------------------------------------------------------------------
    // Prepare slice selection gradient
    // ----------------------------------------------------------------------------
    m_lMySliSelRampTime =  fSDSRoundUpGRT(  MAX_DOUBLE(SysProperties::getCoilCtrlLead(),
                                           m_dMinRiseTime * m_sSRFSinc.getGSAmplitude())) ;

    if (! m_sGSliSel.prepAmplitude(
        m_lMySliSelRampTime,
        fSDSRoundUpGRT(m_lPulseDuration + m_lMySliSelRampTime),
        m_lMySliSelRampTime,
        m_sSRFSinc.getGSAmplitude() ) ){
       return(m_sGSliSel.getNLSStatus());
    }


    // ----------------------------------------------------------------------------
    // Calculate momenta needed
    // ----------------------------------------------------------------------------
    double dMomentForSliceRephase = -1.0 * m_sSRFSinc.getGSAmplitude() * ( m_lPulseDuration + m_lMySliSelRampTime ) / 2.;
    double dMomentForReadDephase  = -1.0 * m_sGradReadout.getAmplitude() * (m_sADC01.getDuration() + m_sGradReadout.getRampUpTime() )/2;

    // Estimate the precompensation moment for the gradient delays. This is different for VB and VD systems (3.0 vs 0.0)
#ifdef VB_VERSION
  #ifndef VERSED_ADC
    double dROPrecompensation = m_lKshift/10; //3.0 + m_lKshift/10.0;
    double dGradDelayMoment=-1.0 * dROPrecompensation * m_sGradReadout.getAmplitude();
    dMomentForReadDephase += dGradDelayMoment;
  #endif
#endif

    double dMomentForReadRephase = -1.0 * (dMomentForReadDephase + m_sGradReadout.getMomentumTOT() );
    double dSpoilMomentum = 1000000.0/(rMrProt.sliceSeries().aFront().thickness() * m_sADC01.getLarmorConst()); //s*mT/m
    dSpoilMomentum*= GRAD_SPOILER_SCALE;

    // ----------------------------------------------------------------------------
    // To be more conistant the effective spoiler momentum should be equivilent to:
    // m_sGSliSel.getAmplitude()*m_sGSliSel.getFlatTopTime();
    // To this end we need to also subtract any unwanted contributions from the first half of the slice selection gradient.
    //
    // ----------------------------------------------------------------------------
    dSpoilMomentum -=  m_sGSliSel.getRampUpTime()*m_sGSliSel.getAmplitude()/2.0;  //This accounts for the getRampUpTime;
    dSpoilMomentum -=  m_sGSliSel.getFlatTopTime()*m_sGSliSel.getAmplitude()/2.0; //This accounts for the first half of the pulse;


    // ----------------------------------------------------------------------------
    // Prepare slice refocusing gradient
    // ----------------------------------------------------------------------------
    m_sGSliSelReph.setMaxMagnitude(m_dGradMaxAmpl);
    m_sGSliSelReph.setMinRiseTime(m_dMinRiseTime);
    if (! m_sGSliSelReph.prepSymmetricTOTShortestTime( dMomentForSliceRephase)   ){ /*! EGA-04; EGA-02 !*/
        DEBUG_BY_REGISTRY( 128, " m_sGSliSelReph.prepSymmetricTOTShortestTime failed " );
        return (m_sGSliSelReph.getNLSStatus());
    }

    // ----------------------------------------------------------------------------
    // Prepare read dephasing gradient
    // ----------------------------------------------------------------------------
    m_sGReadDeph.setMaxMagnitude(m_dGradMaxAmpl);
    m_sGReadDeph.setMinRiseTime(fSDSRoundUpGRT(m_dMinRiseTime));
    if (! m_sGReadDeph.prepSymmetricTOTShortestTime(dMomentForReadDephase) )
    {
        DEBUG_BY_REGISTRY( 128, " sGReadDeph.prepSymmetricTOTShortestTime failed " );
        return (m_sGReadDeph.getNLSStatus());
    }


    // ----------------------------------------------------------------------------
    // Prepare read rephasor
    // ----------------------------------------------------------------------------
    m_sGReadReph.setMaxMagnitude(m_dGradMaxAmpl);
    m_sGReadReph.setMinRiseTime(fSDSRoundUpGRT(m_dMinRiseTime));
    if (! m_sGReadReph.prepSymmetricTOTShortestTime(dMomentForReadRephase) )
    {
        DEBUG_BY_REGISTRY( 128, " m_sGReadReph.prepSymmetricTOTShortestTime failed " );
        return (m_sGReadReph.getNLSStatus());
    }



    // ----------------------------------------------------------------------------
    // Calculate delay to TE1
    // ----------------------------------------------------------------------------
    long lMinTE1 = MAX( m_sGSliSel.getTotalTime()/2 - m_sGSliSel.getRampUpTime()  + m_sGradReadout.getTotalTime()/2 + m_sGReadDeph.getTotalTime()
                       ,m_sGSliSel.getTotalTime()/2 + m_sGSliSelReph.getTotalTime() + m_sGradReadout.getTotalTime()/2 - m_sGradReadout.getRampUpTime());


    if(rMrProt.te()[0] - lMinTE1<0){
      if (rSeqLim.isContextPrepForMrProtUpdate()){
          TRACE_PUT1(TC_INFO, TF_SEQ, "%s: pSeqLim->isContextPrepForMrProtUpdate()==TRUE, updating TE[0]\n",ptModule);
          rMrProt.te()[0] = lMinTE1;
      }else{
        DEBUG_BY_REGISTRY( 128, " reject pMrProt->te()[0] (tefill negative)" << rMrProt.te()[0] );
        return SEQU__NEGATIV_TEFILL;
      }
    }


    // ----------------------------------------------------------------------------
    // Stretch Dephasor & slice refocus
    // ----------------------------------------------------------------------------
    long lAvailibleReadDephTime  = rMrProt.te()[0] - m_sGSliSel.getTotalTime()/2 -m_sGradReadout.getTotalTime()/2 + m_sGSliSel.getRampDownTime();
    long lAvailibleSliceRephTime = rMrProt.te()[0] - m_sGSliSel.getTotalTime()/2 -m_sGradReadout.getTotalTime()/2 + m_sGradReadout.getRampUpTime();


    if (! m_sGReadDeph.prepSymmetricTOTExactMomentum( dMomentForReadDephase, lAvailibleReadDephTime)){
        DEBUG_BY_REGISTRY( 128, " sGReadDeph.prepSymmetricTOTExactMomentum failed " );
        return (m_sGReadDeph.getNLSStatus());
    }
    if (! m_sGSliSelReph.prepSymmetricTOTExactMomentum( dMomentForSliceRephase, lAvailibleSliceRephTime)){
        DEBUG_BY_REGISTRY( 128, " m_sGSliSelReph.prepSymmetricTOTExactMomentum failed " );
        return (m_sGSliSelReph.getNLSStatus());
    }



    // ----------------------------------------------------------------------------
    // Try to fit the spoiler & rephasor
    // ----------------------------------------------------------------------------
    m_lTRMin = m_sGSliSel.getTotalTime()+ lAvailibleSliceRephTime + m_sGradReadout.getTotalTime() - m_sGradReadout.getRampUpTime() + m_sGSliSel.getRampUpTime();


    long   lAvailibleRephTime    = rMrProt.tr()[0] - m_lTRMin;
    long   lAvailibleSpoilerTime = lAvailibleRephTime + m_sGradReadout.getRampDownTime();

    if(lAvailibleRephTime < 0){
      return SBB_NEGATIV_TRFILL;
    }

    // -------------------------------------------------------------------------------
    // Prepare readout (ADC) objects
    // -------------------------------------------------------------------------------
    if (! m_sGReadReph.prepSymmetricTOTExactMomentum(dMomentForReadRephase,lAvailibleRephTime) ){
        DEBUG_BY_REGISTRY( 128, " m_sGReadReph.prepSymmetricTOTExactMomentum failed " );
        return (m_sGReadReph.getNLSStatus());
    }
    if (! m_sGSpoil.prepSymmetricTOTExactMomentum( dSpoilMomentum, lAvailibleSpoilerTime)){
        DEBUG_BY_REGISTRY( 128, " m_sGSpoil.prepSymmetricTOTExactMomentum failed " );
        return (m_sGSpoil.getNLSStatus());
    }


    // ----------------------------------------------------------------------------
    // Try to fit Inversion Preparataion
    // ----------------------------------------------------------------------------
    if (m_sRF_FOCI.getDuration()/2 + 2*m_sGSliSel.getTotalTime() + m_sSRFSinc.getDuration()/2> 2*rMrProt.tr()[0]){
      DEBUG_BY_REGISTRY( 128, " Inversion prepration > 2xTR" );
      cout<<"Failed to fit inversion prepration module"<<endl;
      return (SEQU_ERROR);
    }


    m_sGReadDephR = m_sGReadDeph; m_sGReadDephR.setIdent("sGRdR");
    m_sGReadDephP = m_sGReadDeph; m_sGReadDephR.setIdent("sGRdP");

    m_sGReadRephR = m_sGReadReph; m_sGReadRephR.setIdent("sGRrR");
    m_sGReadRephP = m_sGReadReph; m_sGReadRephR.setIdent("sGRrP");

    m_sGradReadoutR = m_sGradReadout; m_sGradReadoutR.setIdent("sGRR");
    m_sGradReadoutP = m_sGradReadout; m_sGradReadoutP.setIdent("sGRP");



///-----------------------------------------------------------------------------------------------------------------------------
/// Versed readout!
///-----------------------------------------------------------------------------------------------------------------------------
#ifdef VERSED_ADC

    m_sGradReadoutR2 = m_sGradReadout; m_sGradReadoutR.setIdent("sGR2");
    m_sGradReadoutP2 = m_sGradReadout; m_sGradReadoutP.setIdent("sGR2");

    m_sGradReadoutR2.setAmplitude(m_sGradReadout.getAmplitude()/2);
    m_sGradReadoutP2.setAmplitude(m_sGradReadout.getAmplitude()/2);

    double dReadoutOffset  =  m_sGradReadoutR2.getMomentumTOT() - m_sGradReadoutR2.getAmplitude()*m_sGradReadout.getRampUpTime();

    double dReadoutMomentum = m_sGradReadout.getFlatTopTime()*m_sGradReadout.getAmplitude() - dReadoutOffset;
    double dOldRampMomentum = m_sGradReadout.getAmplitude()*m_sGradReadout.getRampUpTime()/2;
    double dNewRampMomentum = m_sGradReadoutR2.getAmplitude()*m_sGradReadout.getRampUpTime()/2;



    if ( !m_sGradReadoutR.prepMomentumTOT( m_sGradReadout.getFlatTopTime()/2, m_sGradReadout.getFlatTopTime()/2, m_sGradReadout.getFlatTopTime()/2 ,dReadoutMomentum)){
        return m_sGradReadoutR.getNLSStatus();
    }

    if ( !m_sGradReadoutP.prepMomentumTOT( m_sGradReadout.getFlatTopTime()/2, m_sGradReadout.getFlatTopTime()/2, m_sGradReadout.getFlatTopTime()/2 ,dReadoutMomentum)){
        return m_sGradReadoutP.getNLSStatus();
    }


    // if (! m_sGReadDephR.prepSymmetricTOTExactMomentum( dMomentForReadDephase + dOldRampMomentum, lAvailibleReadDephTime)){
    if (! m_sGReadDephR.prepSymmetricTOTExactMomentum( -dReadoutMomentum/2 -m_sGradReadoutR2.getMomentumTOT()/2, lAvailibleReadDephTime)){
        DEBUG_BY_REGISTRY( 128, " sGReadDephR.prepSymmetricTOTExactMomentum failed " );
        return (m_sGReadDephR.getNLSStatus());
    }
    if (! m_sGReadRephR.prepSymmetricTOTExactMomentum(-dReadoutMomentum/2 -m_sGradReadoutR2.getMomentumTOT()/2, lAvailibleRephTime) ){
        DEBUG_BY_REGISTRY( 128, " m_sGReadRephR.prepSymmetricTOTExactMomentum failed " );
        return (m_sGReadRephR.getNLSStatus());
    }

    if (! m_sGReadDephP.prepSymmetricTOTExactMomentum( -dReadoutMomentum/2 -m_sGradReadoutR2.getMomentumTOT()/2, lAvailibleReadDephTime)){
        DEBUG_BY_REGISTRY( 128, " sGReadDephR.prepSymmetricTOTExactMomentum failed " );
        return (m_sGReadDephP.getNLSStatus());
    }
    if (! m_sGReadRephP.prepSymmetricTOTExactMomentum(-dReadoutMomentum/2 -m_sGradReadoutR2.getMomentumTOT()/2, lAvailibleRephTime) ){
        DEBUG_BY_REGISTRY( 128, " m_sGReadRephP.prepSymmetricTOTExactMomentum failed " );
        return (m_sGReadRephP.getNLSStatus());
    }

    m_dVersedReadGradAmpl = m_sGradReadoutR.getAmplitude();
    m_dVersedDepGradAmpl = m_sGReadDephR.getAmplitude();
    m_dVersedRepGradAmpl = m_sGReadRephR.getAmplitude();

#endif


    // ----------------------------------------------------------------------------
    // Preparation of OscBit
    // ----------------------------------------------------------------------------
    #ifdef VB_VERSION
        m_sOscBit.lCode = SYNCCODE_OSC0;
    #else
        m_sOscBit.setCode(SYNCCODE_OSC0);
    #endif
    m_sOscBit.setIdent(RTEIDENT_Osc0);
    m_sOscBit.setDuration(10);
    m_sOscBit.setStartTime(0);
    m_sOscBit.prep( 0, 10 );

    // ------------------------------------------------------------------------
    // Set the receiver gain: For thin slices use high receiver gain
    // ------------------------------------------------------------------------
    if (rMrProt.sliceSeries().aFront().thickness() <= 10.0 ){  // unit is mm
        lStatus = fSSLSetRxGain (K_RX_GAIN_CODE_HIGH, __REF(rMrProt), __REF(rSeqLim));
        OnErrorPrintAndReturn (lStatus, "fSSLSetRxGain") ;
    }else{
        lStatus = fSSLSetRxGain (K_RX_GAIN_CODE_LOW, __REF(rMrProt), __REF(rSeqLim));
        OnErrorPrintAndReturn (lStatus, "fSSLSetRxGain") ;
    }

    // ---------------------------------------------------------------------------
    // Calculate the rotation matrices and positions for slices
    // ---------------------------------------------------------------------------
    lStatus = fSUPrepSlicePosArray (__REF(rMrProt), __REF(rSeqLim), m_asSLC);
    OnErrorPrintAndReturn (lStatus,  "fSUPrepSlicePosArray");

    // ---------------------------------------------------------------------------
    // Calculate total measurement time
    // ---------------------------------------------------------------------------
    m_lKernelRequestsPerMeasurement = m_lSlicesToMeasure  * m_lShotsInScan * m_lNumberOfFlipangles * rMrProt.averages();

    double dInversionSegmentDuration = (double) (2 * rMrProt.tr()[0] + m_sRF_FOCI.getDuration()/2 - m_sGSliSel.getTotalTime()/2);
    double dTotalDelayDuration       = (double) (3 * 50 * rMrProt.tr()[0]);  // one delay is filled with 50 noise scans
	double dT1rhoPrepDuration       = 0; //ASX
    dMeasureTimeUsec  = (double) ( m_lKernelRequestsPerMeasurement * rMrProt.tr()[0] ); //RF Train
    dMeasureTimeUsec += (double) ( dInversionSegmentDuration * m_lShotsInScan * m_lSlicesToMeasure );
    dMeasureTimeUsec += (double) ( dTotalDelayDuration       * m_lShotsInScan * m_lSlicesToMeasure );

	//ASX
	for (int lTsl=0; lTsl<m_lTslToMeasure; lTsl++)
	{
		dT1rhoPrepDuration += m_T1rhoSBB[lTsl].getDurationPerRequest(); 
		dT1rhoPrepDuration += fSDSRoundUpGRT(m_lT1Delay_ms*1000); 
	}
	dMeasureTimeUsec  += (double) (dT1rhoPrepDuration * m_lT1rRequestPerMeasurement);
						
    dMeasureTimeUsec = fSDSRoundUpGRT(dMeasureTimeUsec);

    // ---------------------------------------------------------------------------
    // Local SAR calculation for VE
    // ---------------------------------------------------------------------------
#ifdef VE_VERSION
    MrProtocolData::SeqExpoRFInfo  dRfEnergyInT1rPrep;  //ASX
	MrProtocolData::SeqExpoRFInfo dRfEnergyInSRFs;
    dRfEnergyInSRFs.clear();
    dRfEnergyInSRFs = getLocalSAR();
    dRfEnergyInSRFs+= m_sRF_FOCI.getRFInfo();
	
	dRfEnergyInT1rPrep= m_T1rhoSBB[0].getRFInfoPerRequest()  ; //ASX			
	for (long lTsl=1; lTsl<m_lTslToMeasure; lTsl++)
	{
		dRfEnergyInT1rPrep += m_T1rhoSBB[lTsl].getRFInfoPerRequest()  ; //ASX
	}		
	dRfEnergyInSRFs += dRfEnergyInT1rPrep; //ASX
	
    dRfEnergyInSRFs*= m_lSlicesToMeasure  * m_lShotsInScan * rMrProt.averages() * (rMrProt.repetitions()+1);

    rSeqExpo.setRFInfo (dRfEnergyInSRFs);
#else
	double  dRfEnergyInT1rPrep; //ASX								 
    double dRfEnergyInSRFs  = 0.0;
    for(m_lSetIndex=0; m_lSetIndex<m_lNumberOfFlipangles;m_lSetIndex++)
	{
      updateRFpointer();
      dRfEnergyInSRFs+=m_sSRFSinc.getPulseEnergyWs();
    }
     dRfEnergyInSRFs+=m_sRF_FOCI.getPulseEnergyWs();

	for (long lTsl=0; lTsl<m_lTslToMeasure; lTsl++)
	{
		dRfEnergyInT1rPrep += m_T1rhoSBB[lTsl]getEnergyPerRequest()  ; //ASX	
	}		
	dRfEnergyInSRFs += dRfEnergyInT1rPrep; //ASX

    m_lSetIndex=0;
    dRfEnergyInSRFs*= m_lSlicesToMeasure * m_lShotsInScan * rMrProt.averages() * (rMrProt.repetitions()+1);

    rSeqExpo.setRFEnergyInSequence_Ws(dRfEnergyInSRFs);
#endif

    // ---------------------------------------------------------------------------
    // Export information to pSeqExpo
    // ---------------------------------------------------------------------------
    rSeqExpo.setMeasuredPELines        (m_lLinesToMeasure);
    rSeqExpo.setMeasured3dPartitions   (m_lPartitionsToMeasure);

    rSeqExpo.setMeasureTimeUsec (dMeasureTimeUsec);
    lStatus=fSBBMeasRepetDelaysPrep(__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo),
                                    (dMeasureTimeUsec/1000.),
                                    &dTotalMeasureTimeMsec);
    OnErrorPrintAndReturn(lStatus,"fSBBMeasRepetDelaysPrep");

    rSeqExpo.setTotalMeasureTimeUsec (dTotalMeasureTimeMsec * 1000);
    rSeqExpo.setPreScans             (0);

    rSeqExpo.setRelevantReadoutsForMeasTime ( m_lShotsInScan * m_lSlicesToMeasure );
																				
    //m_lKernelCallsPerRelevantSignal =  MAX_LONG(1, (long)(m_lKernelRequestsPerMeasurement * 1000000. / dMeasureTimeUsec) );

    //rSeqExpo.setRelevantReadoutsForMeasTime ( m_lKernelRequestsPerMeasurement / m_lKernelCallsPerRelevantSignal );


    // ---------------------------------------------------------------------------
    // Phase correction for phase images
    // ---------------------------------------------------------------------------
    rSeqExpo.setNoOfPhaseCorrLines   (  16 * rMrProt.kSpace().getlBaseResolution() / 256);
    rSeqExpo.setLinSlopeLength       (  16 * rMrProt.kSpace().getlBaseResolution() / 256);
    rSeqExpo.setNoOfPhaseCorrColumns ( 128 * rMrProt.kSpace().getlBaseResolution() / 256);
    rSeqExpo.setColSlopeLength       ( 128 * rMrProt.kSpace().getlBaseResolution() / 256);
    rSeqExpo.setPCAlgorithm          (SEQ::PC_ALGORITHM_NONE);

    rSeqExpo.setAdditionalScaleFactor( GLOBAL_FFT_SCALE_FACTOR );

    // ----------------------------------------------------------------------
    // Set echo train length (-> used to set corresponding DICOM attributes)
    // ----------------------------------------------------------------------
    #ifdef VE_VERSION
        rSeqExpo.setEchoTrainLength         (static_cast<int32_t>(rMrProt.contrasts()) );
        rSeqExpo.setGradientEchoTrainLength (static_cast<int16_t>(rMrProt.contrasts()) );
        rSeqExpo.setRFEchoTrainLength       (static_cast<int16_t>(0) );
    #endif

    // ---------------------------------------------------------------------------
    // Set ICE Program parameters:
    // ---------------------------------------------------------------------------
    if (m_sIceMode == ICE_ON){
      rSeqExpo.setICEProgramFilename("%CustomerIceProgs%\\Ice4IRFF");
    }else{
      rSeqExpo.setICEProgramFilename("%CustomerIceProgs%\\EmptyIce4IRFF");
    }

    rSeqExpo.setSlicePerConcat(1);

    // Write the number of radial views into the protocol
    rMrProt.kSpace().radialViews(m_lShotsInScan);
    fSUSetSequenceString ("irff", __REF(rMrProt), __REF(rSeqExpo));


    // Tell the UI the Application Card to be used.
    rSeqExpo.setApplicationCard  (SEQ::APPLICATION_CARD_INLINE);

    // ---------------------------------------------------------------------------
    // Update special card
    // ---------------------------------------------------------------------------
    UPDATE_PROTOCOL(pMrProt, pSeqLim);

    // ---------------------------------------------------------------------------
    // End of sequence preparation
    // ---------------------------------------------------------------------------
    DEBUG_BY_REGISTRY( 128, "  everything prepped! ");
    if ( lStatus == SEQU__NORMAL ) DEBUG_BY_REGISTRY( 128, "  SEQU_NORMAL ");;
    return (lStatus) ;
}



// --------------------------------------------------------------------------
// Check of the sequence for gradient stimulation
// --------------------------------------------------------------------------
NLSStatus IRFF::check (MrProt  &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo, SEQCheckMode *pSEQCheckMode){
    static const char *ptModule = {"IRFF::check"};
    NLS_STATUS   lStatus = SEQU__NORMAL;


    //  ----------------------------------------------------------------------
    //  Sequence check:
    //     - max. gradient amplitude
    //     - GSWD look ahead
    //  ----------------------------------------------------------------------
    //PRINT_TRACE0 (rSeqLim, DEBUG_CHECK, DEBUG_CALL, "() >>>>") ;

    // Dummy to avoid compiler warning about unused variables
    #if !defined (VXWORKS) && !defined (BUILD_PLATFORM_LINUX)
        SEQCheckMode *dummy ;
        dummy = pSEQCheckMode ;
    #endif

    long lL = 0;
    long alLinesToCheck[4]      = { 0, m_lShotIndex-1, rSeqExpo.getMeasuredPELines()-2     , rSeqExpo.getMeasuredPELines()-1      };
    long alPartitionsToCheck[4] = { 0, 1, rSeqExpo.getMeasured3dPartitions()-2, rSeqExpo.getMeasured3dPartitions()-1 };

    // --------------------------------------------------------
    // Loop over extreme k-space-points and call fSEQRunKernel
    // --------------------------------------------------------
    while ( (lL < 2) && (((lStatus) & NLS_SEV) == NLS_SUCCESS) ){
        long lP = 0;
        lStatus = runKernel( rMrProt, rSeqLim, rSeqExpo, KERNEL_CHECK, 0, 0, alLinesToCheck[lL]);

        while ( rMrProt.kSpace().getucDimension() == SEQ::DIM_3    && (lP < 4) && (((lStatus) & NLS_SEV) == NLS_SUCCESS) ){
                lStatus = runKernel( rMrProt, rSeqLim, rSeqExpo, KERNEL_CHECK, 0, alPartitionsToCheck[lP], alLinesToCheck[lL]);
                lP++;
        }
        lL++;
    }

    //PRINT_TRACE0 (rSeqLim, DEBUG_CHECK, DEBUG_RETURN, "() <<<<") ;


    return(lStatus);
}



// --------------------------------------------------------------------------
// Execution of the sequence
// --------------------------------------------------------------------------
NLSStatus IRFF::run (MrProt &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo)
{
    static const char *ptModule = {"IRFF::run"};
    NLS_STATUS lStatus          = SEQU__NORMAL;

    // ---------------------------------------------------------------------------
    // Initialization of the unit test function
    // ---------------------------------------------------------------------------
    #ifdef VE_VERSION
        if(IS_UNIT_TEST_ACTIVE(rSeqLim)){
           SeqUT.DisableTestCase(lNoOfLastScanInConcatErr,        RTEB_ORIGIN_fSEQRunFinish, "Ice program treat set similar to repetitions!");
           SeqUT.DisableTestCase(lAlreadyGotLastScanInMeasFlag,   RTEB_ORIGIN_fSEQRunKernel, "Ice program treat set similar to repetitions!");
           SeqUT.DisableTestCase(lAddDimIndexOutOfRangeErr,       RTEB_ORIGIN_fSEQRunFinish, "Ice program treat set similar to repetitions!");
           SeqUT.DisableTestCase(lNoAddDimCheckedMomentErr,       RTEB_ORIGIN_fSEQRunKernel, "Radial 2D, no need to check other dim.");
           mSEQTest(rMrProt,rSeqLim,rSeqExpo,RTEB_ORIGIN_fSEQRunStart,0,0,0,0,0);
        }
    #else
        mSEQTest(__REF(rMrProt),__REF(rSeqLim),__REF(rSeqExpo),RTEB_ORIGIN_fSEQRunStart,0,0,0,0,0);
    #endif

    long lAverageInnerMax = (rMrProt.kSpace().getucAveragingMode() == SEQ::INNER_LOOP ) ? rMrProt.averages() : 1 ;
    long lAverageOuterMax = (rMrProt.kSpace().getucAveragingMode() == SEQ::INNER_LOOP ) ? 1 : rMrProt.averages() ;




    // ---------------------------------------------------------------------------
    // Special Slice placement option
    // ---------------------------------------------------------------------------
    if (m_sSliceMode == RADIAL_SLICES){
      if(m_lSlicesToMeasure>0) getSlicePos(&rMrProt, &rSeqLim, 0,  00.0);
      if(m_lSlicesToMeasure>1) getSlicePos(&rMrProt, &rSeqLim, 1,  90.0);

      if(m_lSlicesToMeasure>2) getSlicePos(&rMrProt, &rSeqLim, 2,  30.0);
      if(m_lSlicesToMeasure>3) getSlicePos(&rMrProt, &rSeqLim, 3, 120.0);

      if(m_lSlicesToMeasure>4) getSlicePos(&rMrProt, &rSeqLim, 4,  60.0);
      if(m_lSlicesToMeasure>5) getSlicePos(&rMrProt, &rSeqLim, 5, 150.0);
    }

    // ---------------------------------------------------------------------------
    // MAIN LOOP STRUCTURE
    // ---------------------------------------------------------------------------
    for (long lRepetition = 0; lRepetition <= m_lRepetitionsToMeasure ; lRepetition++)
    {
      // ---------------------------------------------------------------------------
      // Write Seq info into rawdata
      // ---------------------------------------------------------------------------
        m_lCurrKernelCall = 0;

        if ( rMrProt.intro() )
        {
            if (! m_TokTokSBB.run(__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo), &m_asSLC[0]) )
                return (m_TokTokSBB.getNLSStatus()) ;
        }

        m_sADC01.getMDH().setCrep ((unsigned short) lRepetition);

        fRTSetReadoutEnable(1);

        long lSlice      =0;
        long lSet        =0;
        // long lSegment    =0;


        long lSkip       = m_lSlicesToMeasure/2;

        // ---------------------------------------------------------------------------
        // ---------------------------------------------------------------------------
        // New Sequence timing
        // ---------------------------------------------------------------------------
        // ---------------------------------------------------------------------------
        for ( long lAverageOuter = 0 ; lAverageOuter < lAverageOuterMax ; lAverageOuter++ )
		{
			for ( long lAverageInner = 0 ; lAverageInner < lAverageInnerMax ; lAverageInner++ )
			{
				long lStartIndex = 0;
				for( lSlice = 0; lSlice < lSkip; lSlice++)
				{
					for ( m_lShotIndex = lStartIndex; m_lShotIndex < m_lShotsInScan; m_lShotIndex++ )
					{
						//=============================================================================
						//----------------------------------Start Slice i -----------------------------
						//=============================================================================
						for( m_lSetIndex = 0; m_lSetIndex<NUMBER_OF_NOISE_LINES; m_lSetIndex++)
						{
							m_lCurrKernelCall ++;
							lStatus = noiKernel(rMrProt, rSeqLim, rSeqExpo, lSlice, 0, 0);
							OnErrorPrintAndReturn(lStatus,"fSEQNoiKernel");
						}
						
						lStatus = runInversion(rMrProt,lSlice);
						OnErrorPrintAndReturn(lStatus,"fSEQIrKernel");
						
						runSegment(lSlice             , 0, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						fSBBFillTimeRun(fSDSRoundUpGRT( (float) 50*rMrProt.tr()[0]));
						runSegment(lSlice             , 1, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						fSBBFillTimeRun(fSDSRoundUpGRT( (float) 50*rMrProt.tr()[0]));
						runSegment(lSlice             , 2, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						fSBBFillTimeRun(fSDSRoundUpGRT( (float) 50*rMrProt.tr()[0]));
						runSegment(lSlice             , 3, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						//ASX - T1rho
						for (m_lTsl = 0; m_lTsl < m_lTslToMeasure; m_lTsl++)
						{
							fSBBFillTimeRun(fSDSRoundUpGRT( (float) m_lT1Delay_ms*1000));
							if (!m_T1rhoSBB[m_lTsl].run(__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo), &m_asSLC[lSlice]))
								return (m_T1rhoSBB[m_lTsl].getNLSStatus());
							
							runSegment(lSlice             , 4+m_lTsl, SEGMENT_SIZE_T1RHO, rMrProt, rSeqLim, rSeqExpo);
						}												 

						//=============================================================================
						//----------------------------------Start Slice i+N/2 -------------------------
						//=============================================================================
						for( m_lSetIndex = 0; m_lSetIndex<NUMBER_OF_NOISE_LINES; m_lSetIndex++)
						{
							m_lCurrKernelCall ++;
							lStatus = noiKernel(rMrProt, rSeqLim, rSeqExpo, lSlice+lSkip, 0, 0);
							OnErrorPrintAndReturn(lStatus,"fSEQNoiKernel");
						}

						lStatus = runInversion(rMrProt,lSlice+lSkip);
						OnErrorPrintAndReturn(lStatus,"fSEQIrKernel");							  

						runSegment(lSlice+lSkip          , 0, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						fSBBFillTimeRun(fSDSRoundUpGRT( (float) 50*rMrProt.tr()[0]));
						runSegment(lSlice+lSkip          , 1, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						fSBBFillTimeRun(fSDSRoundUpGRT( (float) 50*rMrProt.tr()[0]));
						runSegment(lSlice+lSkip          , 2, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						fSBBFillTimeRun(fSDSRoundUpGRT( (float) 50*rMrProt.tr()[0]));
						runSegment(lSlice+lSkip          , 3, SEGMENT_SIZE, rMrProt, rSeqLim, rSeqExpo);
						
						//ASX-T1rho
						for (m_lTsl = 0; m_lTsl < m_lTslToMeasure; m_lTsl++)
						{
							fSBBFillTimeRun(fSDSRoundUpGRT( (float) m_lT1Delay_ms*1000));
							if (!m_T1rhoSBB[m_lTsl].run(__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo), &m_asSLC[lSlice]))
								return (m_T1rhoSBB[m_lTsl].getNLSStatus());	
							runSegment(lSlice+lSkip       , 4+m_lTsl, SEGMENT_SIZE_T1RHO , rMrProt, rSeqLim, rSeqExpo);
						}												 
					}// end of shots loop
				}//slice
			}//avinn
		}//aveout


        // Send SyncEvents between Measurement repeats
        lStatus = fSBBMeasRepetDelaysRun ( __REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo), lRepetition );
        OnErrorPrintAndReturn(lStatus,"fSBBMeasRepetDelaysRun");

    }//rept

    // Tell sequence unit test that sequence run is finished.
    #ifdef VE_VERSION
        if(IS_UNIT_TEST_ACTIVE(rSeqLim)){
            mSEQTest(rMrProt,rSeqLim,rSeqExpo,RTEB_ORIGIN_fSEQRunFinish,0,0,0,0,0);
		}
    #else
        mSEQTest(__REF(rMrProt),__REF(rSeqLim),__REF(rSeqExpo),RTEB_ORIGIN_fSEQRunFinish,0,0,0,0,0);
    #endif


    return(lStatus);
}


NLSStatus IRFF::runSegment(long lSlice, long lSegment, long lSegmentSize, MrProt &rMrProt, SeqLim &rSeqLim, SeqExpo &rSeqExpo){
  NLS_STATUS lStatus          = SEQU__NORMAL;


  for (int i=0; i<lSegmentSize; i++ )
  {
      if (lSegment < 4) //ASX segment for train with inversion
	  {
		  m_lSetIndex = (lSegment*SEGMENT_SIZE +i + NUMBER_OF_NOISE_LINES )%m_lNumberOfFlipangles;
	  }
	  else
	  {
		//ASX
		 m_lSetIndex = (4*SEGMENT_SIZE  + NUMBER_OF_NOISE_LINES +(lSegment - 4)*lSegmentSize +i)%m_lNumberOfFlipangles;
	  }		 

#ifdef VE_VERSION
      if(IS_UNIT_TEST_ACTIVE(rSeqLim)){
          mSEQTest (rMrProt, rSeqLim, rSeqExpo, RTEB_ClockInitTR, 0, 0, m_asSLC[lSlice].getSliceIndex(), 0, 0);
      }
#else
      mSEQTest (__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo), RTEB_ClockInitTR, 0, 0, m_asSLC[lSlice].getSliceIndex(), 0, 0);
#endif
      m_lCurrKernelCall ++;	  
      if (m_lSetIndex == (m_lNumberOfFlipangles-1)){
          m_sADC01.setRelevantForMeasTime(); //only count last adc for each flip-angle train
      }
	  
#ifdef debug
      cout<<"m_lSetIndex : "<<m_lSetIndex<<endl;
#endif
      lStatus = runKernel(rMrProt, rSeqLim, rSeqExpo, KERNEL_IMAGE, lSlice, 0, 0);
      OnErrorPrintAndReturn(lStatus,"fSEQRunKernel");
  }
  return lStatus;
}



// --------------------------------------------------------------------------
// Executes the basic timing of the real-time sequence.
// --------------------------------------------------------------------------
NLS_STATUS IRFF::runKernel(MrProt &rMrProt,SeqLim &rSeqLim, SeqExpo &rSeqExpo, long lKernelMode, long lChronologicSlice, long lPartition, long lLine)
{
    static const char *ptModule = {"IRFF::runKernel"};
    NLS_STATUS         lStatus  = SEQU__NORMAL;

    unsigned long      ulTestIdent      = 0;                 // tell unit test whether we're running or checking the kernel
    long               lT               = 0;                 // used as clock time in the main event block
    long               lI               = 0;
    double             _angle           = 0;


    // The following lines are for the unit test: If the Kernel is called in Check mode, pass this information to the unit test.
    if (lKernelMode == KERNEL_CHECK){
        ulTestIdent = RTEB_ORIGIN_fSEQCheck;
    }else{
        ulTestIdent = RTEB_ORIGIN_fSEQRunKernel;
    }



    //. --------------------------------------------------------------------------
    //. Config RF
    //. --------------------------------------------------------------------------
    updateRFpointer();

    // Set the frequency/phase properties of the RF pulses. This will define the slice position.

    #ifdef VE_VERSION
    m_sSRF01zSet.prepSet(m_asSLC[lChronologicSlice],&m_sSRFSinc);
    m_sSRF01zNeg.prepNeg(m_asSLC[lChronologicSlice],&m_sSRFSinc);
    #endif
    #ifdef VB_VERSION
    m_sSRF01zSet.prepSet(m_asSLC[lChronologicSlice],m_sSRFSinc);
    m_sSRF01zNeg.prepNeg(m_asSLC[lChronologicSlice],m_sSRFSinc);
    #endif




    //. --------------------------------------------------------------------------
    //. Handle RF spoiling
    //. --------------------------------------------------------------------------
    if ( !m_bIsFISP_Segment ){
        m_dRFSpoilIncrement += RFSPOIL_INCREMENTdeg ;
        m_dRFSpoilPhase     += m_dRFSpoilIncrement ;
        m_dRFSpoilPhase      = fmod(m_dRFSpoilPhase,     (double) RFMAXPHASEdeg);
        m_dRFSpoilIncrement  = fmod(m_dRFSpoilIncrement, (double) RFMAXPHASEdeg);
        m_sSRF01zSet.increasePhase(m_dRFSpoilPhase);
        m_sSRF01zNeg.decreasePhase(m_dRFSpoilPhase);
    }else{
        m_sSRF01zSet.increasePhase(m_dRFSpoilPhase);
        m_sSRF01zNeg.decreasePhase(m_dRFSpoilPhase);
    }


    //printf("%d: Slice Index=%d \n", lChronologicSlice, asSLC[lChronologicSlice].getSliceIndex());


    //----------------------------------------------------------------------------------------
    // Estimate the angle for the spoke + the ID that should be written into the MDH
    //----------------------------------------------------------------------------------------
    getRotationAngle(_angle);

    double sinValue = sin(_angle);
    double cosValue = cos(_angle);

#ifdef VERSED_ADC

    m_sGReadDephR.setAmplitude(m_dVersedDepGradAmpl*cosValue);
    m_sGReadDephP.setAmplitude(m_dVersedDepGradAmpl*sinValue);
    m_sGradReadoutR.setAmplitude(m_dVersedReadGradAmpl*cosValue);
    m_sGradReadoutP.setAmplitude(m_dVersedReadGradAmpl*sinValue);
    m_sGradReadoutR2.setAmplitude(0.5*m_sGradReadout.getAmplitude()*cosValue);
    m_sGradReadoutP2.setAmplitude(0.5*m_sGradReadout.getAmplitude()*sinValue);
    m_sGReadRephR.setAmplitude(m_dVersedRepGradAmpl*cosValue);
    m_sGReadRephP.setAmplitude(m_dVersedRepGradAmpl*sinValue);

#else

    m_sGReadDephR.setAmplitude(m_sGReadDeph.getAmplitude()*cosValue);
    m_sGReadDephP.setAmplitude(m_sGReadDeph.getAmplitude()*sinValue);
    m_sGradReadoutR.setAmplitude(m_sGradReadout.getAmplitude()*cosValue);
    m_sGradReadoutP.setAmplitude(m_sGradReadout.getAmplitude()*sinValue);
    m_sGReadRephR.setAmplitude(m_sGReadReph.getAmplitude()*cosValue);
    m_sGReadRephP.setAmplitude(m_sGReadReph.getAmplitude()*sinValue);

#endif
    unsigned short angleEcho1ShortPart1 = (unsigned short) floor(_angle*10000);
    unsigned short angleEcho1ShortPart2 = (unsigned short) floor((_angle*10000-floor(_angle*10000))*10000);


    #ifdef debug
    cout<<"angle "<<_angle<<endl;
    cout<<"angleEcho1ShortPart1 "<<angleEcho1ShortPart1<<endl;
    cout<<"angleEcho1ShortPart2 "<<angleEcho1ShortPart2<<endl;
    #endif


    m_sADC01.getMDH().setFirstScanInSlice   ( m_lShotIndex == 0);
    m_sADC01.getMDH().setLastScanInSlice    ( m_lShotIndex == m_lShotsInScan-1);
    m_sADC01.getMDH().setLastScanInMeas     ((m_lShotIndex == m_lShotsInScan-1) && (lChronologicSlice==m_lSlicesToMeasure-1));
    m_sADC01.getMDH().setLastScanInConcat   ( m_lShotIndex == m_lShotsInScan-1);
    m_sADC01.getMDH().addToEvalInfoMask     ( MDH_ONLINE );


    // Set the  MDH parameters
    m_sADC01.getMDH().setKSpaceCentreLineNo (0);
    m_sADC01.getMDH().setKSpaceCentrePartitionNo (0);
    m_sADC01.getMDH().setCset ((unsigned short) m_lSetIndex);
    m_sADC01.getMDH().setCslc(lChronologicSlice);
    m_sADC01.getMDH().setClin(m_lShotIndex);          // what's the current line?
    m_sADC01.getMDH().setCeco(0);
    m_sADC01.getMDH().setCpar(0);
    m_sADC01.getMDH().setCphs(0); //?
    m_sADC01.getMDH().setCacq(0); //?
    m_sADC01.getMDH().setFreeParameterByIndex (0, angleEcho1ShortPart1 );
    m_sADC01.getMDH().setFreeParameterByIndex (1, angleEcho1ShortPart2 );
    m_sADC01.getMDH().setFreeParameterByIndex (2, (unsigned short) 0 ); // <-signal scan
    m_sADC01.getMDH().setFreeParameterByIndex (3, (unsigned short) 0 );

    // m_sADC01.getMDH().setEvalInfoMask (m_sADC01.getMDH()getEvalInfoMask() | MDH_ONLINE) ;
    // m_sADC01.getMDH().setPhaseFT    (m_lShotsInScan); //try to avoid ice warnings
    m_sADC01.getMDH().setPhaseFT    ( m_lShotIndex +1 == m_lShotsInScan ); //try to avoid ice warnings
    m_sADC01.getMDH().setPartitionFT( lPartition +1 == m_lPartitionsToMeasure );            //try to avoid ice warnings


    //. --------------------------------------------------------------------------
    //.  Readout phase
    //. --------------------------------------------------------------------------
    double dFreqRO = m_asSLC[lChronologicSlice].getSliceOffCenterRO()*(m_sGradReadoutR.getAmplitude());
    double dFreqPE = m_asSLC[lChronologicSlice].getSliceOffCenterPE()*(m_sGradReadoutP.getAmplitude());
    // double dPhase3D = m_asSLC[lChronologicSlice].getPhaseOffCenter3D()*lKSpacePartition;

    double dkcenterADC = 0.5;
    // double dFrequencyValue = m_sADC01.getLarmorConst()*(dFreqRO + dFreqPE + dFreq3D);
    double dFrequencyValue = m_sADC01.getLarmorConst()*(dFreqRO + dFreqPE);

    double dPhaseValuePos = -0.00036*dkcenterADC*m_sADC01.getDuration()*dFrequencyValue;
    double dPhaseValueNeg = -0.00036*((double)m_sADC01.getRoundedDuration() - dkcenterADC*m_sADC01.getDuration())*dFrequencyValue;

    m_sADC01zSet.set(m_sADC01.getIdent(), dFrequencyValue, dPhaseValuePos);
    m_sADC01zNeg.set(m_sADC01.getIdent(), 0, dPhaseValueNeg);


    m_sADC01zSet.increasePhase(m_dRFSpoilPhase);
    m_sADC01zNeg.decreasePhase(m_dRFSpoilPhase);


    #ifdef VB_VERSION
        clockShiftCorrection (&rMrProt, &rSeqLim, &rSeqExpo);
    #endif


    // Begin of event block
#ifdef VE_VERSION
    fRTEBInit(m_asSLC[lChronologicSlice].getROT_MATRIX());
#else
    fRTEBInit(&m_asSLC[lChronologicSlice].m_sROT_MATRIX);
#endif

    //- ***************************************************** S E Q U E N C E   T I M I N G *************************************************************************
    //- *               Start Time             |      NCO      |    SRF    |    ADC    |                       Gradient Events                     |       Sync
    //- *                 (usec)               |     Event     |   Event   |   Event   |       phase       |       read        |       slice       |      Event
    //- *************************************************************************************************************************************************************

    // EXCITATION ===================================================================================================================================================
    fRTEI(lT =0                                 ,              0,          0,          0,                 0,                  0,        &m_sGSliSel,       &m_sOscBit);
    fRTEI(lT+=m_sGSliSel.getRampUpTime()        ,&m_sSRF01zSet  ,&m_sSRFSinc,          0,                 0,                  0,                  0,                0);
    fRTEI(lT+ m_sSRFSinc.getDuration()          ,&m_sSRF01zNeg  ,          0,          0,                 0,                  0,                  0,                0);
    fRTEI(lT+ m_sGSliSel.getDuration()          ,              0,          0,          0,                 0,                  0,    &m_sGSliSelReph,                0);
    // EXCITATION ===================================================================================================================================================

#ifdef VERSED_ADC
    // READOUT===========================================================================================================================================================
    fRTEI(lT =m_sGSliSel.getDuration()             ,              0,          0,        0,   &m_sGReadDephP,     &m_sGReadDephR,                  0,                0);
    fRTEI(lT+=m_sGReadDeph.getTotalTime()          ,              0,          0,        0,&m_sGradReadoutP2,  &m_sGradReadoutR2,                  0,                0);
    fRTEI(lT+=m_sGradReadout.getRampUpTime()       ,              0,          0,        0, &m_sGradReadoutP,   &m_sGradReadoutR,                  0,                0);
    fRTEI(lT                                       ,  &m_sADC01zSet,          0,&m_sADC01,                0,                  0,                  0,                0);
    fRTEI(lT+ m_sADC01.getDuration()               ,  &m_sADC01zNeg,          0,        0,                0,                  0,                  0,                0);
    fRTEI(lT+ m_sGradReadout.getDuration()         ,              0,          0,        0,   &m_sGReadRephP,     &m_sGReadRephR,                  0,                0);
    // READOUT===========================================================================================================================================================
#else
  // READOUT===========================================================================================================================================================
  fRTEI(lT =m_sGSliSel.getDuration()             ,              0,          0,        0,   &m_sGReadDephP,     &m_sGReadDephR,                  0,                0);
  fRTEI(lT+=m_sGReadDeph.getTotalTime()          ,              0,          0,        0, &m_sGradReadoutP,   &m_sGradReadoutR,                  0,                0);
  fRTEI(lT+=m_sGradReadout.getRampUpTime()       ,  &m_sADC01zSet,          0,&m_sADC01,                0,                  0,                  0,                0);
  fRTEI(lT+ m_sADC01.getDuration()               ,  &m_sADC01zNeg,          0,        0,                0,                  0,                  0,                0);
  fRTEI(lT+ m_sGradReadout.getDuration()         ,              0,          0,        0,   &m_sGReadRephP,     &m_sGReadRephR,                  0,                0);
  // READOUT===========================================================================================================================================================
#endif
    // SPOILER ==========================================================================================================================================================
    fRTEI(lT+ m_sGradReadout.getFlatTopTime()     ,              0,           0,        0,                 0,                  0,         &m_sGSpoil,                0);
    // SPOILER ==========================================================================================================================================================


    // FILLTR ===========================================================================================================================================================
    fRTEI(fSDSRoundUpGRT(rMrProt.tr()[0]), 0,0,0,0,0,0,0);
    // FILLTR ===========================================================================================================================================================

    // ---------------------------------------------------------------------------
    // Handle Unit test
    // ---------------------------------------------------------------------------
#ifdef VE_VERSION
    if(IS_UNIT_TEST_ACTIVE(rSeqLim)){
        mSEQTest (rMrProt, rSeqLim, rSeqExpo, ulTestIdent    , 10, m_lShotIndex,
                  m_asSLC[lChronologicSlice].getSliceIndex(), 0, 0) ;
        mSEQTest (rMrProt, rSeqLim, rSeqExpo, RTEB_ClockCheck, 10, m_lShotIndex,
                  m_asSLC[lChronologicSlice].getSliceIndex(), 0, 0) ;
    }
#else
    mSEQTest (__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo), ulTestIdent    , 10, m_lShotIndex,
              m_asSLC[lChronologicSlice].getSliceIndex(), 0, 0) ;
    mSEQTest (__REF(rMrProt), __REF(rSeqLim), __REF(rSeqExpo), RTEB_ClockCheck, 10, m_lShotIndex,
              m_asSLC[lChronologicSlice].getSliceIndex(), 0, 0) ;
#endif

    // ---------------------------------------------------------------------------
    // Close the Event block
    // ---------------------------------------------------------------------------
    OnErrorPrintAndReturn(lStatus = fRTEBFinish(),"fRTEBFinish [*0100*]");

    return (lStatus);
}


// --------------------------------------------------------------------------
// Noise Scan kernel.
// --------------------------------------------------------------------------
NLS_STATUS IRFF::noiKernel(MrProt &rMrProt,SeqLim &rSeqLim, SeqExpo &rSeqExpo, long lChronologicSlice, long lPartition, long lLine)
{
    static const char *ptModule = {"IRFF::noiKernel"};
    NLS_STATUS         lStatus  = SEQU__NORMAL;

    long               lT               = 0;                 // used as clock time in the main event block
    long               lI               = 0;
    double             _angle           = 0;


    //----------------------------------------------------------------------------------------
    // Estimate the angle for the spoke + the ID that should be written into the MDH
    //----------------------------------------------------------------------------------------

    getRotationAngle(_angle);
    unsigned short angleEcho1ShortPart1 = (unsigned short) floor(_angle);
    unsigned short angleEcho1ShortPart2 = (unsigned short) floor(_angle);


    m_sADC01.getMDH().setFirstScanInSlice   ( m_lShotIndex == 0);
    m_sADC01.getMDH().setLastScanInSlice    ( m_lShotIndex == m_lShotsInScan-1);
    m_sADC01.getMDH().setLastScanInMeas     ((m_lShotIndex == m_lShotsInScan-1) && (lChronologicSlice==m_lSlicesToMeasure-1));
    m_sADC01.getMDH().setLastScanInConcat   ( m_lShotIndex == m_lShotsInScan-1);
    m_sADC01.getMDH().addToEvalInfoMask     ( MDH_ONLINE );


    // Set the  MDH parameters
    m_sADC01.getMDH().setKSpaceCentreLineNo (0);
    m_sADC01.getMDH().setKSpaceCentrePartitionNo (0);
    m_sADC01.getMDH().setCset ((unsigned short) m_lSetIndex);
    m_sADC01.getMDH().setCslc(lChronologicSlice);
    m_sADC01.getMDH().setClin(m_lShotIndex);          // what's the current line?
    m_sADC01.getMDH().setCeco(0);
    m_sADC01.getMDH().setCpar(0);
    m_sADC01.getMDH().setCphs(0);//
    m_sADC01.getMDH().setCacq(0);//?
    m_sADC01.getMDH().setFreeParameterByIndex (0, angleEcho1ShortPart1 );
    m_sADC01.getMDH().setFreeParameterByIndex (1, angleEcho1ShortPart2 );
    m_sADC01.getMDH().setFreeParameterByIndex (2, (unsigned short) 1 );  // <- NOISE SCAN
    m_sADC01.getMDH().setFreeParameterByIndex (3, (unsigned short) 0 );

    // m_sADC01.getMDH().setEvalInfoMask (m_sADC01.getMDH()getEvalInfoMask() | MDH_ONLINE) ;

    m_sADC01.getMDH().setPhaseFT    ( m_lShotIndex +1 == m_lShotsInScan );         //try to avoid ice warnings
    m_sADC01.getMDH().setPartitionFT( lPartition +1 == m_lPartitionsToMeasure );  //try to avoid ice warnings



    //. --------------------------------------------------------------------------
    //.  Readout phase
    //. --------------------------------------------------------------------------
    m_sADC01zSet.set(m_sADC01.getIdent(), 0, 0);
    m_sADC01zNeg.set(m_sADC01.getIdent(), 0, 0);


    #ifdef VB_VERSION
    #ifndef VERSED_ADC
        clockShiftCorrection (&rMrProt, &rSeqLim, &rSeqExpo);
    #endif
    #endif


    // Begin of event block
#ifdef VE_VERSION
    fRTEBInit(m_asSLC[lChronologicSlice].getROT_MATRIX());
#else
    fRTEBInit(&m_asSLC[lChronologicSlice].m_sROT_MATRIX);
#endif

    //- ***************************************************** S E Q U E N C E   T I M I N G *************************************************************************
    //- *               Start Time             |      NCO      |    SRF    |    ADC    |                       Gradient Events                     |       Sync
    //- *                 (usec)               |     Event     |   Event   |   Event   |       phase       |       read        |       slice       |      Event
    //- *************************************************************************************************************************************************************

    // NOISE READOUT ===================================================================================================================================================
    fRTEI(lT =0                                 ,              0,          0,          0,                 0,                  0,                  0,       &m_sOscBit);
    fRTEI(lT =100                               ,  &m_sADC01zSet,          0,  &m_sADC01,                 0,                  0,                  0,                0);
    fRTEI(lT+ m_sADC01.getDuration()            ,  &m_sADC01zNeg,          0,          0,                 0,                  0,                  0,                0);
    fRTEI( fSDSRoundUpGRT(rMrProt.tr()[0])      ,              0,          0,          0,                 0,                  0,                  0,                0);
    // fRTEI( fSDSRoundUpGRT(lT)                   ,              0,          0,          0,                 0,                  0,                  0,                0);
    // NOISE READOUT ===================================================================================================================================================


    // ---------------------------------------------------------------------------
    // Close the Event block
    // ---------------------------------------------------------------------------
    OnErrorPrintAndReturn(lStatus = fRTEBFinish(),"fRTEBFinish [*0100*]");

    return (lStatus);
}



//+ [ Function ****************************************************************
//+
//+  Name        : Helper functions
//+
//+ ***************************************************************************

void IRFF::updateRFpointer(){
  m_bIsFISP_Segment = true;

  if(m_lSetIndex>2*SEGMENT_SIZE + NUMBER_OF_NOISE_LINES){
    m_bIsFISP_Segment = false;
  }

  // ---------------------------------------------------------------------------
  // Setup RF pulse
  // ---------------------------------------------------------------------------
  m_sSRFSinc.setRunIndex(m_lSetIndex%m_lNumberOfFlipangles);
  m_sSRFSinc.setInitialPhase(0);

}

// ---------------------------------------------------------------------------
// Local SAR caclulation for VD Series
// ---------------------------------------------------------------------------
#if defined(VE_VERSION)
MrProtocolData::SeqExpoRFInfo IRFF::getLocalSAR(){

  MrProtocolData::SeqExpoRFInfo dRfEnergy;
  dRfEnergy.clear();
  if (! pSeqLim->isContextPrepForBinarySearch() ){
    // ---------------------------------------------------------------------------
    // The Real SAR Caclulation is to slow for the binaray search!
    // ---------------------------------------------------------------------------
    for (m_lSetIndex = 0; m_lSetIndex<m_lNumberOfFlipangles; m_lSetIndex++){
      updateRFpointer();
      dRfEnergy += m_sSRFSinc.getRFInfo((unsigned int) m_lSetIndex);
    }
    m_lSetIndex=0;
  }else{
    // ---------------------------------------------------------------------------
    // Quick upper limit for the binary serach (Over estimation!)
    // ---------------------------------------------------------------------------
    double dMax              = 0.0;
    unsigned int uiMaxIndex  = 0;
    long lI                  = 0;

    for (lI = 0; lI<m_lNumberOfFlipangles; lI++){
      if(m_dFlipAngleArray[lI]>dMax){
        dMax = m_dFlipAngleArray[lI];
        uiMaxIndex =(unsigned int) lI;
      }
    }
    updateRFpointer();
    dRfEnergy = m_sSRFSinc.getRFInfo(uiMaxIndex);
    dRfEnergy*= m_lNumberOfFlipangles;
  }

  return dRfEnergy;
}
#endif



// -------------------------------------------------------------------------
// Rotate Field of View
// -------------------------------------------------------------------------
void IRFF::getRotationAngle(double &angle){

    angle   =   (m_lSetIndex-NUMBER_OF_NOISE_LINES) * NYU_GOLDENANGLE *m_lShotsInScan;
    angle  +=   m_lShotIndex * NYU_GOLDENANGLE;


    angle  *=   M_PI/180.0;

    if(m_lShotIndex%2==0){
        angle     += M_PI;
    }

    angle =  fmod(angle, TWOPI);
}


void IRFF::getSlicePos(MrProt *pMrProt, SeqLim * pSeqLim, long lIndex, double dAngle){

  OSlice slice = pMrProt->sliceSeries().chronological(0);
  VectorPat<double> old_normal_vector = slice.normal();
  VectorPat<double> old_normal_read;
  VectorPat<double> old_normal_phas;
  slice.orientation(old_normal_phas,old_normal_read);


  double dC1 = old_normal_read.dSag;
  double dC2 = old_normal_read.dCor;
  double dC3 = old_normal_read.dTra;

  double dD1 = old_normal_phas.dSag;
  double dD2 = old_normal_phas.dCor;
  double dD3 = old_normal_phas.dTra;


  double dE1 = dC1*cos(PI*dAngle/180) +dD1*sin(PI*dAngle/180);
  double dE2 = dC2*cos(PI*dAngle/180) +dD2*sin(PI*dAngle/180);
  double dE3 = dC3*cos(PI*dAngle/180) +dD3*sin(PI*dAngle/180);



  slice.normal(dE1,dE2,dE3);//, true);
  #ifdef VE_VERSION
      m_asSLC[lIndex].prep( *pMrProt,*pSeqLim, slice, lIndex );
  #else
      m_asSLC[lIndex].prep( pMrProt, pSeqLim, slice, lIndex );
  #endif

  //Return the protocol <-- needed because of some leaky code in idea?
  slice.normal(old_normal_vector.dSag,old_normal_vector.dCor,old_normal_vector.dTra);//, true);

}


// -------------------------------------------------------------------------
// Run an adiabatic inversion
// -------------------------------------------------------------------------
NLSStatus IRFF::runInversion(MrProt &rMrProt, long lSlice){

  long lT          =0;
  static const char *ptModule = {"IRFF::irKernel"};
  NLS_STATUS         lStatus  = SEQU__NORMAL;

  #ifdef VE_VERSION
    m_sSRF01zSet.prepSet(m_asSLC[lSlice], &m_sRF_FOCI);
    m_sSRF01zNeg.prepNeg(m_asSLC[lSlice], &m_sRF_FOCI);
    fRTEBInit(m_asSLC[lSlice].getROT_MATRIX());
  #else
    m_sSRF01zSet.prepSet(m_asSLC[lSlice], m_sRF_FOCI);
    m_sSRF01zNeg.prepNeg(m_asSLC[lSlice], m_sRF_FOCI);
    fRTEBInit(&m_asSLC[lSlice].m_sROT_MATRIX);
  #endif

  long lIrBlockDuration = fSDSRoundUpGRT(2*rMrProt.tr()[0]-m_sGSliSel.getTotalTime()/2 + m_sRF_FOCI.getDuration()/2);

  //- ***************************************************** S E Q U E N C E   T I M I N G *************************************************************************
  //- *               Start Time             |      NCO      |    SRF    |    ADC    |                       Gradient Events                     |       Sync
  //- *                 (usec)               |     Event     |   Event   |   Event   |       phase       |       read        |       slice       |      Event
  //- *************************************************************************************************************************************************************

  // INVERSION ===================================================================================================================================================
  fRTEI(lT =0                                 ,  &m_sSRF01zSet,&m_sRF_FOCI,          0,                 0,                  0,                  0,       &m_sOscBit);
  fRTEI(lT+=m_sRF_FOCI.getDuration()          ,  &m_sSRF01zNeg,          0,          0,                 0,                  0,        &m_sGSliSel,                0);
  fRTEI(lT+=m_sGSliSel.getDuration()          ,              0,          0,          0,                 0,                  0,        &m_sGSliSel,                0);
  fRTEI(lT =lIrBlockDuration                  ,              0,          0,          0,                 0,                  0,                  0,                0);
  // INVERSION ===================================================================================================================================================

  // fSBBFillTimeRun(fSDSRoundUpGRT(2*rMrProt.tr()[0]-m_sGSliSel.getTotalTime()/2 + m_sRF_FOCI.getDuration()/2));

  // ---------------------------------------------------------------------------
  // Close the Event block
  // ---------------------------------------------------------------------------
      OnErrorPrintAndReturn(lStatus = fRTEBFinish(),"fRTEBFinish [*0100*]");
  // OnErrorPrintAndReturn(fRTEBFinish(), "fRTEBFinish [*0100*]");

  return (lStatus);
}




// -------------------------------------------------------------------------
// Instantiation of UI classes
// -------------------------------------------------------------------------
NLS_STATUS IRFF::createUI (SeqLim&)
{
    static const char *ptModule = {"IRFF::createUI"};

#ifdef WIN32

    //  Delete existing instance if necessary
    if ( m_pUI ){
        delete m_pUI;
        m_pUI = 0;
    }

    //  Instantiation of the UI class
    try{
        m_pUI = new IRFF_UI();
    }

    catch (...){
        delete m_pUI;
        m_pUI = NULL;

        TRACE_PUT1(TC_ALWAYS, TF_SEQ,"%s: Cannot instantiate UI class !", ptModule);
        return ( SEQU_ERROR );
    }

#endif
    return ( SEQU_NORMAL );
}


const IRFF_UI* IRFF::getUI (void) const
{
    return ( m_pUI );
}

#ifdef VB_VERSION
NLS_STATUS IRFF::clockShiftCorrection (MrProt* pMrProt, SeqLim* pSeqLim, SeqExpo* pSeqExpo)
{
    double dLinCSDelay=-0.5;
    double dConstCSDelay=-2.0e-6;

    double dTos_us       = m_sADC01.getDwellTime() * 1.e-3 / pSeqLim->getReadoutOSFactor();
    double dClockFreq_Hz = m_sADC01zSet.getFrequency(); // Get frequency of NCO

    double dAddPhase     = dClockFreq_Hz * (dLinCSDelay * 1e-6 * dTos_us + dConstCSDelay) * 360.0;

    // Modify ADC phase to compensate for delay between ADC and clock
    m_sADC01zSet.increasePhase( dAddPhase );
    m_sADC01zNeg.decreasePhase( dAddPhase );

    return SEQU__NORMAL;
}
#endif
