#ifndef IRFF_UI_h
#define IRFF_UI_h 1


//  -------------------------------------------------------------------------- *
//  Application includes                                                       *
//  -------------------------------------------------------------------------- *
#include "./IRFF.h"
//#include "MrServers/MrImaging/libUICtrl/UICtrl.h"
  #include "MrServers/MrProtSrv/MrProtocol/libUICtrl/UICtrl.h" //mz: VD13-specific

#include "MrServers/MrProtSrv/MrProt/Physiology/MrPhysiology.h"
#include "MrServers/MrProtSrv/MrProt/KSpace/MrKSpace.h"


#ifdef WIN32
    #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkLimited.h"
    #include "MrServers/MrProtSrv/MrProtocol/libUILink/UILinkSelection.h"
#endif


//  -------------------------------------------------------------------------- *
//  Forward declarations                                                       *
//  -------------------------------------------------------------------------- *
class MrProt;
class SeqLim;
class SeqExpo;
class Sequence;


namespace SEQ_NAMESPACE
{


    class IRFF_UI
    {
    public:

        IRFF_UI();
        virtual ~IRFF_UI();
        virtual NLS_STATUS registerUI (SeqLim &rSeqLim);

        #ifdef WIN32
        #endif
    };



}//END of namespace

#endif
