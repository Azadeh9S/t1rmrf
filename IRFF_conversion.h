

#ifndef IRFF_conversion_h
#define IRFF_conversion_h 1


// --------------------------------------------------------------------------------------
// Macros to switch between RFbase object class name definition
// --------------------------------------------------------------------------------------
#ifdef VE_VERSION
  #define BASE_RF_PULSE IRF_PULSE
#else
  #define BASE_RF_PULSE sRF_PULSE
#endif


// --------------------------------------------------------------------------------------
// Macros for protocol access under VB-line versions
// --------------------------------------------------------------------------------------

#ifndef VE_VERSION
    #define getalFree() alFree
    #define getadFree() adFree
    #define getsWipMemBlock() wipMemBlock()
    #define getsGRADSPEC() gradSpec()
    #define getucMode() mode()
    #define getMDH() Mdh
    #define getROT_MATRIX() m_sROT_MATRIX
    #define getlBaseResolution() baseResolution()
    #define getlSize() size()
    #define getucMultiSliceMode() multiSliceMode()
    #define getucDimension() dimension()
    #define getucAveragingMode() averagingMode()
    #define getulEnableRFSpoiling() RFSpoiling()
    #define getROT_MATRIX() m_sROT_MATRIX
#endif

#ifndef VE_VERSION
    #define MAX_LONG MAX
    #define MIN_LONG MIN
    #define MAX_DOUBLE MAX
    #define MIN_DOUBLE MIN
#else
    #define MAX std::max
    #define MIN std::min
    #define MAX_LONG std::max<long>
    #define MIN_LONG std::min<long>
    #define MAX_DOUBLE std::max<double>
    #define MIN_DOUBLE std::min<double>
#endif

#ifndef VE_VERSION
    #define MRPROTDATA MrProt
    #define SEQEXPO SeqExpo
#else
    #define MRPROTDATA MrProtocolData::MrProtData
    #define SEQEXPO SeqExpo
#endif


// --------------------------------------------------------------------------------------
// Macros for pointer / reference conversion
// --------------------------------------------------------------------------------------

#ifdef VE_VERSION
    #define __PTR(A) A
    #define __REF(A) A
#else
    #define __PTR(A) *A
    #define __REF(A) &A
#endif


// --------------------------------------------------------------------------------------
// Macros for creating MrProt wrapper instances under VD-line versions
// --------------------------------------------------------------------------------------

#ifndef VE_VERSION
    #define SWL_CNV(A,B,C)  MrProt* B = &A;
    #define SWL_CNV2(A,B,C) MrProt* B = (MrProt*) A;
#else
    #define SWL_CNV(A,B,C)  MrProt C(A); MrProt* B = &C;
    #define SWL_CNV2(A,B,C) MrProt C(A); MrProt* B = &C;
#endif


// --------------------------------------------------------------------------------------
// Error Retun
// --------------------------------------------------------------------------------------
#ifdef VE_VERSION
    #define OnErrorReturn(S) if(!MrSucceeded(S)) return(S)

    #define OnErrorPrintAndReturn(S,P) if (!MrSucceeded(S)) \
        { MRTRACE("Error from %s \n",P); return(S);}
#endif

#ifdef VB_VERSION
    #define OnErrorReturn(S)  if (((S) & NLS_SEV) != NLS_SUCCESS) return(S)

    #define OnErrorPrintAndReturn(S,P) if (((S) & NLS_SEV) != NLS_SUCCESS) \
	    { fprintf(stdout,"%s Error from %s \n",ptModule,P); return(S);}
#endif




#endif // IRFF_conversion_h
