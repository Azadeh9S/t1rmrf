#ifndef StdSeqIF_h
#define StdSeqIF_h 1
                
                

// * ------------------------------------------------------------------------------ *
// * Includes                                                                       *
// * ------------------------------------------------------------------------------ *
#include "MrServers/MrMeasSrv/SeqIF/Sequence/SeqIF.h"
#include "MrServers/MrMeasSrv/MeasUtils/nlsmac.h"       // * definition of type NLS_STATUS  *




// * ------------------------------------------------------------------------------ *
// * Forward declarations                                                           *
// * ------------------------------------------------------------------------------ *
class MrProt;
class SeqLim;
class SeqExpo;
class Sequence;




class StdSeqIF : public SeqIF
{

    public:

        StdSeqIF() {};


        // * ------------------------------------------------------------------ *
        // *                                                                    *
        // * Name        :  StdSeqIF::runKernel                                 *
        // *                                                                    *
        // * Description :  Executes the basic timing of the real-time          *
        // *                sequence.                                           *
        // *                                                                    *
        // * Return      :  NLS status                                          *
        // *                                                                    *
        // * ------------------------------------------------------------------ *
        virtual NLS_STATUS runKernel(MrProt *pMrProt,SeqLim *pSeqLim, SeqExpo *pSeqExpo, long lKernelMode, long lSlice, long lPartition, long lLine) = 0;
	
};



#endif
